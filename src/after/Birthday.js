import React, { Component } from 'react';
import { Breadcrumb, Form, Button, Table, Select, message } from 'antd';
import { fpost } from '../common/io.js';
import { Loading, Pagination } from '../common/g.js';

const FormItem = Form.Item;
const Option = Select.Option;

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			personList: null,
			total: 1,
			searchPara: {
				currentPage: 1, 
				pageSize: 10, 
				birthdayMonth: new Date().getMonth()+1
			}
		}
	}
	getDataList(para) {
		fpost('/api/reception/student/pageBirthdayStudent', para).then(res=>res.json()).then(res=>{
			if(res.success == true) {
				this.setState({
					personList: res.result.records.map((e, i)=>{
						e.key = i;
						return e;
					}),
					total: res.result.total
				})
			}else {
				message.error(res.message);
			}
		})
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount() {
		//获取列表
		this.getDataList(this.state.searchPara);
	}
	render() {
		const columns = [
			{
			  title: '头像',
			  key: 'headImgUrl',
			  className:'f-align-center',
			  render: (text, record) => (
			  	record.headImgUrl? <img src={record.headImgUrl} width={50} height={50} className='f-round'/> : ''
			  )
			}, {
			  title: '学员姓名 (学号)',
			  dataIndex: 'name',
			  key: 'name',
			  className:'f-align-center',
			  render: (text, record) => <span href="#">{record.name}（{record.stuNo}）</span>,
			}, {
			  title: '性别',
			  dataIndex: 'genderStr',
			  key: 'genderStr',
			  className:'f-align-center',
			}, {
			  title: '年龄',
			  dataIndex: 'age',
			  key: 'age',
			  className:'f-align-center',
			}, {
			  title: '手机号码',
			  dataIndex: 'phone',
			  key: 'phone',
			  className:'f-align-center',
			}, {
			  title: '生日',
			  key: 'birthday',
			  className:'f-align-center',
			  render: record => (
				<span>{ record.birthday.substr(0, 10) }</span>
			  )
			}, {
			  title: '来源',
			  dataIndex: 'sourceStr',
			  key: 'sourceStr',
			  className:'f-align-center',
			}, {
			  title: '销售员(工号)',
			  dataIndex: 'salesName',
			  key: 'salesName',
			  className:'f-align-center',
			}];

		return (<div>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>售后服务</Breadcrumb.Item>
					    <Breadcrumb.Item>学员服务</Breadcrumb.Item>
					    <Breadcrumb.Item className='f-fz5 f-bold'>生日学员</Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<SearchFormCreate setPara={ this.setPara.bind(this) } updateDataList={ this.getDataList.bind(this) }/>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						{
							this.state.personList != null?
							<Table columns={columns} dataSource={this.state.personList} pagination={ Pagination(this.state.total, this.state.searchPara, this.getDataList.bind(this)) } bordered scroll={{ x: 1300 }}/>
							: <Loading />
						}
					</div>
				</div>)
	}
}

class SearchForm extends Component {
	searchCondition() {
		let obj = this.props.form.getFieldsValue();
			obj.currentPage = 1;
			obj.pageSize = 10;
			this.props.updateDataList(obj);
			this.props.setPara(obj);
	}
	render() {
		const { getFieldDecorator } = this.props.form;

		return (<Form>
					<FormItem className='f-inline-block f-mr4' label='时间' wrapperCol={{style:{width:'50%', display:'inline-block', verticalAlign:'middle'}}}>
						{getFieldDecorator('birthdayMonth', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: (new Date().getMonth()+1)+''
				          })(
				            <Select
				                showSearch
				                style={{ width: 200 }}
				                placeholder="全部"
				                optionFilterProp="children"
				                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				              >
				                <Option value="1">一月</Option>
				                <Option value="2">二月</Option>
				                <Option value="3">三月</Option>
				                <Option value="4">四月</Option>
				                <Option value="5">五月</Option>
				                <Option value="6">六月</Option>
				                <Option value="7">七月</Option>
				                <Option value="8">八月</Option>
				                <Option value="9">九月</Option>
				                <Option value="10">十月</Option>
				                <Option value="11">十一月</Option>
				                <Option value="12">十二月</Option>
				              </Select>
				          )}
					</FormItem>
					<FormItem className='f-inline-block'>
						<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.searchCondition.bind(this) }>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
					</FormItem>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);