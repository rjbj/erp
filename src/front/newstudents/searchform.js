import React, {
	Component
} from 'react';
import $ from 'jquery';
import {
	Input,
	Button,
	Form
} from 'antd';
import {
	undefinedToEmpty,
	getUnicodeParam
} from '../../common/g.js';
const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAdvancedSearch: false,
			name: getUnicodeParam('name') || '',
			phone: getUnicodeParam('phone') || ''
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			isAdvancedSearch,
			name,
			phone
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form layout="inline">
				<FormItem
					label="学员"
				>
					{
						getFieldDecorator('name', { 
							rules: [{
								required: true, 
								message: '请输入学员姓名' 
							}],
							initialValue:name
						})
						(
							<Input style={{width:'300px'}} placeholder="请输入学员姓名" />
						)
					}
				</FormItem>
				<FormItem
					label="手机"
				>
					{
						getFieldDecorator('phone', { 
							rules: [{
								required: true, 
								message: '请输入手机号码' 
							}],
							initialValue:phone
						})
						(
							<Input style={{width:'300px'}} placeholder="请输入手机号码" />
						)
					}
				</FormItem>
				<FormItem>
					<Button 
						type="primary"
						htmlType="submit"
						size="large" 
						icon="search" 
						onClick={ this._search.bind(this) }
					>搜索</Button>
		        </FormItem>
		        <FormItem className="f-pale">
	      			请先查询学员信息是否存在^_^
	      		</FormItem>
	      	</Form>
		</header>);
	}
	componentDidMount() {
		this._changeSearchType();
	}
	_changeSearchType() {
		const self = this;
		$('#J-searchTab li').on('click', function(e) {
			let type = $(this).attr('data-type');
			let isAdvancedSearch = type == 1 ? true : false;

			$(this).addClass('cur f-blue').siblings().removeClass('cur f-blue');

			self.setState({
				isAdvancedSearch
			});
		});
	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		e.preventDefault();

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isAdvancedSearch: false
		}, () => {
			this.setState({
				isAdvancedSearch: true
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;