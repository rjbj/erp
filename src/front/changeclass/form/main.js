import React, {
	Component
} from 'react';
import {
	Button,
	Col,
	Row,
	Breadcrumb,
	message,
	Modal
} from 'antd';
import moment from 'moment';
import {
	default as OutClassInfo
} from './outClassInfo/main.js';
import {
	default as SignUpForm
} from './signupform/main.js';
import {
	default as PayFormIn
} from './pay/in.js';
import {
	default as PayFormOut
} from './pay/out.js';
import {
	default as OtherForm
} from './other/main.js';

import {
	fpost,
	fpostArray
} from '../../../common/io.js';
import {
	Loading,
	changeTwoDecimal
} from '../../../common/g.js';

import '../main.less';

/*封装给服务端*/
// let registrationClassCourseVOList=[];//报班对象

export default class Main extends Component {
	constructor(props) {
		super(props);

		let signData = window.store.session('signData');
		let {
			id,
			signType, //1,单报，2，连报
			school,
			registrationCourseId, //转出报班id
		} = signData;

		this.state = {
			result: null, //初始化数据
			registrationVO: {
				schoolAreaId: school, //所选校区
				amountType: 1, // 金额操作类型 0, "不退不补" 1, "退" 2, "补"
				refundsType: 1, // 多余钱退费类型 1, "转余额" 2, "退款"
			}, //报班对象最外层
			registrationClassCourseVOList: [], //报班的课程对象
			registrationPayTypeVOList: null, //支付信息对象,
			transferClassVO: null, //转出班级信息,
			totalActualAmount: 0, //课程总实收
			bizCode: "COURSE_TRANSFER", // 业务编码  "REGISTRATION", "报名" "COURSE_TRANSFER", "转班" "COURSE_REFUND", "退课" "COURSE_PARYLY_REFUND", "退差价" "COURSE_ARREAR", "补欠费" "BALANCE_RECHARGE", "余额充值" "BALANCE_WITHDRAWALS", "余额提现" "MATERIAL_SALES", "物品售卖" "MATERIAL_REFUND", "物品退货"
		}
	}
	render() {
		let {
			result,
			transferClassVO,
			totalActualAmount = 0,

			arrearAmountOut = 0,
			remainTuitionAmount = 0,

			courseName,
			originalPrice = 0,
			preferentialAmount = 0,
			arrearAmount = 0,
			useBalance = 0,
			goodsTotalPrice = 0,
			actualAmount = 0,
		} = this.state;

		if (!result || !transferClassVO) {
			return <Loading />
		}

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>学员管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">转班</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<section>
				{
					transferClassVO ?
						<OutClassInfo
							data= { transferClassVO }
							getData={ this._getOutClassData.bind(this)}
						/>
					: null
				}

				<div className="f-clear f-mb3 f-mt5">
					<span className="f-title-blue">报名信息</span>
				</div>

				<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white">
					<div className="f-pd5 f-right-title-box">
						<div className="f-bg-yellow f-right-title">
							<h3>课程／班级信息</h3>
						</div>
						{
							result && result.courseVOList ?
								<div>
									{
										result.courseVOList.map((d,i)=> {
											let {
												studentPoolBalance//账户余额
											} = d;

											return (<Row gutter={40} key={i} type="flex" style={{minHeight:'450px'}}>
												<Col span={19}>
													<SignUpForm
														ref={form => this.classForm = form}
														key={i}
														data={d}
														getData={this._getCourseData.bind(this)}
														studentPoolBalance={studentPoolBalance}
													/>
												</Col>
												<Col span={5}>
													<div className="price-tip">
														<dl>
															<dt>{courseName}</dt>
															<dd>
																<span className="label">
																	<i className="operator"/>
																	原价
																</span>
																<span className="f-red">¥{originalPrice}</span>
															</dd>
															<dd>
																<span className="label">
																	<i className="operator">-</i>
																	优惠
																</span>
																<span className="f-red">¥{preferentialAmount}</span>
															</dd>
															<dd>
																<span className="label">
																	<i className="operator">-</i>
																	余额抵用
																</span>
																<span className="f-red">¥{useBalance}</span>
															</dd>
															<dd>
																<span className="label">
																	<i className="operator">+</i>
																	物品费用
																</span>
																<span className="f-red">¥{goodsTotalPrice}</span>
															</dd>
															<dd>
																<span className="label">
																	<i className="operator">-</i>
																	剩余学费
																</span>
																<span className="f-red">¥{remainTuitionAmount}</span>
															</dd>
															<dd>
																<span className="label">
																	<i className="operator">+</i>
																	转出欠费
																</span>
																<span className="f-red">¥{arrearAmountOut}</span>
															</dd>
															<dd>
																	<i className="operator">=</i>
																	<i className="f-fz5 f-red">¥{ totalActualAmount }</i>
															</dd>
														</dl>
													</div>
												</Col>
											</Row>);
										})
									}
								</div>
							: null
						}
		        	</div>
				</div>

				{
					totalActualAmount > 0 ?
						<PayFormIn 
							form= { this.props.form } 
							totalActualAmount = { totalActualAmount }
							onInput = { this._getPayData.bind(this) }
						/>
					: null
				}
				{
					totalActualAmount < 0 ?
						<PayFormOut 
							getData= { this._getBackMoney.bind(this) }
							form= { this.props.form } 
							totalActualAmount = { totalActualAmount }
						/>
					: null
				}

				<OtherForm 
					ref= { form => this.otherForm= form } 
				/>

				<div className="f-align-right f-mt5">
					<Button size="large" className="f-mr5">
						取消
					</Button>
					<Button 
						size="large" 
						type="primary" 
						onClick= {this._getAllParam.bind(this)}
					>
						保存
					</Button>
				</div>

	      	</section>
		</section>);
	}
	componentDidMount() {
		this._init();
	}
	_getCourseData(data) { //当子元素改变时获取报班信息
		let {
			registrationClassCourseVOList
		} = this.state;
		registrationClassCourseVOList = registrationClassCourseVOList.filter((d, i) => {
			return data.courseId != d.courseId
		});
		registrationClassCourseVOList.push(data);
		this.setState({
			registrationClassCourseVOList
		}, () => {
			this.setState({
				courseName: registrationClassCourseVOList[0].courseName,
				originalPrice: registrationClassCourseVOList[0].originalPrice,
				preferentialAmount: registrationClassCourseVOList[0].preferentialAmount,
				arrearAmount: registrationClassCourseVOList[0].arrearAmount,
				useBalance: registrationClassCourseVOList[0].useBalance,
				goodsTotalPrice: registrationClassCourseVOList[0].goodsTotalPrice,
				actualAmount: registrationClassCourseVOList[0].actualAmount,
			}, () => {
				// console.log('报班信息:', this.state.registrationClassCourseVOList);
				this._computePrice();
			});
		});
	}
	_getOutClassData(d) { //获取转出班级信息
		let {
			transferClassVO
		} = this.state;

		let {
			arrearAmount,
			remainTuitionAmount
		} = d;
		this.setState({
			transferClassVO: d,
			arrearAmountOut: arrearAmount,
			remainTuitionAmount
		}, () => {
			console.log('转出班级信息', this.state.transferClassVO);
			this._computePrice();
		});
	}
	_getAllParam() { //获取报班信息
		let {
			registrationVO,
			registrationClassCourseVOList,
		} = this.state;
		/*报名信息表单*/
		this.classForm.validateFields((err, values) => {
			if (!err) {
				registrationVO.registrationClassCourseVOList = registrationClassCourseVOList;

				this.setState({
					registrationVO,
				}, () => {
					this._getOtherData();
					console.log('报名信息: ', this.state);
				});
			}
		});
	}
	_getOtherData() {
		let {
			registrationVO
		} = this.state;
		/*其他信息表单*/
		this.otherForm.validateFields((err, values) => {
			if (!err) {
				let {
					registrationTime,
				} = values;

				registrationTime = moment(registrationTime).format('YYYY-MM-DD HH:mm:ss');
				values.registrationTime = registrationTime;

				registrationVO.registrationTime = registrationTime;

				this.setState({
					registrationVO
				}, () => {
					console.log('其他信息: ', this.state);
					this._save();
				});
			}
		});
	}
	_save() {
		console.log(this.state);
		// console.log(JSON.stringify(this.state));
		let {
			registrationVO
		} = this.state;
		let {
			refundsType,
			amountType,
			registrationPayTypeVOList
		} = registrationVO;
		let mesText = '请选择支付方式';

		if (refundsType == '2' && amountType != '0') {
			if ((!registrationPayTypeVOList || !registrationPayTypeVOList.length) || (!registrationPayTypeVOList[0].tradeType == undefined)) {
				Modal.error({
					title: '温馨提醒',
					content: mesText,
				});
				return;
			} else if (!registrationPayTypeVOList[0].tradeAmount) {
				mesText = '请输入实退金额'
				Modal.error({
					title: '温馨提醒',
					content: mesText,
				});
				return;
			}
		}

		console.log(JSON.stringify(this.state));

		let url = '/api/reception/registration/transfer/class/add';
		fpostArray(url, this.state)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				console.log(res);
				message.success('恭喜您,报名成功!');
				let registrationId = res.result.id;
				let schoolAreaId = res.result.schoolAreaId;
				let url = "/front/students";

				url = `/print?registrationId=${registrationId}&schoolAreaId=${schoolAreaId}&type=5`;

				window.location.href = url;
				// window.location.href="/front/students";
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_getPayData(v) { //支付信息
		let {
			registrationVO,
			registrationClassCourseVOList,
			totalActualAmount
		} = this.state;
		let registrationPayTypeVOList;
		registrationPayTypeVOList = (
			v.trades &&
			v.trades[0] &&
			v.trades[0].tradeAmount.length
		) ? v.trades : [];

		let arrearAmount, actualAmount;
		actualAmount = v.total;
		arrearAmount = changeTwoDecimal(totalActualAmount - actualAmount);

		if (registrationClassCourseVOList && registrationClassCourseVOList[0]) {
			registrationClassCourseVOList[0].arrearAmount = arrearAmount;
			registrationClassCourseVOList[0].actualAmount = actualAmount;
		}
		registrationVO.registrationPayTypeVOList = registrationPayTypeVOList;

		this.setState({
			registrationPayTypeVOList,
			registrationVO,
			actualAmount,
			arrearAmount,
		}, () => {
			console.log('支付信息', this.state);
		});
	}
	_getBackMoney(data) {
		console.log(data);
		let {
			registrationVO
		} = this.state;
		let {
			refundsType,
			backMoney,
			trades
		} = data;
		let registrationPayTypeVOList;
		registrationPayTypeVOList = trades || [];
		registrationVO.refundsType = refundsType;
		registrationVO.backMoney = backMoney;
		registrationVO.registrationPayTypeVOList = registrationPayTypeVOList;
		this.setState({
			registrationPayTypeVOList,
			registrationVO
		}, () => {
			console.log(this.state);
		});
	}
	_init() {
		let signData = window.store.session('signData');
		if (!signData) {
			return;
		}
		let {
			signType,
			singleIds,
			id,
			discount,
			many,
			registrationCourseId
		} = signData;

		let manyIds = many && many.courseIds;
		let courseIds = signType == '1' ? singleIds : manyIds;
		courseIds = courseIds.join(',');

		console.log('signData:', signData);
		let param = {
			courseIds: courseIds,
			registrationCourseId: registrationCourseId
		}

		fpost('/api/reception/registration/transfer/class/add/init', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let {
					courseVOList
				} = result;
				courseVOList.map((d, i) => {
					d.key = i;
				});
				this.setState({
					result
				}, () => {
					this._initTransferClassVO(); //初始化转出班级信息
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_computePrice() {
		let {
			registrationVO,
			originalPrice, //原价
			actualAmount, //实收
			preferentialAmount, //总优惠
			arrearAmount, //转入欠费
			useBalance, //余额抵用
			goodsTotalPrice, //商品总额
			remainTuitionAmount, //剩余学费
			arrearAmountOut, //转出欠费
		} = this.state;
		originalPrice = changeTwoDecimal(originalPrice);
		preferentialAmount = changeTwoDecimal(preferentialAmount);
		useBalance = changeTwoDecimal(useBalance);
		actualAmount = changeTwoDecimal(actualAmount);
		goodsTotalPrice = changeTwoDecimal(goodsTotalPrice);
		goodsTotalPrice = changeTwoDecimal(goodsTotalPrice);
		remainTuitionAmount = changeTwoDecimal(remainTuitionAmount);
		arrearAmountOut = changeTwoDecimal(arrearAmountOut);

		let amountType;

		let totalActualAmount = changeTwoDecimal(originalPrice - preferentialAmount - useBalance + goodsTotalPrice - remainTuitionAmount + arrearAmountOut);
		if (totalActualAmount > 0) { //应补
			amountType = '2';
		} else if (totalActualAmount < 0) { //应退
			amountType = '1';
		} else {
			amountType = '0';
		}
		registrationVO.amountType = amountType;
		this.setState({
			totalActualAmount,
			registrationVO
		});
	}
	_initTransferClassVO() {
		let {
			transferClassVO,
			result
		} = this.state;
		let {
			arrearAmount, // 欠费金额
			classId, // 班级id
			className, // 班级名称
			classState, // 班级状态1:已排课未开课、2:已开课、3:已结课
			courseName, // 课程名称
			coursePeriodConsumeVO, // 课时消耗统计对象
			isArrear, // 是否欠费
			receivableAmount, // 应付金额
			remainTuitionAmount, // 剩余学费
			studentId,
			registrationCourseId, // 报班id
			transferBalance, // 转出余额
			actualConsumeType, // 实际课消类型  1 原价 2 应付
			actualConsumeAmount, // 实际课消金额
		} = result;
		transferClassVO = {
			arrearAmount, // 欠费金额
			classId, // 班级id
			className, // 班级名称
			classState, // 班级状态1:已排课未开课、2:已开课、3:已结课
			courseName, // 课程名称
			coursePeriodConsumeVO, // 课时消耗统计对象
			isArrear, // 是否欠费
			receivableAmount, // 应付金额
			remainTuitionAmount, // 剩余学费
			studentId,
			registrationCourseId, // 报班id
			transferBalance, // 转出余额
			actualConsumeType, // 实际课消类型  1 原价 2 应付
			actualConsumeAmount, // 实际课消金额
		}
		this.setState({
			transferClassVO
		}, () => {
			// console.log('转出班级信息：',this.state.transferClassVO);
		});
	}

}