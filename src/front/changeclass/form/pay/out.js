import React, {
	Component
} from 'react';
import {
	InputNumber,
	Col,
	Row,
	Radio,
	Select
} from 'antd';

import {
	payTypes
} from '../../../../common/staticData.js';

const RadioGroup = Radio.Group;
const Option = Select.Option;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			trades: [],
			backMoney: 0, //实退
			refundsType: 1, // 多余钱退费类型 1, "转余额" 2, "退款"
		}
	}
	render() {
		let {
			refundsType,
			backMoney
		} = this.state;
		let {
			getData,
			totalActualAmount
		} = this.props;
		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
			        <div className="f-pd5 f-right-title-box">
				        <div className="f-bg-green f-right-title">
		        			<h3>费用信息</h3>
		        		</div>
		        		<Row gutter={16} className="f-mb5">
		        			<Col span={2} className="f-align-right">转班应退: </Col>
		        			<Col span={20}><i className="f-fz5 f-red">¥{totalActualAmount}</i></Col>
		        		</Row>
		        		<Row gutter={16} className="f-mb5">
		        			<Col span={2} className="f-align-right f-lh6">实退: </Col>
		        			<Col span={20}>
		        				<span className="f-mr5">
		        					<InputNumber
										formatter={
											(value)=> {
												if(/^\d*(\.\d{0,2})?$/.test(value)){
													return `¥ ${value}`
												}else{
													value = parseFloat(value);
													value = Math.round(value * 100) / 100;
													return `¥ ${value}`;
												};
											}
										} 
										style={{ width:'100px' }} 
										size="large"
										value={backMoney}
										min={0}
										max={-totalActualAmount}
										onChange = {
											(v)=> {
												let {
													trades,
													tradeType,
													backMoney
												} = this.state;

												if(/^\d*(\.\d{0,2})?$/.test(v)){
													//符合条件
												}else{
													v = parseFloat(v);
													v = Math.round(v * 100) / 100;
												};
												
												trades = [{
													tradeType: tradeType,
													tradeAmount: v
												}]
												this.setState({
													tradeType,
													backMoney:v,
													trades
												},()=> {
													getData(this.state);
												});
											}
										}
									/>
		        				</span>
		        			</Col>
		        		</Row>
		        		<Row gutter={16} className="f-mb5">
		        			<Col span={2} className="f-align-right f-lh6">退款方式: </Col>
		        			<Col span={20} className="f-lh6">
		        				<RadioGroup
		        					onChange={
		        						(e)=> {
		        							let v = e.target.value;
		        							this.setState({
		        								refundsType:v
		        							},()=> {
		        								getData(this.state);
		        							});
		        						}
		        					}
		        					defaultValue={refundsType}
	        					>
	        				        <Radio value={1}>转余额</Radio>
	        				        <Radio value={2}>退款</Radio>
	        				      </RadioGroup>
		        			</Col>
		        		</Row>
		        		{
		        			this.state.refundsType == 2 ?
				        		<Row gutter={16}>
				        			<Col span={2} className="f-align-right f-lh6 ant-form-item-required">支付方式: </Col>
				        			<Col span={20}>
				        				<Select
				        					style={{
				        						width:'200px'
				        					}}
				        					size="large"
				        					placeholder="请选择支付方式"
				        					onChange= {
				        						(v)=> {
				        							let {
				        								trades,
				        								backMoney
				        							} = this.state;

				        							trades = [{
				        								tradeType: v,
				        								tradeAmount: backMoney
				        							}]
				        							this.setState({
				        								tradeType:v,
				        								backMoney,
				        								trades
				        							},()=> {
				        								getData(this.state);
				        							});
				        						}
				        					}
			        					>
						                    {
						                        payTypes.map((d,i)=> {
						                            return <Option key={i} value={d.value}>{d.label}</Option>
						                        })
						                    }
						                </Select>
				        			</Col>
				        		</Row>
				        	: null
		        		}
			        </div>
				</div>);
	}
}