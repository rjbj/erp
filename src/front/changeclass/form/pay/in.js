import React, {
	Component
} from 'react';
import {
	Col,
	Row,
} from 'antd';
import {
	changeTwoDecimal
} from '../../../../common/g.js';

import PayTypes from '../../../../common/payTypes.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			arrearAmount: 0
		}
	}
	render() {
		let {
			arrearAmount
		} = this.state;
		let {
			totalActualAmount
		} = this.props;
		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
			        <div className="f-pd5 f-right-title-box">
				        <div className="f-bg-green f-right-title">
		        			<h3>费用信息</h3>
		        		</div>
		        		<Row gutter={16} className="f-mb5">
		        			<Col span={2} className="f-align-right">转班应补: </Col>
		        			<Col span={20}><i className="f-fz5 f-red">¥{totalActualAmount}</i></Col>
		        		</Row>
		        		<Row gutter={16}>
		        			<Col span={2} className="f-align-right f-lh6">支付方式: </Col>
		        			<Col span={20}>
		        				<PayTypes 
		        					onInput = { this._getInput.bind(this) }
		        				/>
		        			</Col>
		        		</Row>
		        		{
		        			arrearAmount ?
				        		<Row gutter={16}>
				        			<Col span={2} className="f-align-right f-lh6">欠费: </Col>
				        			<Col span={20}>
				        				<Col span={20}><i className="f-fz5 f-red">¥{arrearAmount}</i></Col>
				        			</Col>
				        		</Row>
				        	: null
		        		}
			        </div>
				</div>);
	}
	_getInput(v) {
		this.props.onInput(v);
		let {
			totalActualAmount
		} = this.props;
		let arrearAmount = changeTwoDecimal(totalActualAmount) - changeTwoDecimal(v.total);
		this.setState({
			arrearAmount
		});
	}
}