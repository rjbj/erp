import React, {
	Component
} from 'react';
import Remark from '../../../../common/remark.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			remark: null, //备注信息
		}
	}
	render() {

		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
	        <div className="f-pd5 f-right-title-box">
		        <div className="f-bg-blue f-right-title">
        			<h3>其他</h3>
        		</div>
        		{/*备注*/}
				<Remark 
					onInput= { this._getRemark.bind(this) }
				/>
	        </div>
		</div>);
	}
	_getRemark(v) {
		let {
			getData
		} = this.props;
		let {
			internalRemark,
			foreignRemark
		} = v;

		this.setState({
			internalRemark,
			foreignRemark
		}, () => {
			getData({
				internalRemark,
				foreignRemark
			});
			// console.log('备注:', this.state.remark);
		});
	}
}