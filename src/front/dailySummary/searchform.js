import React, {
	Component
} from 'react';
import $ from 'jquery';
import {
	Input,
	Button,
	Form
} from 'antd';
import {
	undefinedToEmpty,
	getUnicodeParam
} from '../../common/g.js';
import SelectSchool from '../../common/selectSchool.js';

const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAdvancedSearch: false,
			name: getUnicodeParam('name') || '',
			phone: getUnicodeParam('phone') || ''
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			isAdvancedSearch,
			name,
			phone
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form layout="inline">
				<FormItem
			        label="校区"
			    >
			        {
			            getFieldDecorator('schoolAreaId', { 
			                rules: [{
			                    required: true, 
			                    message: '请选择校区' 
			                }]
			            })
			            (
		                	<SelectSchool 
		                		setDefault="1"
								onSelect={ 
									(v)=> {
										this.props.form.setFieldsValue({
											schoolAreaId:v
										});
										this._search();
									}
								} 
							/>
			            )
			        }
			    </FormItem>
	      	</Form>
		</header>);
	}
	_search() { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				onSearch(values);
			}
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;