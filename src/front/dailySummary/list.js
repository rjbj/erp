import React, {
	Component
} from 'react';
import {
	Table,
	Icon,
	Button
} from 'antd';
import {
	Link
} from 'react-router-dom';
import printJS from 'print-js';

export default class Main extends Component {
	render() {

		let {
			data,
			loading,
			pagination,
			onChange
		} = this.props;

		return (
			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">今日收入</span>
        </h3>
        <div id="J-print-content">
	        <Table
	            columns={ columns }
	            bordered
	            dataSource={ data }
	            pagination={ false }
	            loading={loading}
	            onChange={onChange}
	        />
        </div>
        <br />
        <div className="f-align-right">
	        <Button
				size = "large"
				type = "primary"
				onClick = {
					() => {
						printJS({
							printable: 'J-print-content',
							type: 'html',
							documentTitle: '凡学教育集团',
							honorColor: true
						});
					}
				} 
			>
				打印 
			</Button>
		</div>
      </div>
		);
	}

}

const columns = [{
	title: '业务类型',
	dataIndex: 'businessCodeStr',
}, {
	title: '现金',
	dataIndex: 'cashAmount',
}, {
	title: '微信',
	dataIndex: 'wxpayAmount',
}, {
	title: '支付宝',
	dataIndex: 'alipayAmount',
}, {
	title: '刷卡',
	dataIndex: 'cardAmount',
}, {
	title: '转账',
	dataIndex: 'transferAmount',
}, {
	title: '支票',
	dataIndex: 'checkAmount',
}];