import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Form,
} from 'antd';
import {
	getUnicodeParam,
	undefinedToEmpty
} from '../../common/g.js';
const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form layout="inline">
				<FormItem
					label="学员"
				>
					{
						getFieldDecorator('name', { 
							rules: [{
								required: false, 
								message: '请输入学员姓名' 
							}],
							initialValue:getUnicodeParam('name') || ''
						})
						(
							<Input style={{width:'300px'}} placeholder="请输入学员姓名" />
						)
					}
				</FormItem>
				<FormItem
					label="手机"
				>
					{
						getFieldDecorator('phone', { 
							rules: [{
								required: false, 
								message: '请输入手机号码' 
							}],
							initialValue:getUnicodeParam('phone') || ''
						})
						(
							<Input style={{width:'300px'}} placeholder="请输入手机号码" />
						)
					}
				</FormItem>
				<FormItem>
					<Button 
						type="primary"
						htmlType="submit"
						size="large" 
						icon="search" 
						onClick={ this._search.bind(this) }
					>搜索</Button>
		        </FormItem>
				
	      	</Form>
		</header>);
	}

	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		e.preventDefault();

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;