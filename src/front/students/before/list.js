import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Button,
  Modal,
  Row,
  Col
} from 'antd';
let self;

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true
    }
  }
  render() {
    self = this;
    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue">售前沟通日志</span>
        </h3>
        <Table
            columns={ columns }
            dataSource={ data }
            scroll={{ x: 1260}}
          />

          <Modal title="查看沟通记录"
            visible={ this.state.visible }
            onOk={ ()=>{} }
            confirmLoading={ true }
            onCancel={ ()=>{ this.setState({visible:false}) } }
            footer={null}
          >
            <div className="f-lh5">
              <Row>
                <Col span={12}>
                  沟通类型：售前
                </Col>
                <Col span={12}>
                  沟通方式：微信
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  沟通时间：2017-08-22  10:00
                </Col>
                <Col span={12}>
                  承诺到访：是
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  预约到访校区：昂立局前
                </Col>
                <Col span={12}>
                  预约到访时间：2017-08-22  10:00
                </Col>
              </Row>
              <Row>
                <Col span={4}>
                  意向课程：
                </Col>
                <Col span={20}>
                  预约到访时间：2017-08-22  10:00
                </Col>
              </Row>
              <Row>
                <Col span={4}>
                  沟通内容：
                </Col>
                <Col span={20} className="f-lh1">
                  沟通内容描述沟通内容描述沟通内容描述沟通内容描述沟通内容描述沟通内容描述沟通内容描述沟通内容描述
                </Col>
              </Row>
            </div>
          </Modal>

      </div>
    );
  }
}

const columns = [{
  title: '沟通时间',
  dataIndex: 'time',
  fixed: 'left',
  key: 'asdf',
  render: (value, row, index) => {
    return <div>
      鹿子野(02210832)
    </div>
  }
}, {
  title: '意向课程(意向度)',
  dataIndex: 'name',
  key: 'asdf1',
  render: (value, row, index) => {
    return (<div>
      <Icon type="user" /> { value }
    </div>);
  }
}, {
  title: '沟通方式',
  dataIndex: 'type',
  key: 'asd3',
  render: (value, row, index) => {
    return (<div>
      <Icon type="tags" /> { value }
    </div>);
  }
}, {
  title: '是否诺访',
  dataIndex: 'des',
  key: 'asd2',
  render: (value, row, index) => {
    return (<div>
      <Icon type="file-text" /> { value }
    </div>);
  }
}, {
  title: '诺访时间',
  dataIndex: 'des',
  key: 'asdf5',
  render: (value, row, index) => {
    return (<div>
      <Icon type="file-text" /> { value }
    </div>);
  }
}, {
  title: '诺访校区',
  dataIndex: 'des',
  key: 'asdfa1',
  render: (value, row, index) => {
    return (<div>
      <Icon type="file-text" /> { value }
    </div>);
  }
}, {
  title: '沟通内容',
  dataIndex: 'des',
  key: 'asdfsdfa',
  render: (value, row, index) => {
    return (<div>
      <Icon type="file-text" /> { value }
    </div>);
  }
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  key: 'asdf89',
  className: "f-align-center",
  render: (value, row, index) => {
    return (<div>
        <Button 
        onClick={
          ()=> {
            self.setState({
              visible:true
            });
          }
        }
        type="primary" className="f-radius4">
          查看
        </Button>
    </div>);
  }
}];

const data = [{
  key: '1',
  time: '2017-9-1  9:00',
  name: '小陈陈',
  type: '报名',
  des: '操作描述'
}, {
  key: '1a',
  time: '2017-9-1  9:00',
  name: '小陈陈',
  type: '报名',
  des: '操作描述'
}, {
  key: '1d',
  time: '2017-9-1  9:00',
  name: '小陈陈',
  type: '报名',
  des: '操作描述'
}, {
  key: '1df',
  time: '2017-9-1  9:00',
  name: '小陈陈',
  type: '报名',
  des: '操作描述'
}, {
  key: '1s',
  time: '2017-9-1  9:00',
  name: '小陈陈',
  type: '报名',
  des: '操作描述'
}, {
  key: '1dsa',
  time: '2017-9-1  9:00',
  name: '小陈陈',
  type: '报名',
  des: '操作描述'
}];