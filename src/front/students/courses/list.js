import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  getUnicodeParam
} from '../../../common/g.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    const columns = [{
      title: '课程名称',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'courseName'
    }, {
      title: '报名时间',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'registrationTime'
    }, {
      title: '报名类型',
      dataIndex: 'type',
      className: 'f-align-center',
      width: '150px',
      render: (value, row, index) => {
        let arr = ['', '单报', '连报', '课程包']
        let {
          currentPeriod,
          totalPeriod
        } = row;
        return (<div>
          { arr[value] }
          { value == 2 ? <span className="f-ml2">({ currentPeriod }/ { totalPeriod })</span> : null }
        </div>);
      }
    }, {
      title: '班级名称',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'className'
    }, {
      title: '班主任',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'headmasterName'
    }, {
      title: '校区',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'schoolAreaName'
    }, {
      title: '开班',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classStartTime'
    }, {
      title: '教室',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classRoomName'
    }, {
      title: '学费',
      width: '50px',
      className: 'f-align-center',
      dataIndex: 'receivableAmount'
    }, {
      title: '班级状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classStateStr'
    }, {
      title: '学费状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'isArrearStr'
    }, {
      title: '优惠状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'isOriginalStr'
    }, {
      title: '报班状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'stateStr'
    }, {
      title: '是否续报',
      width: '100px',
      className: 'f-align-center',
      dataIndex: 'isContinueStr'
    }, {
      title: '是否新生',
      width: '100px',
      className: 'f-align-center',
      dataIndex: 'isFreshManStr'
    }, {
      title: '操作',
      dataIndex: 'des',
      fixed: 'right',
      width: '200px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          state, // 1:正常报名、2:转出、3:未开始、4:退课、5:结课、6:停课
          isArrear, //是否欠费 0:否 1:是
          id, //报课id
          studentId, //学员id
        } = row;
        return (<div>
          <Dropdown 
            overlay={
              <Menu>
                <Menu.Item key="1">
                  <Link to={`/front/students/detail?id=${row.id}&name=${getUnicodeParam('name')}&stuNo=${getUnicodeParam('stuNo')}`}>课程详情</Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <Link to={`/front/students/after?id=${row.id}&name=${getUnicodeParam('name')}&stuNo=${getUnicodeParam('stuNo')}&courseName=${row.courseName}&className=${row.className}`}>沟通日志</Link>
                </Menu.Item>
                <Menu.Item key="3">
                    <Link to={`/front/students/singRecord?id=${row.id}`}>上课记录</Link>
                </Menu.Item>
              </Menu>
            }>
            <Button className="f-radius4 f-mr2">
              查看<Icon type="down" />
            </Button>
          </Dropdown>
          <Dropdown 
            overlay={
              <Menu>
                {
                  (isArrear && isArrear == '1') ?
                    <Menu.Item key="1">
                      <Link to={`/front/fillmoney/form?registrationCourseId=${id}&studentId=${studentId}`}>补欠费</Link>
                    </Menu.Item>
                  : null
                }
                {
                  state == '1' ?
                    <Menu.Item key="2">
                      <Link to={`/front/signup/step1?isChangeClass=1&registrationCourseId=${id}&id=${studentId}&type=1`}>转班</Link>
                    </Menu.Item>
                  : null
                }
                {
                  state == '1' ?
                    <Menu.Item key="3">
                      <Link to={`/front/stopclasses/form?registrationCourseId=${id}&studentId=${studentId}`}>停课</Link>
                    </Menu.Item>
                  : null
                }
                <Menu.Item key="4">
                  <Link to={`/front/signup/step1?type=1&id=${studentId}`}>续报</Link>
                </Menu.Item>
                {
                  (state == '1' && isArrear == '0') ?
                    <Menu.Item key="5">
                      <Link to={`/front/returnmoney/form?registrationCourseId=${id}&studentId=${studentId}`}>退差价</Link>
                    </Menu.Item>
                  : null
                }
                {
                  state == '1' ?
                    <Menu.Item key="6">
                      <Link to={`/front/moneyback/form?registrationCourseId=${id}&studentId=${studentId}`}>退课</Link>
                    </Menu.Item>
                  : null
                }
              </Menu>
            }>
            <Button type="primary" className="f-radius4">
              操作<Icon type="down" />
            </Button>
          </Dropdown>
        </div>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">课程列表</span>
        </h3>
        <Table
            columns={ columns }
            dataSource={ data }
            scroll={{ x: 2250}}
            bordered
            pagination={ pagination }
            loading={loading}
            onChange={onChange}
          />
      </div>
    );
  }

}