import React, {
	Component
} from 'react';
import {
	InputNumber,
	Form,
	Select,
	Modal,
	message,
	Input
} from 'antd';
import {
	fpostArray
} from '../../../common/io.js';
import {
	payTypes
} from '../../../common/staticData.js';
import SelectSchool from '../../../common/selectSchool.js';

const FormItem = Form.Item;
const Option = Select.Option;
const {
	TextArea
} = Input;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			studentId: this.props.studentId,
			saveing: false,
			trades: null,
			total: this.props.balanceAmount || 0
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onCancel
		} = this.props;

		let {
			saveing,
			total,
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<Modal title="提现"
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
			>
				<Form>
				    <FormItem
				        label="校区"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('schoolAreaId', { 
				                rules: [{
				                    required: true, 
				                    message: '请选择校区' 
				                }]
				            })
				            (
			                	<SelectSchool 
									onSelect={ 
										(v)=> {
											this.props.form.setFieldsValue({
												schoolAreaId:v
											});
										}
									} 
								/>
				            )
				        }
				    </FormItem>
				    <FormItem
				        label="提现金额"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('tradeAmount', { 
				                rules: [{
				                    required: true, 
				                    message: '请输入提现金额' 
				                }],
				                initialValue:total
				            })
				            (
			                	<InputNumber 
			                		min={0} 
			                		max={total} 
		                		/>
				            )
				        }
				        <span className="f-pale">{`最多可提现¥${total}`}</span>
				    </FormItem>
				    <FormItem
				        label="提现方式"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('trades', { 
				                rules: [{
				                    required: true, 
				                    message: '请选择提现方式' 
				                }] 
				            })
				            (
				                <Select placeholder="请选择提现方式">
				                    {
				                        payTypes.map((d,i)=> {
				                            return <Option key={i} value={d.value}>{d.label}</Option>
				                        })
				                    }
				                </Select>
				            )
				        }
				    </FormItem>
				    <FormItem
				        label="对内备注"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('internalRemark', { 
				                rules: [{
				                    required: false, 
				                }] 
				            })
				            (
				                <TextArea 
				                	rows={2} 
				                />
				            )
				        }
				    </FormItem>
				    <FormItem
				        label="对外备注"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('foreignRemark', { 
				                rules: [{
				                    required: false, 
				                }] 
				            })
				            (
				                <TextArea 
				                	rows={2} 
				                />
				            )
				        }
				    </FormItem>
		      	</Form>
		</Modal>);
	}

	_ok(e) {
		let {
			studentId
		} = this.state;
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				let {
					trades,
					tradeAmount,
					foreignRemark,
					internalRemark,
					schoolAreaId
				} = values;
				values = {
					trades: [{
						tradeType: trades,
						tradeAmount: tradeAmount
					}],
					studentId: studentId,
					foreignRemark,
					internalRemark,
					schoolAreaId
				}
				console.log(values);
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;


		fpostArray('/api/reception/student/studentWithdrawals', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk();
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;