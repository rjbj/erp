import React, {
	Component
} from 'react';
import {
	Input,
	Col,
	Row,
	Form,
	DatePicker,
	Select,
	Radio,
} from 'antd';
import moment from 'moment';
import {
	default as SelectPerson
} from '../../../common/selectPerson.js';
import AvatarUpload from '../../../common/uploadAvatar.js';
import SelectCity from '../../../common/selectCity.js';
import {
	gradeList,
	fromData
} from '../../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			data
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 7
			},
			wrapperCol: {
				span: 17
			}
		};

		if (!data) {
			return null;
		}

		let {
			id,
			name,
			headImgUrl,
			resourceId,
			gender,
			stuNo,
			birthday,
			motherPhone,
			fatherPhone,
			otherPhone,
			recruitSource,
			salesId,
			salesName,
			currentSchoolName,
			balanceAmount = 0,

			isPublicSchool,
			grade,
			provinceId,
			cityId,
			districtId,
			address

		} = this.props.data;

		let citySelect = provinceId && cityId ?
			[provinceId, cityId, districtId] : '';

		birthday = birthday ? moment(birthday) : null;

		return (<Form>
			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white">
				<div className="f-pd5 f-right-title-box">
					<div className="f-bg-blue f-right-title">
						<h3>学生详情信息</h3>
					</div>
					<Row gutter={24}>
						<Col span={15}>
			    			<Row gutter={ 40 }>
			    				<Col span={12}>
			    					<span className="f-hide">
			    						{
			    							getFieldDecorator('id',{
			    								initialValue:id
			    							})
			    							(
			    								<Input />
			    							)
			    						}
			    					</span>
		        					<FormItem
										label="学员姓名"
										{...formItemLayout}
									>
										{
											getFieldDecorator('name', { 
												rules: [{
													required: true, 
													message: '请输入学员姓名' 
												}],
												initialValue:name
											})
											(
												<Input placeholder="请输入学员姓名" />
											)
										}
									</FormItem>
		        				</Col>
		        				<Col span={ 12 }>
		    						<FormItem
										label="性别"
										{...formItemLayout}
									>
										{
											getFieldDecorator('gender', {
												rules: [{ 
													required: true, 
													message: '请选择性别' 
												}],
												initialValue:gender
											})(
												<RadioGroup>
											        <Radio value='1'>男</Radio>
											        <Radio value='2'>女</Radio>
											     </RadioGroup>
											)
										}
									</FormItem>
		    					</Col>
			    	        </Row>
			    	        <Row gutter={ 40 }>
			    				<Col span={12}>
			    					<FormItem
										label="学员生日"
										{...formItemLayout}
									>
										{
											getFieldDecorator('birthday', {
												rules: [{  
													required:false,
													message: '请选择学员生日' 
												}],
												initialValue:birthday
											})(
												<DatePicker
													style={{width:'100%'}}
													placeholder="请选择学员生日" 
												/>
											)
										}
									</FormItem>
			    				</Col>
			    				<Col span={ 12 }>
									<FormItem
										label="母亲电话"
										{...formItemLayout}
									>
										{
											getFieldDecorator('motherPhone', {
												rules: [{ 
													message: '电话号码输入有误',
													pattern: /^1[0-9]\d{4,9}$/ 
												}],
												initialValue:motherPhone
											})(
												<Input placeholder="请输入母亲电话" />
											)
										}
									</FormItem>
								</Col>
			    	        </Row>
			    	        <Row gutter={ 40 }>
			    	        	<Col span={ 12 }>
			    	        		<FormItem
			    	        			label="父亲电话"
			    	        			{...formItemLayout}
			    	        		>
			    	        			{
			    	        				getFieldDecorator('fatherPhone', {
			    	        					rules: [{  
			    	        						message: '电话号码输入有误',
			    	        						pattern: /^1[0-9]\d{4,9}$/  
			    	        					}],
			    	        					initialValue:fatherPhone
			    	        				})(
			    	        					<Input placeholder="请输入父亲电话" />
			    	        				)
			    	        			}
			    	        		</FormItem>
			    	        	</Col>
			    	        	<Col span={12}>
			    					<FormItem
										label="其他电话"
										{...formItemLayout}
									>
										{
											getFieldDecorator('otherPhone', {
												rules: [{  
													message: '请输入其他电话' ,
													pattern: /^1[0-9]\d{4,9}$/ 
												}],
												initialValue:otherPhone
											})(
												<Input placeholder="请输入其他电话" />
											)
										}
									</FormItem>
			    				</Col>
			    	        </Row>
			    	        <Row gutter={ 40 }>
			    				<Col span={ 12 }>
									<FormItem
										label="市场来源"
										{...formItemLayout}
									>
										{
											getFieldDecorator('recruitSource', {
												rules: [{
													required:true,
													message: '请选择' 
												}],
												initialValue:recruitSource
											})(
												<Select
													size="large"
													allowClear
				                					placeholder="请选择"
												>
													{
														fromData.map((d,i)=> {
															return(<Option 
																	key={i} 
																	value={d.value}
																>{d.label}</Option>);
														})
													}
												</Select>
											)
										}
									</FormItem>
								</Col>
								<Col span={ 12 }>
									<FormItem
										label="市场顾问"
										{...formItemLayout}
									>
										{
											getFieldDecorator('salesId', {
												rules: [{  
													required:false,
													message:'请选择销售员'
												}],
												initialValue:salesId
											})(
												<SelectPerson
													url='/api/hr/staff/listStaffByCondForDropDown'
													data={
														{ 
															type:1
														}
													} 
													onSelect={ 
														(v)=> {
															this.props.form.setFieldsValue({
																salesId: v
															});
														} 
													}
													initialValueData={salesId}
												/>
											)
										}
									</FormItem>
								</Col>
			    	        </Row>
						</Col>
						<Col span={9}>
							<FormItem
								label="上传照片"
								{...formItemLayout}
							>
								{
									getFieldDecorator('resourceId', {
										initialValue:resourceId
									})(
										<AvatarUpload 
											imageUrl={headImgUrl}
											onSuccess={ 
												(v)=> {
													let {
														id,
														url
													} = v;
													this.props.form.setFieldsValue({
														resourceId: id
													});
												}
											}
										/>
									)
								}
							</FormItem>
						</Col>
					</Row>
	        	</div>
		        <div className="f-border"></div>
		        <div className="f-pd5 f-right-title-box">
			        <div className="f-bg-green f-right-title">
	        			<h3>学校信息</h3>
	        		</div>
			        <Row gutter={ 40 }>
			        	<Col span={ 15 }>
			        		<Row gutter={ 40 }>
			        			<Col span={ 12 }>
		    						<FormItem
										label="学校类型"
										{...formItemLayout}
									>
										{
											getFieldDecorator('isPublicSchool', {
												rules: [{ 
													required: false, 
													message: '请选择学校类型' 
												}],
												initialValue:isPublicSchool
											})(
												<RadioGroup>
											        <Radio value='1'>公立</Radio>
											        <Radio value='0'>私立</Radio>
											    </RadioGroup>
											)
										}
									</FormItem>
		    					</Col>
			    				<Col span={ 12 }>
									<FormItem
										label="学校名称"
										{...formItemLayout}
									>
										{
											getFieldDecorator('currentSchoolName', { 
												rules: [{  
													required: false, 
													message: '请输入学校名称',
												}],
												initialValue:currentSchoolName
											})
											(
												<Input placeholder="请输入学校名称" />
											)
										}
									</FormItem>
								</Col>
			        		</Row>
			        		<Row gutter={ 40 }>
			        			<Col span={ 12 }>
		    						<FormItem
										label="年级"
										{...formItemLayout}
									>
										{
											getFieldDecorator('grade', {
												rules: [{
													required:false,
													message: '请选择年级' 
												}],
												initialValue:grade
											})(
												<Select
													size="large"
													allowClear
				                					placeholder="请选择年级"
												>
													{
														gradeList.map((d,i)=> {
															return(<Option 
																	key={i} 
																	value={d.value}
																>{d.label}</Option>);
														})
													}
												</Select>
											)
										}
									</FormItem>
		    					</Col>
		    					<Col span={ 12 }>
									<FormItem
										label="居住区域"
										{...formItemLayout}
									>
										{
											getFieldDecorator('citySelect', {
												rules: [{ 
													required: false, 
													message: '请输入居住区域' 
												}],
												initialValue:citySelect
											})(
												<SelectCity 
													onSelect={
														(v) => {
															this.props.form.setFieldsValue({
																citySelect: v
															});
														}
													}
													initialValueData={ citySelect }
												/>
											)
										}
									</FormItem>
								</Col>
			        		</Row>
			        		<Row gutter={ 4 }>
			    				<Col>
									<FormItem
										label="详细地址"
										labelCol={
											{ span:3 }
										}
										wrapperCol={
											{ span: 21 }
										}
									>
										{
											getFieldDecorator('address', {
												rules: [{ 
													required: false, 
													message: '请输入详细地址' 
												}],
												initialValue:address
											})(
												<Input placeholder="请输入详细地址" />
											)
										}
									</FormItem>
								</Col>
			        		</Row>
			        	</Col>
	    	        </Row>
		        </div>
			</div>
      	</Form>);
	}
}



const MyForm = Form.create()(MainForm);
export default MyForm;