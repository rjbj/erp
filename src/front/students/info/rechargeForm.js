import React, {
	Component
} from 'react';
import {
	Form,
	Modal,
	message,
	Input
} from 'antd';
import {
	fpostArray
} from '../../../common/io.js';
import PayTypes from '../../../common/payTypes.js';
import SelectSchool from '../../../common/selectSchool.js';

const FormItem = Form.Item;
const {
	TextArea
} = Input;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			studentId: this.props.studentId,
			saveing: false,
			trades: null,
			total: 0
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onCancel
		} = this.props;

		let {
			saveing,
			total,
			trades
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<Modal title="充值"
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
				width='660px'
			>
				<FormItem
			        label="校区"
			        {...formItemLayout}
			    >
			        {
			            getFieldDecorator('schoolAreaId', { 
			                rules: [{
			                    required: true, 
			                    message: '请选择校区' 
			                }]
			            })
			            (
		                	<SelectSchool 
								onSelect={ 
									(v)=> {
										this.props.form.setFieldsValue({
											schoolAreaId:v
										});
									}
								} 
							/>
			            )
			        }
			    </FormItem>
				<Form>
				    <FormItem
				        label="充值方式"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('trades', { 
				                rules: [{
				                    required: true, 
				                    message: '请输入充值金额' 
				                }],
				                initialValue:trades
				            })
				            (
				                <PayTypes 
		        					onInput = { this._getPayInfo.bind(this) }
		        				/>
				            )
				        }
				    </FormItem>
				    <FormItem
				        label="总计"
				        {...formItemLayout}
				    >
				        ¥{total}
				    </FormItem>
				    <FormItem
				        label="对内备注"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('internalRemark', { 
				                rules: [{
				                    required: false, 
				                }] 
				            })
				            (
				                <TextArea 
				                	rows={2} 
				                />
				            )
				        }
				    </FormItem>
				    <FormItem
				        label="对外备注"
				        {...formItemLayout}
				    >
				        {
				            getFieldDecorator('foreignRemark', { 
				                rules: [{
				                    required: false, 
				                }] 
				            })
				            (
				                <TextArea 
				                	rows={2} 
				                />
				            )
				        }
				    </FormItem>
		      	</Form>
		</Modal>);
	}
	_getPayInfo(v) {
		let trades = v.trades;
		let total = 0;

		trades.forEach((d, i) => {
			let {
				tradeAmount
			} = d;
			total += Number(tradeAmount);

		});

		this.props.form.setFieldsValue({
			trades
		});

		this.setState({
			trades,
			total
		}, () => {
			console.log(this.state);
		});
	}
	_ok(e) {
		let {
			total,
			studentId
		} = this.state;

		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				if (total <= 0) {
					message.error('请输入充值金额');
				} else if (!studentId) {
					message.error('缺少学生id');
				} else {
					values.studentId = studentId;
					this._save(values);
				}
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;


		fpostArray('/api/reception/student/studentRecharge', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk(res.result);
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;