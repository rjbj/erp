import React, {
	Component
} from 'react';
import {
	Route
} from 'react-router-dom';

import {
	default as IndexPage
} from './index/main';
import {
	default as InfoPage
} from './info/main';
import {
	default as InfoCourses
} from './courses/main';
import {
	default as InfoBefore
} from './before/main';
import {
	default as InfoAfter
} from './after/main';
import {
	default as CourseDetail
} from './detail/main';
import {
 SingRecord
} from './singRecord/main'; //以学生为纬度的单一查看某门课的上课记录

let PARENTPATH;

export default class Main extends Component {
	constructor(props) {
		super(props);
		const {
			match
		} = props;
		PARENTPATH = match.path;
	}
	render() {
		return (<section>

			<Route path={ PARENTPATH } exact component={ IndexPage } />
			<Route path={ `${ PARENTPATH }/info` } component={ InfoPage } />
			<Route path={ `${ PARENTPATH }/courses` } component={ InfoCourses } />
			<Route path={ `${ PARENTPATH }/before` } component={ InfoBefore } />
			<Route path={ `${ PARENTPATH }/after` } component={ InfoAfter } />
			<Route path={ `${ PARENTPATH }/detail` } component={ CourseDetail } />
			{/*以学生为纬度的单一查看某门课的上课记录*/}
			<Route path={ `${ PARENTPATH }/singRecord` }  component={ SingRecord } /> 
		</section>);
	}
}