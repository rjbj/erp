import React, {
	Component
} from 'react';
import {
	numberFormate
} from '../../../../common/g.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		let {
			data
		} = this.props;
		if (data) {
			data = data.studentVO;
		}
		console.log(data);

		if (!data) {
			return null;
		}

		let {
			name,
			headImgUrl,
			genderStr,
			stuNo,
			birthday,
			motherPhone,
			fatherPhone,
			otherPhone,
			registrationTime,
			sourceStr,
			salesName,
			currentSchoolName,
			balanceAmount = 0,

			isPublicSchoolStr,
			gradeStr,
			provinceName,
			cityName,
			districtName,
			address

		} = data;

		balanceAmount = numberFormate(balanceAmount);

		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white">
			<div className="f-pd5 f-right-title-box">
				<div className="f-bg-blue f-right-title">
					<h3>学生详情信息</h3>
				</div>
	        	<div className="f-clear f-flex">
			        <ul className="f-clear info-list info-bd-top">
			        	<li><span className="f-pale f-mr2">学员姓名:</span>{name}</li>
						<li><span className="f-pale f-mr2">性别:</span>{genderStr}</li>
						<li><span className="f-pale f-mr2">学号:</span>{stuNo}</li>
						<li><span className="f-pale f-mr2">学员生日:</span>{birthday}</li>
						<li><span className="f-pale f-mr2">母亲电话:</span>{motherPhone}</li>
						<li><span className="f-pale f-mr2">父亲电话:</span>{fatherPhone}</li>
						<li><span className="f-pale f-mr2">其他电话:</span>{otherPhone}</li>
						<li><span className="f-pale f-mr2">注册时间:</span>
							{registrationTime}
						</li>
						<li><span className="f-pale f-mr2">来源:</span>
							{sourceStr}
						</li>
						<li><span className="f-pale f-mr2">销售员:</span>
							{salesName}
						</li>
						<li>
							<span className="f-pale f-mr2">余额:</span>
							<i className="f-cny f-mr2">¥ {balanceAmount || 0}</i>
						</li>
			        </ul>
			        <div className="f-right">
			        	<span className="f-pale f-mr2 f-left">头像:</span>
			        	<span className="avatar f-over-hide f-radius2" >
			        	{
			        		headImgUrl ?
			        			<img style={{width:'100%'}} src={headImgUrl} />
			        		: null
			        	}
			        	</span>
			        </div>
		        </div>
        	</div>
	        <div className="f-border"></div>
	        <div className="f-pd5 f-right-title-box">
		        <div className="f-bg-green f-right-title">
        			<h3>学校信息</h3>
        		</div>
		        <ul className="f-clear info-list info-bd-top">
		        	<li><span className="f-pale f-mr2">学校类型:</span>{isPublicSchoolStr}</li>
					<li><span className="f-pale f-mr2">学校名称:</span>{currentSchoolName}</li>
					<li><span className="f-pale f-mr2">年级:</span>{gradeStr}</li>
					<li><span className="f-pale f-mr2">居住区域:</span>{provinceName + cityName + districtName + address}</li>
		        </ul>
	        </div>

		</div>);
	}
}