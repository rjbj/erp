import React, {
	Component
} from 'react';
import {
	Col,
	Row,
	Breadcrumb,
	message
} from 'antd';
import {
	Link
} from 'react-router-dom';
import {
	default as MyList
} from './list.js';
import {
	default as SearchForm
} from './searchform.js';
import {
	fpost
} from '../../common/io.js';
import {
	Loading,
	NoData
} from '../../common/g.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {}
		}
	}
	render() {
		let {
			result,
			loading,
			pagination
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">物品售卖</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<Row type="flex" gutter={24}>
				<Col span={19}>
					<SearchForm
						ref={ form => this.searchForm = form }
						onSearch= { this._search.bind(this) }
					/>
				</Col>
				<Col span={5}>
					<header style={{height:'100%'}} className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-align-center">
						<h3 className="f-mb4">外部人员购买请点击</h3>
						<Link to="/front/salegoods" className="f-btn-green">
							教材杂费零售
						</Link>
					</header>
				</Col>
			</Row>

			

			<MyList 
				data={result}
		        loading={loading}
		        onChange={this._getList.bind(this)}
		        pagination={pagination}
			/>

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_search(values) { //搜索
		console.log(values);
		this.setState({
			searchParma: values,
			currentPage: 1,
			pagination: {
				...this.state.pagination,
				current: 1,
			}
		}, () => {
			this._getList();
		});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		console.log('pager', pager);

		fpost('/api/reception/student/pageStudent', searchParma)
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;
				data.forEach((d) => {
					d.key = d.id
				});

				let total = Number(result.total);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						current: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				}, () => {
					console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
}