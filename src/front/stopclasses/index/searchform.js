import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Col,
	Row,
	Form,
	DatePicker,
	Select
} from 'antd';
import {
	default as SelectPerson
} from '../../../common/selectPerson.js';
import SelectCourses from '../../../common/selectCourses.js';
import {
	fromData
} from '../../../common/staticData.js';
import {
	undefinedToEmpty
} from '../../../common/g.js';
const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: true
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			visible
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		if (!visible) {
			return null;
		}

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form>
    			<Row gutter={ 40 }>
    				<Col span={10}>
    					<FormItem
							label="学员"
							{...formItemLayout}
						>
							{
								getFieldDecorator('studentName', { 
									rules: [{
										required: false, 
										message: '请输入学员姓名' 
									}] 
								})
								(
									<Input placeholder="请输入学员姓名" />
								)
							}
						</FormItem>
    				</Col>
    				<Col span={ 7 }>
						<FormItem
							label="手机"
							{...formItemLayout}
						>
							{
								getFieldDecorator('phone', { 
									rules: [{
										required: false, 
										message: '请输入手机号码' 
									}] 
								})
								(
									<Input placeholder="请输入手机号码" />
								)
							}
						</FormItem>
					</Col>
					<Col span={7}>
    					<FormItem
							label="课程名称"
							{...formItemLayout}
						>
							{
								getFieldDecorator('courseId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectCourses 
										onSelect= {
											(v)=> {
												this.props.form.setFieldsValue({
													courseId:v
												});
											}
										}
									/>
								)
							}
						</FormItem>
    				</Col>
    	        </Row>
    	        <Row gutter={ 40 }>
    				<Col span={ 10 }>
						<FormItem
							label="上课老师"
							{...formItemLayout}
						>
							{
								getFieldDecorator('salesId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectPerson
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{ 
												type:4
											}
										} 
										onSelect={ 
											(v)=> {
												v = v ? v : null;
												this.props.form.setFieldsValue({
													salesId: v
												});
											} 
										}
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={ 7 }>
						<FormItem
							label="班级名称"
							{...formItemLayout}
						>
							{
								getFieldDecorator('className', {
									rules: [{  
										message: '请输入班级名称' 
									}]
								})(
									<Input placeholder="请输入班级名称" />
								)
							}
						</FormItem>
					</Col>
    	        </Row>
    	        <div className="f-align-center">
    	        	<Button 
    	        		size="large" 
    	        		className="f-mr4" 
    	        		onClick={ this._reset.bind(this) }
    	        	>重置</Button>
    	        	<Button 
    	        		type="primary"
    	        		size="large" 
    	        		icon="search" 
    	        		onClick={ this._search.bind(this) }
    	        	>查询</Button>
    	        </div>
	      	</Form>
		</header>);
	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			visible: false
		}, () => {
			this.setState({
				visible: true
			}, () => {
				this._search();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;