import React, {
	Component
} from 'react';
import {
	Col,
	Row,
	Form,
	Select
} from 'antd';

import {
	payTypes
} from '../../../../common/staticData.js';
import {
	default as NumberInput
} from '../../../../common/numberInput.js';

const FormItem = Form.Item;
const Option = Select.Option;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isBalanceRefund: this.props.data.isBalanceRefund
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			data
		} = this.props;
		let {
			amount,
			resultAmount,
			tradeWay
		} = data;
		let {
			isBalanceRefund
		} = this.state;

		amount = amount ? amount : 0;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
	        <div className="f-pd5 f-right-title-box">
		        <div className="f-bg-green f-right-title">
        			<h3>费用信息</h3>
        		</div>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right">应退: </Col>
        			<Col span={20}><i className="f-fz5 f-red">¥{resultAmount}</i></Col>
        		</Row>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right f-lh6">实退: </Col>
        			<Col span={20}>
	                    <FormItem>
		                    {
								getFieldDecorator('amount', {
									rules: [{
										required:true,
									}],
									initialValue: { 
										number: amount || 0, 
										currency: 'rmb',
										min:0,
		                            	max:resultAmount
									},
								})(
									<NumberInput
										style={{
											width:'120px'
										}}
									/>
								)
							}
		                </FormItem>
        			</Col>
        		</Row>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right f-lh6">退款方式: </Col>
        			<Col span={20}>
	                    {/*<FormItem>
		                    {
		                        getFieldDecorator('isBalanceRefund', {
		                            rules: [{
		                                required: true,
		                                message:'请选择退款方式'
		                            }],
		                            initialValue:isBalanceRefund
		                        })
		                        (
		                            <RadioGroup
		                            	onChange={
		                            		(e)=> {
		                            			let v = e.target.value;
		                            			this.setState({
		                            				isBalanceRefund:v
		                            			});
		                            		}
		                            	}
		                            >
			    				        <Radio value={1}>转余额</Radio>
			    				        <Radio value={0}>退款</Radio>
			    				    </RadioGroup>
		                        )
		                    }
		                </FormItem>*/}
		                <span className="f-lh6">转余额</span>
        			</Col>
        		</Row>
        		{
        			isBalanceRefund == '0' ?
		        		<Row gutter={16}>
		        			<Col span={2} className="f-align-right f-lh6">支付方式: </Col>
		        			<Col span={20}>
			                    <FormItem
				                    {...formItemLayout}
				                >
				                    {
				                        getFieldDecorator('tradeWay', { 
				                            rules: [{
				                                required: true, 
				                                message: '请选择提现方式' 
				                            }] 
				                        })
				                        (
				                            <Select
							                	style={{
							                		width:'200px'
							                	}}
							                	size="large" 
							                	placeholder="请选择提现方式"
						                	>
							                    {
							                        payTypes.map((d,i)=> {
							                            return <Option key={i} value={d.value}>{d.label}</Option>
							                        })
							                    }
							                </Select>
				                        )
				                    }
				                </FormItem>
		        			</Col>
		        		</Row>
		        	: null
        		}
	        </div>
        </div>);
	}
}