import React, {
	Component
} from 'react';
import {
	Col,
	Row,
} from 'antd';

import PayTypes from '../../../../common/payTypes.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		let {
			data
		} = this.props;
		let {
			resultAmount,
		} = data;

		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
	        <div className="f-pd5 f-right-title-box">
		        <div className="f-bg-green f-right-title">
        			<h3>费用信息</h3>
        		</div>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right">应补: </Col>
        			<Col span={20}><i className="f-fz5 f-red">¥{resultAmount}</i></Col>
        		</Row>
        		<Row gutter={16}>
        			<Col span={2} className="f-align-right f-lh6">支付方式: </Col>
        			<Col span={20}>
        				<PayTypes
        					onInput = { this.props.onInput }
        				/>
        			</Col>
        		</Row>
	        </div>
		</div>);
	}
}