import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Row,
	Col,
	Select,
	Button,
	Form,
	message,
	Modal
} from 'antd';

import {
	default as PayFormIn
} from './pay/in.js';
import {
	default as PayFormOut
} from './pay/out.js';
import {
	default as OtherForm
} from './other/main.js';
import {
	fpost,
	fpostArray
} from '../../../common/io.js';
import {
	getUrlParam
} from '../../../common/g.js';
import {
	default as NumberInput
} from '../../../common/numberInput.js';

const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false,
			studentId: getUrlParam('studentId'),
			registrationCourseId: getUrlParam('registrationCourseId'),
			classStateName: ['-', '已排课未开课', '已开课', '已结课'], //用户展示班级状态
			trades: [], //应补时候支付方式

			amountType: '', //类型 0:不退不补 1:退 2:补
			restAmount: '', //剩余学费金额
			amount: 0, //实退金额
			resultAmount: '', //应退金额
			offsetBalanceAmount: '', //余额抵扣金额
			availableBalanceAmount: '', //可用余额
			receivableAmount: 0, //应收学费
			isArrear: '', //是否欠款
			courseName: '', //课程名称
			totalPeriod: '', //课程总期数
			registrationCourseState: '',
			classState: '', //班级状态 1:已排课未开课、2:已开课、3:已结课
			className: '', //班级名称
			consumeCalculateType: '1', //课消计算类型 1-原价 2-应收
			coursePeriod: '', //课程课时总数
			arrearAmount: '', //欠款金额
			currentPeriod: '', //当前第几期
			consumeCoursePeriod: '', //消耗课时数
			consumedAmount: '', //课消金额

			tradeWay: '', //退费方式
			isBalanceRefund: 1, //是否转余额 0-不转 1-转余额
			internalRemark: '', //对内备注
			foreignRemark: '', //对外备注

		}
		this._checkConsumedAmount = this._checkConsumedAmount.bind(this); //校验课消金额
		this._checkOffsetBalanceAmount = this._checkOffsetBalanceAmount.bind(this); //校验使用余额

	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			registrationCourseId,
			studentId,
			amount,
			resultAmount,
			restAmount,
			courseName,
			currentPeriod,
			isArrear,
			arrearAmount,
			registrationCourseState,
			classStateName,
			receivableAmount,
			consumeCoursePeriod,
			amountType,
			availableBalanceAmount,
			consumeCalculateType,
			consumedAmount,
			offsetBalanceAmount,
			className,
			classState,
		} = this.state;

		return (<Form layout="inline" onSubmit={this._handleSubmit.bind(this)}>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>学员管理</Breadcrumb.Item>
			    <Breadcrumb.Item>办理停课</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">停课</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-radius1 f-over-hide f-bg-white f-box-shadow2">
				<Row gutter={12} type="flex">
					<Col span={19}>
						<div className="f-pd5 f-left-title-box" style={{height:'100%'}}>
					        <div className="f-bg-green f-left-title">
			        			<h3>课程信息</h3>
			        		</div>

			        		<div className="f-mb2">
			        			<span className="gradeName f-fz5">{courseName}</span>
			        			{
			        				currentPeriod && currentPeriod > 0 ? 
			        					<span className="f-ml2">第{currentPeriod}期</span>
			        				: null
			        			}
			        			{
			        				isArrear == '1' ?
					        			<span className="f-ml4">
						        			<i className="f-round f-bg-orange f-white f-round-tip f-mr2">欠</i>
						        			<a 
						        				href={`/front/fillmoney/form?registrationCourseId=${registrationCourseId}&studentId=${studentId}`}
						        				className="f-red"
					        				>学员欠费{arrearAmount}元，点此查看</a>
					        			</span>
					        		: null
			        			}
			        			<span className="f-ml5 f-mr2">{className}</span>
			        			{
			        				registrationCourseState ?
					        			<span className="f-green">
					        				{ classStateName[classState] }
					        			</span>
					        		: null
			        			}
			        		</div>

			        		<div className="f-lh6 f-mb4 f-mt4">
			        			<span>学费¥{receivableAmount}</span>
			        			<span className="f-ml2 f-mr2">-</span>
			        			<span className="f-mr2">已上{consumeCoursePeriod}课时</span>
			                    <FormItem>
				                    {
				                        getFieldDecorator('consumeCalculateType', { 
				                            rules: [{
				                                required: true, 
				                                message: '请选择课消方式' 
				                            }],
				                            initialValue:consumeCalculateType
				                        })
				                        (
				                            <Select
							                	style={{
							                		width:'120px'
							                	}}
							                	onChange= {
							                		(v)=> {
							                			this.props.form.setFieldsValue({
							                				offsetBalanceAmount: {
							                					number: 0
							                				}
							                			});
							                			this.setState({
							                				consumeCalculateType:v,
							                				consumedAmount:'',
							                				offsetBalanceAmount: 0
							                			},()=> {
							                				this._initData();
							                			});
							                		}
							                	}
							                	size="large" 
							                	placeholder="请选择课消方式"
						                	>
							                    <Option value="1">原价课消</Option>
			        							<Option value="2">优惠课消</Option>
							                </Select>
				                        )
				                    }
				                </FormItem>
			                    {
            	        			receivableAmount && consumedAmount !== '' ? 
            		                    <FormItem>
            								{
            									getFieldDecorator('consumedAmount', {
            										rules: [{
            											required:true,
            											validator: this._checkConsumedAmount 
            										}],
            										initialValue: { 
            											number: consumedAmount || 0, 
            											currency: 'rmb',
            											min:0,
            			                            	max:receivableAmount
            										},
            									})(
            										<NumberInput
            											style={{
            												width:'120px'
            											}}
        											/>
        										)
            								}
					        				<span className="f-ml2 f-mr2">=</span>
						        			<span>
						        				剩余学费¥{restAmount}
						        			</span>
                                        </FormItem>
                                    : null
            	        		}
			        		</div>
			        		{
			        			(restAmount < arrearAmount) ?
					        		<div className="f-lh6">
					        			<span className="f-mr5">
											使用余额：
		    			                    <FormItem>
                								{
                									getFieldDecorator('offsetBalanceAmount', {
                										rules: [{ 
                											validator: this._checkOffsetBalanceAmount
                										}],
                										initialValue: { 
                											number: offsetBalanceAmount || 0, 
                											currency: 'rmb',
                											min:0,
                			                            	max:(arrearAmount - restAmount) || 0
                										},
                									})(
                										<NumberInput
                											style={{
                												width:'120px'
                											}}
            											/>
            										)
                								}
												<span className="f-ml2">可用¥{availableBalanceAmount}</span>
		    				                </FormItem>
										</span>
					        		</div>
				        		: null
			        		}
				        </div>
					</Col>
					<Col span={5}>
						<div className="price-tip">
							<dl>
								<dt>费用</dt>
								<dd>
									<span className="label">
										<i className="operator"/>
										剩余学费
									</span>
									<span className="f-red">¥{restAmount}</span>
								</dd>
								{
									arrearAmount ? 
										<dd>
											<span className="label">
												<i className="operator">-</i>
												欠费
											</span>
											<span className="f-red">¥{arrearAmount}</span>
										</dd>
									: null
								}
								{
			        				amountType == '2' ?
									<dd>
										<span className="label">
											<i className="operator">-</i>
											余额抵用
										</span>
										<span className="f-red">¥{offsetBalanceAmount || 0}</span>
									</dd>
									: null
								}
								<dd>
									<span className="label">
										<i className="operator">=</i>
										<i className="f-fz5 f-red">
											¥
											{
												amountType == '2' ?
													<i className="operator">-</i>
												: null
											}
											{resultAmount}
										</i>
									</span>
								</dd>
							</dl>
						</div>
					</Col>
				</Row>
			</div>

			{
				amountType == '2' ?
					<PayFormIn 
						form = { this.props.form }
						onInput = { this._getPayData.bind(this) }
						data= { this.state }
					/>
				: null
			}
			{
				amountType == '1' ?
					<PayFormOut 
						form = { this.props.form }
						data = { this.state }
					/>
				: null
			}

			<OtherForm
				getData={
					(d)=> {
						this.setState({
							...d
						});
					}
				}
				form= { this.props.form } 
			/>

			<div className="f-align-right f-mt5">
				<Button 
					size="large" 
					className="f-mr5"
					onClick={
						()=> {
							window.location.href ="/front/stopclasses";
						}
					}
				>
					取消
				</Button>
				<Button 
					size="large" 
					type="primary" 
					htmlType="submit"
					loading={ this.state.saveing }
				>
					办理停课
				</Button>
			</div>

		</Form>);
	}
	componentDidMount() {
		this._initData();
	}
	_initData() {
		let {
			registrationCourseId,
			consumeCalculateType,
			consumedAmount,
			offsetBalanceAmount,
			consumeCoursePeriod,
			resultAmount,
			arrearAmount
		} = this.state;

		offsetBalanceAmount = resultAmount >= arrearAmount ? 0 : offsetBalanceAmount;

		let param = {
			registrationCourseId,
			consumeCalculateType,
			consumedAmount,
			offsetBalanceAmount,
			consumeCoursePeriod
		}

		fpost('/api/reception/transaction/findWithCourseStop', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					...result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_handleSubmit(e) {
		e.preventDefault();

		this.props.form.validateFields((err, values) => {
			console.log(values);
			if (!err) {
				console.log('Received values of form: ', values);
				values.offsetBalanceAmount = values.offsetBalanceAmount && values.offsetBalanceAmount.number;
				values.consumedAmount = values.consumedAmount && values.consumedAmount.number;
				values.amount = values.amount && values.amount.number;
				let {
					trades,
					amountType
				} = this.state;
				if (amountType == '1') { //如果是退款
					let {
						tradeWay,
						amount
					} = values;
					trades = [{
						tradeType: tradeWay,
						tradeAmount: amount
					}]
				}

				let state = {
					...this.state,
					...values,
					trades
				}
				this.setState({
					...state
				}, () => {
					this._validator();
				});
			}
		});
	}
	_checkConsumedAmount(rule, value, callback) { //校验课消金额
		if (this.timer) {
			clearTimeout(this.timer);
		}
		let {
			number
		} = value;
		let {
			receivableAmount,
			consumedAmount
		} = this.state;

		if (number === '') {
			callback('请输入课消金额!');
			return;
		}

		if (number >= 0 && number <= receivableAmount) {
			this.setState({
				consumedAmount: number,
				offsetBalanceAmount: 0
			}, () => {
				this.props.form.setFieldsValue({
					offsetBalanceAmount: {
						number: 0
					}
				});
				if (consumedAmount !== number) {
					this.timer = setTimeout(() => {
						this._initData()
					}, 500);
				}
			});

			callback();
		} else {
			callback('请输入课消金额!');
		}

	}
	_checkOffsetBalanceAmount(rule, value, callback) { //校验使用余额
		if (this.timer) {
			clearTimeout(this.timer);
		}
		let {
			number
		} = value;
		let {
			availableBalanceAmount,
			offsetBalanceAmount
		} = this.state;

		if (number >= 0 && number <= availableBalanceAmount) {
			this.setState({
				offsetBalanceAmount: number
			}, () => {
				if (offsetBalanceAmount !== number) {
					this.timer = setTimeout(() => {
						this._initData()
					}, 500);
				}
			});

			callback();
		} else {
			callback('请输入课消金额!');
		}

	}
	_validator() {
		let {
			trades,
			amountType
		} = this.state;

		if (amountType == '2' && (!trades || !trades.length)) {
			message.error('请选择支付方式');
			return;
		};

		this._save();
	}
	_save() {
		let {
			studentId
		} = this.state;
		// console.log(this.state);
		this.setState({
			saveing: true
		});
		fpostArray('/api/reception/transaction/saveWithCourseStop', this.state)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					saveing: false
				});
				// confirm({
				// 	title: '温馨提醒',
				// 	content: '恭喜您操作成功!',
				// 	onOk() {
				// 		window.history.back();
				// 	},
				// 	onCancel() {
				// 		window.history.back();
				// 	},
				// });
				let {
					showPrint,
					businessId,
					businessType,
					financeFundFlowIds
				} = result;

				if (showPrint) {
					window.location.href = `/print?type=1&businessType=${businessType}&businessId=${businessId}&financeFundFlowIds=${financeFundFlowIds}`
				} else {
					// window.location.href = `/front/students/courses?studentId=${studentId}`
					window.location.href = "/front/registrationCourse";
				}
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
	_getPayData(v) { //支付信息
		let {
			trades
		} = v;
		this.setState({
			trades
		}, () => {
			console.log('支付信息', this.state);
		});
	}
}

const MyForm = Form.create()(MainForm);
export default MyForm;