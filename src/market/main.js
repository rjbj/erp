import React, {
	Component
} from 'react';
import {
	Route
} from 'react-router-dom';
import {
	default as Layout
} from '../common/layout.js';
import {
	default as Client
} from './Client.js';
import {
	default as ViewCommunication
} from './client/ViewCommunication.js';
import {
	default as New
} from './New.js';
import {
	default as Communication
} from './Communication.js';
import {
	default as Change
} from './Change.js';

import './main.less';


export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'scyy'
	})
	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	render() {
		return (
			<div>
				<Route exact path={`/market`} component={Client} />
				<Route exact path={`/market/client`} component={Client} />
				<Route path={`/market/client/viewCommunication`} component={ViewCommunication} />
				<Route path={`/market/new`} component={New} />
				<Route path={`/market/communication`} component={Communication} />
				<Route path={`/market/change`} component={Change} />
			</div>
		);
	}
}