import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Breadcrumb,
	Tabs,
	Row,
	Col,
	Icon,
	Button,
	Input,
	Select,
	Form,
	Table,
	Popover,
	message,
	Upload,
	Modal,
	Menu,
	Dropdown
} from 'antd';
import reqwest from 'reqwest';
import {
	fpost,
	hostname
} from '../common/io.js';
import {
	Loading,
	Pagination,
	undefinedToEmpty
} from '../common/g.js';

const TabPane = Tabs.TabPane;
const Option = Select.Option;
const FormItem = Form.Item;

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleType: false,
			clientDataList: null,
			urlStr: '',
			tabKey: 1,
			total: 1,
			searchPara: {
				currentPage: 1,
				pageSize: 10,
				name: '',
				phone: '',
				isValid: '',
				status: ''
			}
		}
	}
	showModal() {
		this.setState({ //确定对话框的展示
			visibleType: true
		})
	}
	cancelModal() {
		this.setState({ //确定对话框的展示
			visibleType: false
		})
	}
	getDataList(para) {
		let tabKey = this.state.tabKey;
		fpost('/api/marketing/pagePotentialStudent', para).then(res => res.json()).then(res => {
			if (res.success == true) {
				let arr = [];
				res.result.records.map((e, i) => {
					e.key = i;
					arr.push(e);
				})
				this.setState({
					clientDataList: arr,
					urlStr: tabKey == 1 ? ('?name=' + para.name + '&phone=' + para.phone) : ('?isValid=' + para.isValid + '&status=' + para.status),
					total: res.result.total
				})
			} else {
				message.error(res.message)
			}
		});
	}
	setCondition(v) {
		this.setState({
			tabKey: v
		})
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount() {
		this.getDataList(this.state.searchPara);
	}
	render() {

		const columns = [{
			title: '客户姓名',
			dataIndex: 'name',
			key: 'name',
			className: 'f-align-center',
			width: '100px',
			fixed: 'left'
		}, {
			title: '头像',
			key: 'headImgUrl',
			width: '110px',
			className: 'f-align-center f-line1',
			render: (text, record) => (
				record.headImgUrl ? <img src={record.headImgUrl} width={50} height={50} className='f-round'/> : ''
			)
		}, {
			title: '性别',
			dataIndex: 'genderStr',
			key: 'genderStr',
			className: 'f-align-center f-line1',
		}, {
			title: '年龄',
			key: 'age',
			dataIndex: 'age',
			className: 'f-align-center f-line1',
		}, {
			title: '手机号码',
			dataIndex: 'phone',
			key: 'phone',
			className: 'f-align-center f-line1',
		}, {
			title: '是否有效',
			key: 'isValidStr',
			className: 'f-align-center f-line1',
			render: (text, record) => (
				<div>
			      {
			      	record.isValidStr=='是'? <span className='f-block'>是</span> : ''
			      }
			      {
			      	record.isValidStr=='否'? <span className='f-block'>否 <Popover 
				        content={<p>{ record.invalidReason }</p>}
				        trigger="click"
				      >
				        <a href='javascript:void(0)' className='iconfont icon-yiwen f-fz5' style={{ verticalAlign:'-1px', display: record.invalidReason.length? 'block' : 'none' }}></a>
				      </Popover></span> : ''
			      }
				</div>
			)
		}, {
			title: '邀约次数',
			dataIndex: 'connectCount',
			key: 'connectCount',
			className: 'f-align-center f-line1',
		}, {
			title: '诺访率',
			dataIndex: 'confirmRate',
			key: 'confirmRate',
			className: 'f-align-center f-line1',
		}, {
			title: '到访率',
			dataIndex: 'visitRate',
			key: 'visitRate',
			className: 'f-align-center f-line1',
		}, {
			title: '市场来源',
			dataIndex: 'sourceStr',
			key: 'sourceStr',
			className: 'f-align-center f-line1',
		}, {
			title: '市场顾问(工号)',
			dataIndex: 'salesName',
			key: 'salesName',
			className: 'f-align-center f-line1',
		}, {
			title: '跟进状态',
			key: 'statusStr',
			className: 'f-align-center f-line1',
			render: (text, record) => (
				<span className={ record.statusStr=='待跟进'? 'f-orange' : record.statusStr=='已跟进'? 'f-blue' : '' }><i className='iconfont icon-dian'></i>{record.statusStr}</span>
			)
		}, {
			title: '操作人',
			dataIndex: 'operatorName',
			key: 'operatorName',
			className: 'f-align-center f-line1',
		}, {
			title: '操作时间',
			dataIndex: 'modifyTime',
			key: 'modifyTime',
			className: 'f-align-center f-line1',
		}, {
			title: '操作',
			key: 'action',
			className: 'f-align-center',
			width: '150px',
			render: (text, record) => (
				<div>
					<i className='iconfont icon-bianji1 f-pointer f-mr3 f-fz5' onClick={ ()=>{ window.location.href='/market/new?id='+record.id } }></i>
					<Dropdown overlay={(<Menu>
					    <Menu.Item>
					      <a rel="noopener noreferrer" href={'/market/client/viewCommunication?potentialStudentName='+record.name+'&motherPhone='+record.phone+'&potentialStudentId='+record.id}>查看沟通</a>
					    </Menu.Item>
					    <Menu.Item>
					      <a rel="noopener noreferrer" href={'/front/signup/step1?type=2&id='+record.id}>办报名</a>
					    </Menu.Item>
					  </Menu>)}>
						<Button type="primary" className="f-radius4">查看<Icon type="down" /></Button>
					</Dropdown>
				</div>
			),
			fixed: 'right'
		}];
		return (<div className='classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>市场运营</Breadcrumb.Item>
					    <Breadcrumb.Item>客户管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>客户列表</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
						<Tabs onChange={ this.setCondition.bind(this) } defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false}>
							<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
					    		<ExactSearchForm pageSize={this.state.searchPara.pageSize} setPara={ this.setPara.bind(this) } updateMainData={this.getDataList.bind(this)}/>	
					    	</TabPane>
						    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
						    	<AdvancedSearchForm pageSize={this.state.searchPara.pageSize} setPara={ this.setPara.bind(this) } updateMainData={this.getDataList.bind(this)}/>	
						    </TabPane>
						 </Tabs>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt1 f-radius1 f-mt5'>
						<div className='f-bg-white f-radius1'>
							<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
								<h3 className="f-title-blue">客户列表</h3>
								<div>
									<a className="f-btn-orange" target='_blank' href={hostname()+'/api/marketing/exportPotentialStudent'+this.state.urlStr+'&x-auth-token='+store.get('sessionId')}>
										<i className='iconfont icon-daoru1-copy f-mr2'></i>导出客户名单
									</a>
									<a className="f-btn-blue f-ml3" onClick={this.showModal.bind(this)}>
										<i className='iconfont icon-daoru f-mr2'></i>导入客户名单
									</a>
									<a className="f-btn-green f-ml3" href="/market/new">
										<i className='iconfont icon-tianjiajiahaowubiankuang f-mr2'></i>创建客户
									</a>
								</div>
							</div>
							{
								this.state.clientDataList != null?
								<Table columns={columns} dataSource={this.state.clientDataList} pagination={ Pagination(this.state.total, this.state.searchPara, this.getDataList.bind(this)) } bordered className='f-align-center' scroll={{ x: 1370 }}/>
								: <Loading />
							}
						</div>
						<ImportOutCreate visible={this.state.visibleType} cancel={this.cancelModal.bind(this)} searchPara={this.state.searchPara} updateDataList={this.getDataList.bind(this)}/>
					</div>
				</div>)
	}
}

class ExactSearch extends Component {
	exactCondition() {
		let obj = this.props.form.getFieldsValue();
		obj.currentPage = 1;
		obj.pageSize = this.props.pageSize;

		obj = undefinedToEmpty(obj);
		this.props.updateMainData(obj);
		this.props.setPara(obj);
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		return (<Form style={{ textAlign:'left' }}>
					<FormItem style={{ width:'25%', marginLeft:'30px' }} className='f-inline-block'>
						{getFieldDecorator('name', {
				            rules: [{
				              required: false,
				              message: '请输入客户姓名',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入客户姓名" />
				          )}
					</FormItem>
					<FormItem style={{ width:'25%', marginLeft:'30px' }} className='f-inline-block'>
						{getFieldDecorator('phone', {
				            rules: [{
				              pattern: /(^1[3|4|5|8][0-9]\d{4,8}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
				              message: '请输入正确的号码'
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入手机号码" />
				          )}
					</FormItem>
					<FormItem className='f-inline-block f-ml4'>
						<Button type="primary" icon="search" style={{height:'40px'}} onClick={ this.exactCondition.bind(this) }>搜索</Button>
					</FormItem>
				</Form>)
	}
}
class AdvancedSearch extends Component {
	resetForm() {
		let obj = {
			currentPage: 1,
			pageSize: this.props.pageSize,
			isValid: '',
			status: ''
		};
		this.props.form.resetFields();
		this.props.updateMainData(obj);
		this.props.setPara(obj);
	}
	AdvancedCondition() {
		let obj = this.props.form.getFieldsValue();
		obj.currentPage = 1;
		obj.pageSize = this.props.pageSize;
		obj.isValid = obj.isValid ? obj.isValid : '';
		obj.status = obj.status ? obj.status : '';

		obj = undefinedToEmpty(obj);
		this.props.updateMainData(obj);
		this.props.setPara(obj);
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		return (<Form style={{ textAlign:'left' }}>
					<FormItem className='f-inline-block' style={{ marginLeft:'30px' }} label='是否有效' wrapperCol={{ style: { width:'30%', display:'inline-block', verticalAlign:'middle' } }}>
						{getFieldDecorator('isValid', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: undefined
				          })(
	                    	<Select
	            			    className='f-vertical-middle f-ml3'
	            			    style={{ width: 200 }}
	            			    placeholder="请选择"
	            			    optionFilterProp="children"
	            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
	            			  >
	            			    <Option value="1">是</Option>
	            			    <Option value="0">否</Option>
	            		  </Select>
				          )}
					</FormItem>
					<FormItem className='f-inline-block' style={{ marginLeft:'30px' }} label='跟进状态' wrapperCol={{ style: { width:'30%', display:'inline-block', verticalAlign:'middle' } }}>
						{getFieldDecorator('status', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: undefined
				          })(
	                    	<Select
	            			    className='f-vertical-middle f-ml3'
	            			    style={{ width: 200 }}
	            			    placeholder="请选择"
	            			    optionFilterProp="children"
	            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
	            			  >
	            			    <Option value="0">待跟进</Option>
	            			    <Option value="1">已跟进</Option>
	            		  </Select>
				          )}
					</FormItem>
					<FormItem className='f-inline-block' style={{ marginLeft:'30px' }}>
						<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.AdvancedCondition.bind(this) }>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
	        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
	        				<i className='iconfont icon-reset f-mr2'></i>
	        				重置
	        			</Button>
					</FormItem>
				</Form>)
	}
}
class ImportOut extends Component {
	constructor(props) {
		super(props)
		this.state = {
			fileList: [],
			uploading: false,
			isRequest: false
		}
	}
	saveData() { //对话框里的确定按钮对应事件
		let isError = false;

		this.setState({
			isRequest: true
		});

		this.props.form.validateFields(
			(err, fieldsValue) => {
				if (err) {
					message.error('请填写完整!');
					isError = true;
				}
			}
		)

		if (isError) return;

		this.handleUpload()
	}
	handleUpload() {
		const {
			fileList
		} = this.state, recruitSource = this.props.form.getFieldsValue().recruitSource, self = this;
		const formData = new FormData();

		if (fileList.length == 0) {
			message.warning('请先上传文件!');
			return;
		}

		fileList.forEach((file) => {
			formData.append('file', file);
		});
		formData.append('recruitSource', recruitSource);
		this.setState({
			uploading: true,
		});

		// You can use any AJAX library you like
		reqwest({
			url: hostname() + '/api/marketing/uploadPotentialStudentBatch',
			method: 'post',
			processData: false,
			data: formData,
			headers: {
				'x-auth-token': store.get('sessionId'),
			},
			success: (res) => {
				if (res.success == true) {
					let searchPara = this.props.searchPara;
					searchPara.currentPage = 1;
					this.setState({
						fileList: [],
						uploading: false,
						visibleType: false
					});
					this.props.cancel();
					message.success(<span dangerouslySetInnerHTML={{__html: res.message}}></span>);
					this.props.updateDataList(searchPara);
				} else {
					message.error(<span dangerouslySetInnerHTML={{__html: res.message}}></span>);
				}
				self.setState({
					isRequest: false
				})
			},
			error: (res) => {
				this.setState({
					uploading: false,
				});
				message.error(<span dangerouslySetInnerHTML={{__html: res.message}}></span>);
			},
		});
	}
	cancelModal(e) { //对话框里的取消按钮对应的事件
		this.props.cancel();
		this.props.form.resetFields(); //重置
		this.setState({
			fileList: []
		});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const props = {
			action: hostname() + '/api/marketing/uploadPotentialStudentBatch',
			onRemove: (file) => {
				this.setState(({
					fileList
				}) => {
					const index = fileList.indexOf(file);
					const newFileList = fileList.slice();
					newFileList.splice(index, 1);
					return {
						fileList: newFileList,
					};
				});
			},
			beforeUpload: (file) => {
				if (this.state.fileList.length == 1) {
					message.warning('只能上传一个表单哦!');
					return false;
				}
				this.setState(({
					fileList
				}) => ({
					fileList: [...fileList, file],
				}));
				return false;
			},
			fileList: this.state.fileList,
		}
		return (<Modal
		          title='操作'
		          visible={this.props.visible}
		          onOk={this.saveData.bind(this)}
		          onCancel={this.cancelModal.bind(this)}
		          confirmLoading={ this.state.isRequest }
		          footer={[
		            <Button key="back" size="large" onClick={this.cancelModal.bind(this)}>取消</Button>,
		            <Button key="submit" type="primary" size="large" onClick={this.saveData.bind(this)}>
		              确定
		            </Button>,
		          ]}>
				<Form ref="importOut">
					<Row gutter={40}>
						<Col span={12} className='uploadFile'>
							 <Upload {...props} accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
					          <Button className='f-bg-blue f-white'>
					             上传
					          </Button>
					        </Upload>
								
						</Col>
						<Col span={12}>
							<a className='f-btn' target='_blank' href={hostname()+'/api/marketing/downloadPotentialStudentTemplate?x-auth-token='+store.get('sessionId')} style={{border: "1px solid #D9D9D9",color:'#666666'}}>
								下载导入模版
							</a>
						</Col>
					</Row>
					<Row style={{ marginTop:'50px' }}>
						<Col span={24}>
				        	<FormItem className='f-inline-block f-mb5' style={{ margin:'0 50px 0 0' }} label='来源方式' wrapperCol={{ style: { display:'inline-block', verticalAlign:'middle' } }}>
        							{getFieldDecorator('recruitSource', {
        					            rules: [{
        					              required: true,
        					              message: '请选择来源方式'
        					            }],
        					          })(
        		                    	<Select
        		            			    className='f-vertical-middle'
        		            			    style={{ width: 200 }}
        		            			    placeholder="请选择"
        		            			    optionFilterProp="children"
        		            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        		            			  >
        		            			    <Option value="1">电话邀约</Option>
        		            			    <Option value="2">地推活动</Option>
        		            			    <Option value="3">网络数据</Option>
        		            			    <Option value="4">渠道推广</Option>
        		            			    <Option value="5">网络推广</Option>
        		            			    <Option value="6">主动上门</Option>
        		            			    <Option value="7">陌生来电</Option>
        		            			    <Option value="8">老带新</Option>
        		            			    <Option value="9">内部转化</Option>
        		            		  </Select>
        					          )}
        						</FormItem>
						</Col>
					</Row>
				</Form>
			</Modal>)
	}
}

const ExactSearchForm = Form.create()(ExactSearch);
const AdvancedSearchForm = Form.create()(AdvancedSearch);
const ImportOutCreate = Form.create()(ImportOut);