import React, { Component } from 'react';
import { Breadcrumb, Tabs, Row, Col, Button, Input, Select, Form, Table, DatePicker, message } from 'antd';
import { fpost } from '../common/io.js';
import { Loading, Pagination, undefinedToEmpty } from '../common/g.js';
import SelectSchool from '../common/selectSchool.js';
import SelectCourses from '../common/selectCourses.js';
import { default as SelectPerson } from '../common/selectPerson.js';

const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const FormItem = Form.Item;


export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			linkUpData: null,
			countLinkUp: {},
			total: 1,
			searchPara: {
				currentPage: 1, 
				pageSize: 10
			}
		}

	}
	getDataList(para) {
		fpost('/api/marketing/potentialStudentConnect/pagePotentialStudentConnect', para).then(res=>res.json()).then(res=>{
			if(res.success == true) {
				let arr = [];
				res.result.records.map((e, i)=>{
					e.key = i;
					arr.push(e);
				})
				this.setState({
					linkUpData: arr,
					total: res.result.total
				});
			}else {
				message.error(res.message);
			}
		});

		fpost('/api/marketing/potentialStudentConnect/countPotentialStudentConnect', para).then(res=>res.json()).then(res=>{
			if(res.success == true) {
				this.setState({
					countLinkUp: res.result
				})
			}else {
				message.error(res.message);
			}
		});
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount() {
		//获取数据列表
		this.getDataList();
	}
	render() {
		const columns = [
			{
			  title: '客户姓名',
			  dataIndex: 'potentialStudentName',
			  key: 'potentialStudentName',
			  className:'f-align-center',
			}, {
			  title: '头像',
			  key: 'headImg',
			  className:'f-align-center',
			  render: (text, record) => (
			  	record.imgUrl? <img src={record.imgUrl} width={50} height={50} className='f-round'/> : ''
			  )
			}, {
			  title: '性别',
			  dataIndex: 'genderStr',
			  key: 'genderStr',
			  className:'f-align-center',
			}, {
			  title: '年龄',
			  dataIndex: 'age',
			  key: 'age',
			  className:'f-align-center',
			}, {
			  title: '手机号码',
			  dataIndex: 'motherPhone',
			  key: 'motherPhone',
			  className:'f-align-center',
			}, {
			  title: '沟通时间',
			  dataIndex: 'connectTime',
			  key: 'connectTime',
			  className:'f-align-center',
			}, {
			  title: '意向课程(意向度)',
			  dataIndex: 'potentialConnectIntentionStr',
			  key: 'potentialConnectIntentionStr',
			  className:'f-align-center',
			  render: text => (
			  	<span dangerouslySetInnerHTML={{__html: text}}></span>
			  )
			}, {
			  title: '沟通方式',
			  dataIndex: 'connectWayStr',
			  key: 'connectWayStr',
			  className:'f-align-center',
			}, {
			  title: '承诺到访',
			  dataIndex: 'confirmStateStr',
			  key: 'confirmStateStr',
			  className:'f-align-center',
			}, {
			  title: '预约到访时间',
			  dataIndex: 'planedVisitTime',
			  key: 'planedVisitTime',
			  className:'f-align-center',
			}, {
			  title: '预约到访校区',
			  dataIndex: 'visitSchoolAreaName',
			  key: 'visitSchoolAreaName',
			  className:'f-align-center',
			}, {
			  title: '沟通内容',
			  dataIndex: 'connectMemo',
			  key: 'connectMemo',
			  className:'f-align-center',
			}];

		const { confirmNum, connectNum, visitNum, inviteNum } = this.state.countLinkUp;
		return (<div>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>市场运营</Breadcrumb.Item>
					    <Breadcrumb.Item>客户管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>沟通列表</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
						<Tabs defaultActiveKey="1" style={{textAlign:'center'}}>
							<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
					    		<ExactSearchForm searchPara={this.state.searchPara} setPara={this.setPara.bind(this)} updateDataList={this.getDataList.bind(this)}/>	
					    	</TabPane>
						    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
						    	<AdvancedSearchForm searchPara={this.state.searchPara} setPara={this.setPara.bind(this)} updateDataList={this.getDataList.bind(this)}/>	
						    </TabPane>
						 </Tabs>
					</div>
					<div className='f-bg-white f-radius1 f-box-shadow1 f-pd4 f-mt5'>
						<div className='f-pt4 f-pb4 f-flex'>
							<h3 className="f-title-blue f-inline-block">沟通列表</h3>
							<ul className='f-inline-block f-ml5'>
								<li className='f-inline-block' style={{ marginRight:'50px' }}>总沟通数：{ connectNum }</li>
								<li className='f-inline-block' style={{ marginRight:'50px' }}>邀约数：{ inviteNum }</li>
								<li className='f-inline-block' style={{ marginRight:'50px' }}>诺访数：{ confirmNum }</li>
								<li className='f-inline-block'>到访数：{ visitNum }</li>
							</ul>
						</div>
						{
							this.state.linkUpData != null?
							<Table columns={columns} dataSource={this.state.linkUpData} bordered className='f-align-center' pagination={ Pagination(this.state.total, this.state.searchPara, this.getDataList.bind(this)) } scroll={{ x: 1300 }}/>
							: <Loading />
						}
					</div>
				</div>)
	}
}

class ExactSearch extends Component{
	exactCondition() {
		let obj = this.props.form.getFieldsValue(), pageSize = this.props.searchPara.pageSize;
			obj.currentPage = 1;
			obj.pageSize = pageSize;

			obj = undefinedToEmpty(obj);

			this.props.updateDataList(obj);
			this.props.setPara(obj);
	}
	render() {
		const { getFieldDecorator } = this.props.form;
		return (<Form style={{ textAlign:'left' }}>
					<FormItem style={{ width:'25%', marginLeft:'30px' }} className='f-inline-block f-vertical-middle'>
						{getFieldDecorator('name', {
				            rules: [{
				              required: false,
				              message: '请输入客户姓名',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入客户姓名" />
				          )}
					</FormItem>
					<FormItem style={{ width:'25%', marginLeft:'30px' }} className='f-inline-block f-vertical-middle'>
						{getFieldDecorator('phone', {
				            rules: [{
				              pattern: /(^1[3|4|5|8][0-9]\d{4,8}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
				              message: '请输入正确的号码',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入手机号码" />
				          )}
					</FormItem>
					<FormItem className='f-inline-block f-ml3 f-vertical-middle'>
						<Button type="primary" icon="search" style={{height:'40px'}} onClick={ this.exactCondition.bind(this) }>搜索</Button>
					</FormItem>
				</Form>)
	}
}
class AdvancedSearch extends Component {
	constructor(props) {
		super(props);
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
			initValue: [],
			initSelect: true
		}
	}
	resetForm(){
		let dom = this.props.form, pageSize = this.props.searchPara.pageSize;
		dom.resetFields();
		this.props.updateDataList({currentPage: 1, pageSize});
		this.props.setPara({currentPage: 1, pageSize});
		this.setState({
			initSelect: false
		}, function(){
			this.setState({
				initSelect: true
			})
		})
	}
	_selectSchool(v) {
		this.props.form.setFieldsValue({
			visitSchoolAreaId: v
		});
	}
	getSelectPerson(v) {
		this.props.form.setFieldsValue({
			salesId: v
		});
	}
	disabledStartDate = (startValue) => {
	    const endValue = this.state.endValue;
	    if (!startValue || !endValue) {
	      return false;
	    }
	    return startValue.valueOf() > endValue.valueOf();
	  }

	  disabledEndDate = (endValue) => {
	    const startValue = this.state.startValue;
	    if (!endValue || !startValue) {
	      return false;
	    }
	    return endValue.valueOf() <= startValue.valueOf();
	  }

	  onChange = (field, value) => {
	    this.setState({
	      [field]: value,
	    });
	  }

	  onStartChange = (value) => {
	    this.onChange('startValue', value);
	  }

	  onEndChange = (value) => {
	    this.onChange('endValue', value);
	  }

	  handleStartOpenChange = (open) => {
	    if (!open) {
	      this.setState({ endOpen: true });
	    }
	  }

	  handleEndOpenChange = (open) => {
	    this.setState({ endOpen: open });
	  }
	  advancedCondition() {
	  	let obj = this.props.form.getFieldsValue(), pageSize = this.props.searchPara.pageSize;
	  		obj.startTime = obj.startTime? obj.startTime.format(dateFormat) : '';
	  		obj.endTime = obj.endTime? obj.endTime.format(dateFormat) : '';
	  		obj.connectWay = obj.connectWay? obj.connectWay : '';
	  		obj.intentionLevel = obj.intentionLevel? obj.intentionLevel : '';
	  		obj.currentPage = 1;
	  		obj.pageSize = pageSize;

	  		obj = undefinedToEmpty(obj);
		this.props.updateDataList(obj);
		this.props.setPara(obj);
	  }
	_selectCourses(v) {
		this.props.form.setFieldsValue({
			courseId: v
		});
	}
	render() {
		const { getFieldDecorator } = this.props.form;
		
		const { startValue, endValue, endOpen } = this.state;

		return (<Form style={{ textAlign:'left' }} className='f-pd4'>
					<Row>
						<Col span={8}>
							<Row>
								<Col span={5}>
									<span className='f-right f-custom-label'>选择时段</span>
								</Col>
								<Col span={19}>
									<Row>
										<Col span={11}>
											<FormItem>
												{
													getFieldDecorator('startTime', { 
														
													})(<DatePicker
												          disabledDate={this.disabledStartDate}
												          style={{width: '100%'}}
												          format="YYYY-MM-DD"
												          setFieldsValue={startValue}
												          placeholder="开始时间"
												          onChange={this.onStartChange}
												          onOpenChange={this.handleStartOpenChange}
												        />)
												}
											</FormItem>
										</Col>
										<Col className='f-align-center' span={2}>
											<span style={{lineHeight:'36px'}}>--</span>
										</Col>
										<Col span={11}>
											<FormItem>
												{
													getFieldDecorator('endTime', { 
														
													})(<DatePicker
												          disabledDate={this.disabledEndDate}
												          style={{width: '100%'}}
												          format="YYYY-MM-DD"
												          setFieldsValue={endValue}
												          placeholder="结束时间"
												          onChange={this.onEndChange}
												          open={endOpen}
												          onOpenChange={this.handleEndOpenChange}
												        />)
												}
											</FormItem>
										</Col>
									</Row>
								</Col>
							</Row>
						</Col>
						<Col span={8}>
							<FormItem style={{width: '100%'}} className='f-inline-block f-mb5' labelCol={{ span: 6 }} label='沟通方式' wrapperCol={{ span: 18, style: { display:'inline-block', verticalAlign:'middle' }}}>
								{getFieldDecorator('connectWay', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<Select
			            			    className='f-vertical-middle'
			            			    placeholder="请选择"
			            			    optionFilterProp="children"
			            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
			            			  >
			            			    <Option value="1">聊天工具</Option>
			            			    <Option value="2">电话</Option>
			            			    <Option value="3">面对面沟通</Option>
			            		  </Select>
						          )}
							</FormItem>
						</Col>
						<Col span={8}>
							{
								this.state.initSelect? <FormItem style={{width: '100%'}} labelCol={{span: 6}} className='f-inline-block f-mb5' label='校区' wrapperCol={{ span: 18, style: { display:'inline-block', verticalAlign:'middle' } }}>
								{getFieldDecorator('visitSchoolAreaId', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<SelectSchool 
			                    		width='100%'
										onSelect={ this._selectSchool.bind(this) } 
									/>
						          )}
							</FormItem> : ''
							}
						</Col>
					</Row>
					<Row>
						<Col span={8}>
							{
								this.state.initSelect? <FormItem className='f-inline-block f-mb5' labelCol={{span: 5}} style={{ width:'100%' }} label='市场顾问' wrapperCol={{ span: 19, style: { display:'inline-block', verticalAlign:'middle' } }}>
								{getFieldDecorator('salesId', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<SelectPerson
										onSelect={ this.getSelectPerson.bind(this) }
									/>
						          )}
							</FormItem> : ''
							}
						</Col>
						<Col span={8}>
							{
								this.state.initSelect? <FormItem className='f-inline-block f-mb5' style={{ width:'100%' }} label='意向课程' labelCol={{span: 6}} wrapperCol={{ span: 18, style: { display:'inline-block', verticalAlign:'middle' } }}>
								{getFieldDecorator('courseId', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<SelectCourses 
			                    		onSelect= { this._selectCourses.bind(this) } 
			                    	/>
						          )}
							</FormItem> : ''
							}
						</Col>
						<Col span={8}>
							<FormItem className='f-inline-block f-mb5' style={{ width: '100%' }} labelCol={{span: 6}} label='意向度' wrapperCol={{ span: 18, style: { display:'inline-block', verticalAlign:'middle' } }}>
								{getFieldDecorator('intentionLevel', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<Select
			            			    className='f-vertical-middle'
			            			    placeholder="请选择"
			            			    optionFilterProp="children"
			            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
			            			  >
			            			    <Option value="1">高</Option>
			            			    <Option value="2">中</Option>
			            			    <Option value="3">低</Option>
			            		  </Select>
						          )}
							</FormItem>
						</Col>
					</Row>
					<Row>
						<Col span={8}>
							<FormItem className='f-inline-block f-mb5' style={{ width:'100%' }} labelCol={{span: 5}} label='是否邀约' wrapperCol={{ span: 19, style: { display:'inline-block', verticalAlign:'middle' } }}>
								{getFieldDecorator('isInvite', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<Select
			            			    className='f-vertical-middle'
			            			    placeholder="请选择"
			            			    optionFilterProp="children"
			            			  >
			            			    <Option value="1">是</Option>
			            			    <Option value="0">否</Option>
			            		  </Select>
						          )}
							</FormItem>	
						</Col>
						<Col span={8} offset={1}>
							<FormItem className='f-inline-block'>
								<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.advancedCondition.bind(this) }>
			        				<i className='iconfont icon-hricon33 f-mr2'></i>
			        				搜索
			        			</Button>
			        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
			        				<i className='iconfont icon-reset f-mr2'></i>
			        				重置
			        			</Button>
							</FormItem>
						</Col>
					</Row>
				</Form>)
	}
}
const ExactSearchForm = Form.create()(ExactSearch);
const AdvancedSearchForm = Form.create()(AdvancedSearch);
