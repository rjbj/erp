import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Breadcrumb,
	Row,
	Col,
	Button,
	Form,
	Input,
	InputNumber,
	message
} from 'antd';
import '../main.less';
import $ from 'jquery'
//使用fetch请求接口数据
import {
	fpost,
	fpostArray
} from '../../common/io.js'; //同步、异步请求
import {
	getUrlParam
} from '../../common/g.js';
const statePage = getUrlParam('state') || ''; //state决定是新增记上课还是修改记上课 ，如果state为1的话表示新增记上课
const FormItem = Form.Item;
const u = require('../../common/io.js');
let ss = '';
//教务教学默认打开的首页
export class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			value: 1,
			studentList: [], //学员列表
			className: '', //班级名称
			classRoomName: '', //教室名称
			assistantName: '', ///助教名称
			teacherName: '', //教师名称
			consumePeriod: '', //消耗课时
			classStartDateTime: '', //上课时间,
			registrationNum: '', //总计人数
			assistantId: '',
			classId: '',
			classTimesDetailId: '',
			homework: '', //课后作业
			operatorId: '',
			iconLoading: false,
			remark: '',
			classContent: '',
			normalNum: '',
			editId: '',
		}
	}
	_save() {
		let self = this;
		const clazz = {
			assistantId: self.state.assistantId,
			classContent: self.props.form.getFieldValue('classContent'),
			classId: self.state.classId,
			classTimesDetailId: self.state.classTimesDetailId,
			consumePeriod: self.state.consumePeriod,
			homework: self.props.form.getFieldValue('homework'),
			operatorId: self.state.operatorId || '',
			registrationNum: self.state.registrationNum,
			schoolGroupId: self.state.schoolGroupId,
			studentConsumePeriod: self.state.studentConsumePeriod,
			teacherConsumePeriod: self.state.teacherConsumePeriod,
			teacherId: self.state.teacherId,
			remark: self.props.form.getFieldValue('remark'),
			studentList: []
		}
		for (let i = 0; i < $('.listData').length; i++) {
			let list = {};
			if (!statePage) {
				list.id = $('.listData').eq(i).attr('data-id');
			}
			list.active = $('.listData').eq(i).find('.active .ant-input-number-input').parents('div').attr('aria-valuenow');
			if (!list.active) {
				message.error('学员的活跃参与度不能为空哦');
				return false;
			}
			list.disciplineFocus = $('.listData').eq(i).find('.disciplineFocus .ant-input-number-input').parents('div').attr('aria-valuenow');
			if (!list.disciplineFocus) {
				message.error('学员的纪律专注度不能为空哦');
				return false;
			}
			list.insertClassPeriod = $('.listData').eq(i).attr('data-insertclassperiod') || '';
			list.originalClassAmount = $('.listData').eq(i).attr('data-originalclassamount') || '';
			list.receivableClassAmount = $('.listData').eq(i).attr('data-receivableclassamount') || '';
			list.studentId = $('.listData').eq(i).attr('data-studentid');
			list.totalClassPeriod = $('.listData').eq(i).attr('data-totalclassperiod') || '';
			for (var j = 0; j < $('.listData').eq(i).find('.RadioGroup input').length; j++) {
				if ($('.listData').eq(i).find('.RadioGroup input').eq(j).prop('checked')) {
					list.signUpStatus = $('.listData').eq(i).find('.RadioGroup input').eq(j).val();
				}
			}
			if (!list.signUpStatus) {
				message.error('学员状态必选');
				return false;
			}
			clazz.studentList.push(list);
		}
		if (statePage) {
			self._savePage(clazz); //新增记上课
		} else {
			clazz.id = self.state.editId;
			self.editSave(clazz); //修改记上课
		}
	}
	editSave(data) {
		fpostArray('/api/educational/class/signup/update', data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					message.success('修改成功');
					this.setState({
						iconLoading: true
					});
					setTimeout(() => {
						window.location.href = '/teach/recordAttend'
					}, 500);
				} else {
					message.error(res.message)
				}
			});

	}
	_savePage(data) {
		fpostArray('/api/educational/class/signup/add', data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					message.success('记上课成功');
					this.setState({
						iconLoading: true
					});
					setTimeout(() => {
						if (getUrlParam('linkId')) {
							window.location.href = 'teach/viewDetails?id=' + getUrlParam('linkId');
						} else {
							window.location.href = '/teach/recordAttend'
						}
					}, 500);
				} else {
					message.error(res.message)
				}
			});

	}
	componentDidMount() {
		if (statePage) {
			this._getDetail(); //新增获取详情
			$('.newname').html('新增上课记录');
		} else {
			this._editDetail(); //修改获取详情
		}
	}
	_event() {
		$('.RadioGroup input').off().on('change', function() {
			let html = $('.attendClass').html();
			if (html == '--') {
				html = 0;
			}
			let goClass = $(this).val();
			if (goClass != 1) {
				if (html != 0) {
					$('.attendClass').html(html - 1);
				}
			} else if (goClass == 1) {
				$('.attendClass').html(parseFloat(html) + parseFloat(1));
			}
		})
	}
	_editDetail() { //修改记上课初始化
		const self = this;
		$.ajax({
			type: 'get',
			cache: false,
			data: {
				classTimesDetailId: getUrlParam('id'),
			},
			url: u.hostname() + '/api/educational/class/signup/read',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/x-www-form-urlencoded',
				'x-auth-token': store.get('sessionId'), //登录功能放开后需要加上的
			},
			success: function(res) {
				if (res.success == true) {
					if (res.result) {
						const data = res.result;
						self.setState({
							studentList: data.studentList,
							className: data.className,
							classRoomName: data.classRoomName,
							assistantName: data.assistantName,
							teacherName: data.teacherName,
							consumePeriod: data.consumePeriod,
							classStartDateTime: data.classStartDateTime,
							registrationNum: data.registrationNum,
							assistantId: data.assistantId,
							classId: data.classId,
							classTimesDetailId: data.classTimesDetailId,
							operatorId: data.operatorId,
							schoolGroupId: data.schoolGroupId,
							studentConsumePeriod: data.studentConsumePeriod,
							teacherConsumePeriod: data.teacherConsumePeriod,
							teacherId: data.teacherId,
							homework: data.homework,
							classContent: data.classContent,
							remark: data.remark,
							normalNum: data.normalNum,
							editId: data.id
						})
					}
					self._event();
				} else {
					message.error(res.message)
				}

			}
		})
	}
	_getDetail() { //新增记上课初始化
		const self = this;
		fpost('/api/educational/class/signup/add/init', {
				classTimesDetailId: getUrlParam('id'),
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					const data = res.result;
					self.setState({
						studentList: data.studentList,
						className: data.className,
						classRoomName: data.classRoomName,
						assistantName: data.assistantName,
						teacherName: data.teacherName,
						consumePeriod: data.consumePeriod,
						classStartDateTime: data.classStartDateTime,
						registrationNum: data.registrationNum,
						assistantId: data.assistantId,
						classId: data.classId,
						classTimesDetailId: data.classTimesDetailId,
						operatorId: data.operatorId,
						registrationNum: data.registrationNum,
						schoolGroupId: data.schoolGroupId,
						studentConsumePeriod: data.studentConsumePeriod,
						teacherConsumePeriod: data.teacherConsumePeriod,
						teacherId: data.teacherId,
						normalNum: data.normalNum
					})
					$('.attendClass').html($('.listData').length)
					self._event();
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const radioStyle = {
			marginRight: '30px',
		};
		const {
			getFieldDecorator
		} = this.props.form;
		const {
			audioFileList,
			fileList,
			audioCoverFileList,
			teacherPicFileList
		} = this.state;
		const formItemLayout = {
			labelCol: {
				span: 2
			},
			wrapperCol: {
				span: 20
			}
		};
		const formItemLayout1 = {
			labelCol: {
				span: 14
			},
			wrapperCol: {
				span: 10
			}
		};
		return (
			<Form className='checkRecord'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>上课记录</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold newname">修改上课记录</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-mb5 f-pd5 f-pt3' style={{paddingBottom:'21px'}}>
					<div className='f-fz5 f-black f-mb5' style={{fontWeight:'bold'}}>
						{this.state.className}
					</div>
					<Row className='f-mb2'>
						<Col span={22} style={{borderBottom:'0.5px solid #dcdcdc',paddingBottom:'20px'}}>
							<span className='f-fz3 f-dark f-mr5'>上课时间</span>
							<span className='f-fz3 f-dark' style={{marginRight:'60px'}}>{this.state.classStartDateTime}</span>
							<span className='f-fz3 f-dark f-mr5'>消耗课时:</span>
							<span className='f-fz3 f-dark f-mr5'>{this.state.consumePeriod}</span>
						</Col>
					</Row>
					<Row>
						<div className='f-fz5 f-black f-mb5' style={{fontWeight:'bold',marginTop:'20px'}}>
							操作
						</div>
					</Row>
					{this.state.studentList?this.state.studentList.map((item, i) => {
						return(
							<div key={i} style={{width:'100%',marginBottom:'10px'}} className='f-flex listData f-align-inputNumber' 
								data-insertclassperiod={item.insertClassPeriod} data-originalclassamount={item.originalClassAmount}
								data-receivableclassamount={item.receivableClassAmount} data-studentid={item.studentId}
								data-totalclassperiod={item.totalClassPeriod} data-id={item.id}
								>
								<ul className='f-flex f-mr3' style={{width:'55%',height:'30px',lineHeight:'30px'}}>
									<Col span={6}>
										<li className='f-fz4 f-bold f-mr5 f-line1 f-black '>{item.studentName}</li>
									</Col>
									<li className='f-fz3 RadioGroup'>
									{item.signUpStatus==4?
										<div><input type='radio' name={i} className='f-mr2' value='4' defaultChecked={true} /><i className='f-fz3 f-dark' style={radioStyle}>补课</i></div>:
										<div><input type='radio' name={i} className='f-mr2' value='1' defaultChecked={item.signUpStatus==1?true:false} /><i className='f-fz3 f-dark' style={radioStyle}>上课</i>
							              <input type='radio' name={i} className='f-mr2' value='2' defaultChecked={item.signUpStatus==2?true:false}/><i className='f-fz3 f-dark' style={radioStyle}>请假</i>
							              <input type='radio' name={i} className='f-mr2' value='3' defaultChecked={item.signUpStatus==3?true:false}/><i className='f-fz3 f-dark' style={radioStyle}>旷课</i>
										</div>
									}
							              
									</li>
								</ul>
								<ul style={{width:'40%',height:'30px'}} className='f-flex'>
									<li className='f-fz3 disciplineFocus' style={{marginRight:'80px'}}>
										<FormItem {...formItemLayout1} label="纪律专注">
								            <InputNumber min={0} max={5} defaultValue={item.disciplineFocus?item.disciplineFocus:5}
								            		className='disciplineFocus'  precision={0}/>
								        </FormItem>
									</li>
									<li className='f-fz3 active'>
										<FormItem {...formItemLayout1} label="活跃参与">
								            <InputNumber min={0} max={5} defaultValue={item.active?item.active:5} precision={0}/>
								        </FormItem>
									</li>
								</ul>
							</div>
						)
					}):''}
					<div style={{width:'100%',marginTop:'37px'}} className='f-flex f-bold'>
						<ul className='f-flex f-dark' style={{width:'55%',height:'30px'}}>
							<li style={{marginRight:'40px'}}>
								<i className='f-fz3 f-mr1'>教师：</i>
								<i className='f-fz4'>{this.state.teacherName}</i>
							</li>
							<li>
								<i className='f-fz3 f-mr1'>助教：</i>
								<i className='f-fz4'>{this.state.assistantName}</i>
							</li>
						</ul>
						<ul className='f-flex f-dark' style={{width:'40%',height:'30px',display: 'flex',justifyContent: 'space-around'}}>
							<li style={{marginRight:'40px'}}>
								<i className='f-fz3 f-mr1'>教室：</i>
								<i className='f-fz4'>{this.state.classRoomName}</i>
							</li>
							<li>
								<i className='f-fz3 f-mr1'>上课人数：</i>
								<i className='f-fz4'><i className='attendClass'>{this.state.normalNum?this.state.normalNum:'--'}</i>人（总计{this.state.registrationNum}人）</i>
							</li>
						</ul>
					</div>
				</div>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-mb5 f-pd5 f-pt3'>
					<Row>
						<FormItem label="上课内容" {...formItemLayout}>
					          {getFieldDecorator('classContent', {
					            rules: [{  max:150, message: '最多可输入150字' }],
		                  		initialValue:this.state.classContent
					          })(
					          	<Input type="textarea" rows={6} placeholder='最多可输入150字'/>
					          )}
			        		</FormItem>
					</Row>
					<Row>
						<FormItem label="课后作业" {...formItemLayout}>
					          {getFieldDecorator('homework', {
					            rules: [{  max:250, message: '最多可输入250字' }],
		                  		initialValue:this.state.homework
					          })(
					          	<Input type="textarea" rows={6} placeholder='最多可输入250字'/>
					          )}
			        		</FormItem>
					</Row>
					<Row>
						<FormItem label="备注" {...formItemLayout}>
					          {getFieldDecorator('remark', {
					            rules: [{  max:90, message: '最多可输入90字' }],
		                  		initialValue:this.state.remark
					          })(
					          	<Input type="textarea" rows={3} placeholder='最多可输入90字'/>
					          )}
			        		</FormItem>
					</Row>
				</div>
				<Row>
				<Col span={22}>
				<div className='f-align-right'>
					<Button style={{height:'40px'}} className='f-mr5' onClick={()=>{window.history.go(-1)}}>
	        				<i className='iconfont icon-fanhui f-mr2'></i>
	        				返回
	        			</Button>
				    	<Button type="primary" style={{height:'40px'}} onClick={this._save.bind(this)} loading={this.state.iconLoading}><i className='iconfont icon-queding f-mr2 modalSure'></i>确定</Button>
	        			
        			</div>
        			</Col>
        			</Row>
			</Form>
		)
	}
}
const ModifyRecord = Form.create()(Main);
export {
	ModifyRecord
}