import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../../common/io.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">补课记录</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 2060}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  fixed: 'left',
  key: 'studentName',
  width: 180,
  render: (text, record, index) => {
    return (
      <div>
          {record.studentName}({record.studentNo})
        </div>
    )
  }
}, {
  title: '课程名称',
  width: 180,
  key: 'courseName',
  render: (text, record, index) => {
    return (
      <div title={record.courseName}>
          <i className='iconfont icon-shu f-mr2'></i>
          <i className='courseName f-line1'>{record.courseName}</i>
        </div>
    )
  }
}, {
  title: '当前课次',
  className: 'f-align-center',
  dataIndex: 'age',
  key: 'age',
  width: 150,
  render: (text, record) => (
    <span>{record.classTimes}</span>
  )
}, {
  title: '上课时间',
  className: 'f-align-center',
  dataIndex: 'classDate',
  key: 'classDate',
  width: 200,
}, {
  title: '班级',
  key: 'className',
  dataIndex: 'className',
  width: 150,
}, {
  title: '校区',
  key: 'schoolAreaName',
  dataIndex: 'schoolAreaName',
  width: 150,
}, {
  title: '教室',
  key: 'classRoomName',
  dataIndex: 'classRoomName',
  width: 150,
}, {
  title: '上课老师',
  width: 150,
  key: 'teacherName',
  dataIndex: 'teacherName'
}, {
  title: '助教',
  width: 150,
  key: 'assistantName',
  dataIndex: 'assistantName'
}, {
  title: '备注',
  width: 150,
  key: 'remark',
  render: (text, record, index) => {
    return (<div title={record.remark} className='f-line1' style={{width:'150px'}}>
                {record.remark}
            </div>);
  }
}, {
  title: '补课老师',
  key: 'makeUpTeacherName',
  dataIndex: 'makeUpTeacherName',
  width: 150,
}, {
  title: '补课时间',
  dataIndex: 'makeUpTime',
  key: 'makeUpTime',
  width: 150,
}, {
  title: '补课教室',
  key: 'makeUpRoomName',
  dataIndex: 'makeUpRoomName',
  width: 150,
}];