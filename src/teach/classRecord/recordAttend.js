import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Button,
	Select,
	Table,
	Tabs,
	Input,
	DatePicker,
	Row,
	Col,
	Form,
	Pagination,
	message
} from 'antd';
import '../main.less';
import $ from 'jquery';
import {
	Loading,
	getUnicodeParam
} from '../../common/g.js';
//使用fetch请求接口数据
import {
	fpost
} from '../../common/io.js'; //同步、异步请求
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const FormItem = Form.Item;
let classname = {};


//教务教学默认打开的首页
class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
			className: null, //班级名称
			courseName: null, //课程名称
			assistant: null, //助教
			pageSize: 10, //每页显示条数
			pageResult: null,
			currentPage: 1, //当前页码
			total: null, //总条数
			searchState: 0,
			course: [], //获取课程
			_getAssiant: [], //获取助教
			classroomList: [], //获取教室
			masterList: [], //班主任列表
			zhujiaoData: [], //助教列表
			name: getUnicodeParam('name')
		}
	}

	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm();
		});

	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			if (this.state.searchState == 0) {
				this.jqSearch();
			} else {
				this.advancedSearch()
			}
		})
	}
	componentDidMount() {
		this.jqSearch();
		this._getCourse(); //获取全部课程
		this._getClassroom(); //获取教室
		this._getMaster();
		this._getZhujiao();
	}
	_getMaster() { //获取班主任下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 4
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getZhujiao() { //获取助教下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 5
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						zhujiaoData: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	className(e) { //班级名称
		let v = e.target.value;
		this.setState({
			className: v
		});
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getCourse() { //获取课程
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getForm() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue.isSignUp = fieldsValue.isSignUp ? fieldsValue.isSignUp : '';
				fieldsValue.className = fieldsValue.className ? fieldsValue.className : '';
				fieldsValue.courseId = fieldsValue.courseId ? fieldsValue.courseId : '';
				fieldsValue.teacherId = fieldsValue.teacherId ? fieldsValue.teacherId : '';
				fieldsValue.roomId = fieldsValue.roomId ? fieldsValue.roomId : '';
				fieldsValue.assistantId = fieldsValue.assistantId ? fieldsValue.assistantId : '';
				fieldsValue['startDate'] = fieldsValue['startDate'] ? fieldsValue['startDate'].format('YYYY-MM-DD') : '';
				fieldsValue['endDate'] = fieldsValue['endDate'] ? fieldsValue['endDate'].format('YYYY-MM-DD') : "";
				fieldsValue['pageSize'] = this.state.pageSize;
				fieldsValue['currentPage'] = this.state.currentPage;
				this.getList(fieldsValue);
			}
		)
	}

	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			if (!this.state.searchState) {
				classname['className'] = $.trim($('.inputBG').val());
				classname.pageSize = this.state.pageSize;
				classname.currentPage = this.state.currentPage;
				this.getList(classname);
			} else {
				this._getForm(); //表单内容
			}
		});
	}
	jqSearch() { //精确搜索
		this.setState({
			currentPage: 1,
		}, function() {
			classname['className'] = $.trim($('.inputBG').val());
			classname.pageSize = this.state.pageSize;
			classname.currentPage = this.state.currentPage;
			this.getList(classname);
		});
	}
	advancedSearch() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm(); //表单内容
		});
	}
	callback(key) {
		if (key == 2) {
			this.setState({
				searchState: 1
			})
		} else {
			this.setState({
				searchState: 0
			})
		}
	}
	getList(fieldsValue) { //获取列表
		let self = this;
		fpost('/api/educational/class/times/detail/list', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const columns = [{
			title: '班级名称',
			key: '1',
			fixed: 'left',
			width: 180,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div title={record.className} style={{width:'180px'}} className='f-line1'>
			   			{record.className}
			   		</div>
				)
			}
		}, {
			title: '上课老师',
			dataIndex: 'teacherName',
			key: '2',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '助教',
			dataIndex: 'assistantName',
			key: '3',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '课程',
			key: '4',
			width: 180,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div title={record.courseName}>
			   			<i className='iconfont icon-shu f-mr2'></i>
			   			<i className='courseName f-line1'>{record.courseName}</i>
			   		</div>
				)
			}
		}, {
			title: '上课日期',
			dataIndex: 'displayStartDate',
			key: '5',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '上课时段',
			dataIndex: 'displayStartTime',
			key: '6',
			width: 180,
			className: 'f-align-center',
		}, {
			title: '校区',
			key: '7',
			width: 150,
			className: 'f-align-center',
			dataIndex: 'schoolAreaName'
		}, {
			title: '教室',
			key: '8',
			width: 150,
			className: 'f-align-center',
			dataIndex: 'classRoomName'
		}, {
			title: '状态',
			key: '9',
			width: 150,
			className: 'f-align-center',
			render: (text, record) => (
				<span>{record.isSignUp=='2'?<i style={{color: '#FF0000'}}>{record.displaySignUp}</i>:<i>{record.displaySignUp}</i>}</span>
			)
		}, {
			title: '操作',
			key: '10',
			width: 150,
			fixed: 'right',
			className: 'f-align-center',
			render: (text, record, index) => {
				return (<div>
		            {record.isSignUp=='1'?<i className='iconfont icon-bianji1 f-pointer f-mr5 f-fz5' onClick={()=>{window.location.href='/teach/modifyRecord?id='+record.id}}></i>
		            :<Button className='f-radius4 f-bg-green f-white f-viewdetail-check' onClick={()=>{window.location.href='/teach/modifyRecord?id='+record.id+'&state=1'}}><i className='iconfont icon-biji f-mr1 f-viewdetail-down f-fz5'></i>记上课</Button>
		            }
		            </div>);
			}
		}];
		return (
			<div className='classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>上课记录</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">记上课</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
					<Tabs defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false} onChange={this.callback.bind(this)}>
						<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
				    			<div style={{padding:'0 0 30px 26px'}}>
				    				<Input placeholder="请输入班级名称模糊查询" onChange={this.className.bind(this)} className='inputBG pageInpHeight f-radius1' defaultValue={this.state.name?this.state.name:''} style={{width:'352px',border:0}}/>
				    				<Button type="primary" icon="search" style={{height:'40px'}} onClick={this.jqSearch.bind(this)}>搜索</Button>
				    			</div>
				    		</TabPane>
					    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
					    		<Form style={{padding:'0 20px'}}>
					    		<Row gutter={40}>
					          <Col span={9} >
					          	 <Col span={6} className='f-fz4 f-black f-h4-lh4'>班级名称:&emsp;</Col>
					          	 <Col span={18} >
					          	 <FormItem>
					                  {getFieldDecorator('className', {
								         initialValue:this.state.name?this.state.name:''
					                  })(
					                  	<Input placeholder='请输入班级名称' className='inputBG pageInpHeight'/>
					                  )}
					                </FormItem>
							     </Col>
					          </Col>
					          <Col span={7} >
					          	 <Col span={9} className='f-fz4 f-black f-h4-lh4'>课程名称:&emsp;</Col>
					          	  <Col span={15} >
					          	  <FormItem {...formItemLayout}>
					                  {getFieldDecorator('courseId', {
					                  })(
					                    <Select showSearch allowClear placeholder="全部课程" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.course?
							                        	this.state.course.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
					                      </Select>
					                  )}
				                </FormItem>
						           </Col>
					          </Col>
					          <Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>上课老师:&emsp;</Col>
					              <Col span={15} >
					               <FormItem {...formItemLayout}>
					                  {getFieldDecorator('teacherId', {
					                  })(
					                    <Select
					                  	  showSearch
					                  	  allowClear
								          placeholder='请选择上课老师'
								          style={{ width: '100%' }}
								          optionFilterProp="children"
                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								        >
											{this.state.masterList?
							                        	this.state.masterList.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
								        </Select>
					                  )}
				                </FormItem>
						            </Col>
					          </Col>
					        </Row>
					        <Row className='' gutter={40}>
					        		<Col span={9}>
					        		<Col span={6} className='f-fz4 f-black f-h4-lh4'>上课日期:&emsp;</Col>
					        		<Col span={18} >
						    			 <Col span={11}>
						    			 <FormItem>
					                  {getFieldDecorator('startDate', {
					                  })(
						    			 	<DatePicker
								          disabledDate={this.disabledStartDate.bind(this)}
								          format="YYYY-MM-DD"
								          setFieldsValue={this.state.startValue}
								          placeholder="开始时间"
								          onChange={this.onStartChange.bind(this)}
								          onOpenChange={this.handleStartOpenChange.bind(this)}/>
						    			 	)}
			                			</FormItem>
						    			 </Col>
						    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
						    			  <Col span={11}>
						    			  <FormItem>
					                  {getFieldDecorator('endDate', {
					                  })(
								        <DatePicker
								          disabledDate={this.disabledEndDate.bind(this)}
								          format="YYYY-MM-DD"
								          setFieldsValue={this.state.endValue}
								          placeholder="结束时间"
								          onChange={this.onEndChange.bind(this)}
								          open={this.state.endOpen}
								          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								        )}
			                			</FormItem>
							         	</Col>
							        	</Col>
					        		</Col>
					        		<Col span={5} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>教室:&emsp;</Col>
					              <Col span={16} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('roomId', {
					                  })(
							            <Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.classroomList?
							                        	this.state.classroomList.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
					                      </Select>
							           )}
									</FormItem>
						            </Col>
					          	</Col>
					          	<Col span={5} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>助教:&emsp;</Col>
					              <Col span={15} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('assistantId', {
					                  })(
							            <Select
					                  	  showSearch
					                  	  allowClear
								          placeholder='请选择助教'
								          style={{ width: '100%' }}
								          optionFilterProp="children"
                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								        >
											{this.state.zhujiaoData?
							                        	this.state.zhujiaoData.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
								        </Select>
							            )}
									</FormItem>
						            </Col>
					          	</Col>
					          	<Col span={5} >
					             <Col span={10} className='f-fz4 f-black f-h4-lh4'>状态:&emsp;</Col>
					              <Col span={14} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('isSignUp', {
					                  })(
							            <Select placeholder='全部状态'>
							              <Option value="">全部状态</Option>
							              <Option value="0">未记上课</Option>
							              <Option value="1">记上课</Option>
							              <Option value="2">应记上课</Option>
							            </Select>
						            		)}
									</FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.advancedSearch.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					    		</Form>
					    </TabPane>
					 </Tabs>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">排课列表</h3>
					</div>
					<div>
						{this.state.pageResult?
							<div>
								<Table columns={columns} dataSource={this.state.pageResult} bordered scroll={{ x: 1600}} pagination={false}/>
									<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
										<div>
											<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
										</div>
									</div>
							</div>:<Loading/>
						}
					</div>
				</div>
			</div>
		)
	}
}

const RecordAttend = Form.create()(Main);
export {
	RecordAttend
}