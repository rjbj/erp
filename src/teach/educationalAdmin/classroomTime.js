import React, {
	Component
} from 'react';
import { Breadcrumb, Button, DatePicker, Form, Row, Col, Select, message, Pagination } from 'antd';
import '../main.less';
import {fpost } from '../../common/io.js'; //同步、异步请求
import {Loading } from '../../common/g.js';
const FormItem = Form.Item;
const Option = Select.Option;
export class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			batch: false, //是否批量
			teachersData: [],
			size: 'large',
			startValue: null,
			endValue: null,
			endOpen: false,
			current: 1,
			total: 50,
			teacher: null, //教室名称
			startTime: null, //开始时间
			endTime: null, //结束时间
			pageSize: 10, //每页显示条数
			pageResult: null,
			currentPage: 1,   //当前页码
			pageSize: 10,  //每页条数
			total: 40,   //总条数
			classroomList:[]
		}
	}

	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	onChange1 = (page) => {
		this.setState({
			current: page,
		});
	}
	_search() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this.getList();
		});
	}
	onShowSizeChange (current, pageSize) {
		this.setState({
			currentPage:current,
			pageSize:pageSize
		},function(){
			this.getList();
		})
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			this.getList();
		});
	}
	componentDidMount() {
		this.getList();
		this._getClassroom();
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	getList() { //获取列表
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['startDate']=fieldsValue['startDate']?fieldsValue['startDate'].format('YYYY-MM-DD HH:mm:ss'):'';
				fieldsValue['endDate']=fieldsValue['endDate']?fieldsValue['endDate'].format('YYYY-MM-DD HH:mm:ss'):'';
				fieldsValue['currentPage']=this.state.currentPage;
				fieldsValue['pageSize']=this.state.pageSize;
				fieldsValue['name']=fieldsValue['name']?fieldsValue['name']:'';
				this.getData(fieldsValue);
			}
		)
	}
	getData(fieldsValue){
		let self = this;
		fpost('/api/educational/period/classroom', fieldsValue)
		.then((res) => {
			return res.json();
		})
		.then((res) => {
			if(res.success == true) {
				if(res.result) {
					res.result.records.forEach((item, i) => {
						item.key = i;
					});
					self.setState({
						pageResult: res.result.records,
						total: parseInt(res.result.total)
					})
				}
			} else {
				message.error(res.message)
			}
		});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 3
			},
			wrapperCol: {
				span: 20
			}
		};
		return(<div className='courseArranging classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>教务</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">教室时段</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<Row className='f-bg-white f-radius1 f-box-shadow1 Topselect' style={{padding:'30px 20px'}}>
					<Col span={2} className='f-black f-fz4 f-h4-lh4'>选择时间：</Col>
					<Col span={9}>
						<Col span={11}>
						<FormItem {...formItemLayout}>
					         {getFieldDecorator('startDate')(
								<DatePicker
						          disabledDate={this.disabledStartDate.bind(this)}
						          showTime
						          format="YYYY-MM-DD HH:mm:ss"
						          setFieldsValue={this.state.startValue}
						          placeholder="开始时间"
						          onChange={this.onStartChange.bind(this)}
						          onOpenChange={this.handleStartOpenChange.bind(this)} />
								)}
							</FormItem>
				    		</Col>
			    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
				    		<Col span={11}>
				    			<FormItem {...formItemLayout}>
					         {getFieldDecorator('endDate')(
						        <DatePicker
						          disabledDate={this.disabledEndDate.bind(this)}
						          showTime
						          format="YYYY-MM-DD HH:mm:ss"
						          setFieldsValue={this.state.endValue}
						          placeholder="结束时间"
						          onChange={this.onEndChange.bind(this)}
						          open={this.state.endOpen}
						          onOpenChange={this.handleEndOpenChange.bind(this)} />
						      )}
							</FormItem>
						</Col>
					</Col>
					<Col span={3} className='f-align-right f-black f-fz4 f-pl5 f-h4-lh4'>教室：</Col>
					<Col span={7}>
						<FormItem {...formItemLayout}>
		        	          {getFieldDecorator('name', {
			                  })(
			                  	 <Select
			                  	  showSearch
			                  	  allowClear
						          placeholder='请选择教室'
						          style={{ width: '100%' }}
						          optionFilterProp="children"
                					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
				                      {this.state.classroomList?
					                        	this.state.classroomList.map((item,i)=> {
					                        		return(<Option key={item.id} value={item.name}>{ item.name }</Option>)
					                        	})
					                        	:null
				                      }
			                      </Select>
			                  )}
						</FormItem>
					</Col>
					<Col span={3}>
						<Button type="primary" style={{width:'110px',height:'40px'}} className='f-ml5 f-radius1' onClick={this._search.bind(this)}>
		        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
		        			</Button>
					</Col>
				</Row>
				<div className='f-mt5 f-pb2 f-dark f-mb3 f-fz5'>教室列表</div>
				<ul style={{width:'100%'}}>
					{this.state.pageResult?this.state.pageResult.length>0?this.state.pageResult.map((item, i) => {
						return(
							<li key={i} style={{width:'20%'}} className='f-pr5 f-inline-block f-mb5'>
								<div className='f-radius1 f-box-shadow1 f-bg-white'>
									<div className='f-clear f-pd3 f-flex'>
										<i className='f-dark f-line1' title={item.name} style={{fontSize:'24px'}}>{item.name}</i>
									</div>
									<ul className='teacherCourList'>
										<li style={{height:'50px',color:' #2187FF',lineHeight:'50px'}} className='f-align-center f-fz3 f-pointer'
											onClick={()=>{window.location.href='/teach/classroomDetail?id='+item.id+'&schoolGroupId='+item.schoolGroupId+'&name='+item.name}}>
											{item.classCount}个班级
										</li>
										<li style={{height:'50px',color:' #2187FF',lineHeight:'50px'}} 
											className='f-align-center f-fz3 f-pointer' onClick={()=>{window.location.href='/teach/classRecord?id='+item.id}}>
											上课记录
										</li>
									</ul>
								</div>
							</li>		
						)
					}):<div className='f-align-center'>暂无数据......</div>
					:
					<Loading/>}
				</ul>
				<div className='f-align-right'>
					<Pagination current={this.state.currentPage} onChange={this.pageChange.bind(this)} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper total={this.state.total} />
				</div>
				
		</div>);
	}
}
const ClassroomTime = Form.create()(Main);
export {
	ClassroomTime
}