import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Input,
	Button,
	Col,
	Row,
	Breadcrumb,
	Form,
	message,
	Switch
} from 'antd';
import {
	fpost
} from '../../common/io.js';
const FormItem = Form.Item;

export default class Main extends Component {
	render() {
		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>系统设置</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">系统参数设置</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
				<MyForm />
			</div>

		</section>);
	}
}

/*搜索表单*/
class MainForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			code: store.get('userInfo') && store.get('userInfo').institution && store.get('userInfo').institution.code, //集团代码
			result: null
		}
	}
	render() {
		const {
			getFieldDecorator,
			setFieldsValue
		} = this.props.form;

		let {
			result,
			code
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (
			<Form onSubmit={ this._validator.bind(this) }>
				<Row gutter={ 40 }>
					<Col span={ 24 }>
						<h3 className="f-mb5">
							<span className="f-title-blue f-mt3">机构相关</span>
						</h3>
					</Col>
					<Col span={ 12 }>
						<FormItem
							label="集团代码"
							{ ...{...formItemLayout} }
						>
							{ code }
						</FormItem>
					</Col>
				</Row>

				<Row gutter={ 40 }>
					<Col span={ 12 }>
						<FormItem
							label="机构名称"
							{ ...{...formItemLayout} }
						>
							{
								getFieldDecorator('name', {
									rules: [{ 
										required: true, 
										message: '请输入机构名称' }],
								})(
									<Input />
								)
							}
						</FormItem>
					</Col>
					<Col span={ 12 }>
						<FormItem
							label="总部固话"
							{ ...{...formItemLayout} }
						>
							{
								getFieldDecorator('groupPhone', {
									rules: [{ 
										required: true, 
										message: '请输入总部固话' 
									},{
										pattern:/(^1[0-9]\d{4,9}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
										message:'电话号码格式有误'
									}],
								})(
									<Input />
								)
							}
						</FormItem>
					</Col>
				</Row>

				<Row gutter={ 40 }>
					<Col span={ 12 }>
						<FormItem
							label="负责人"
							{ ...{...formItemLayout} }
						>
							{
								getFieldDecorator('supervisorName', {
									rules: [{ 
										required: true, 
										message: '请输入负责人' 
									}],
								})(
									<Input />
								)
							}
						</FormItem>
					</Col>
					<Col span={ 12 }>
						<FormItem
							label="负责人电话"
							{ ...{...formItemLayout} }
						>
							{
								getFieldDecorator('supervisorPhone', {
									rules: [{ 
										required: true, 
										message: '请输入负责人电话' 
									},{
										pattern: /^1[0-9]\d{4,9}$/,
										message:'电话号码格式有误'
									}],
								})(
									<Input />
								)
							}
						</FormItem>
					</Col>
					<Col span={ 24 }>
						<h3 className="f-mb5">
							<span className="f-title-blue f-mt3">物品相关</span>
						</h3>
					</Col>
					<Col span={ 12 }>
						<FormItem
							label="物品改价"
							{ ...{...formItemLayout} }
						>
							{
								getFieldDecorator('modifyMaterialPrice', {
									rules: [],
									valuePropName: 'checked'
								})(
									<Switch 
										size="large" 
										checkedChildren="允许" 
										unCheckedChildren="不允许" 
									/>
								)
							}
						</FormItem>
					</Col>
				</Row>
	        	<br />
		        <FormItem
		        	labelCol={{ span: 2 }}
		        	wrapperCol={{ span: 3, offset: 2 }}
		        >
					<Button 
						type="primary"
						htmlType="submit"
						size="large" 
						icon="save" 
					>保存</Button>
		        </FormItem>

		        {
					result ?
						<span className="f-hide">
							{
								getFieldDecorator('id')
								(
									<Input />
								)
							}
						</span>
					: null
				}

	      	</Form>
		);
	}
	componentDidMount() {
		this._getData();
	}
	_getData() {
		fpost('/api/system/schoolarea/findSchoolGroup')
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					result
				});
				this.props.form.setFieldsValue(result);
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_validator(e) {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this._save(values);
			}
		});
	}
	_save(values) {
		fpost('/api/system/schoolarea/updateSchoolGroup', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false
				});
				message.success(res.message || '保存成功');
			})
			.catch((err) => {
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);