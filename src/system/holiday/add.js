import React, {
	Component
} from 'react';
import {
	Input,
	Form,
	Modal,
	message
} from 'antd';
import {
	fpostArray
} from '../../common/io.js';
import RangeDate from '../../common/rangeDate.js';

const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false,
			rangeTime: null
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onOk,
			onCancel,
			defaultData
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		let {
			rangeTime,
			saveing
		} = this.state;
		let modalTitle = defaultData ? '编辑' : '新增';
		return (<Modal title={`${modalTitle}节假日`}
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
			>
				<Form>
					<FormItem
						label="节假日名称"
						{...formItemLayout}
					>
						{
							getFieldDecorator('name', {
								rules: [{ 
									required: true, 
									message: '请输入节假日名称' 
								}],
							})(
								<Input placeholder="请输入节假日名称" />
							)
						}
					</FormItem>

					<FormItem
						label="选择时间段"
						{...formItemLayout}
					>
						{
							getFieldDecorator('rangeTime',{
								rules: [{ 
									required: true, 
									message: '请选择时间段' 
								}],
								initialValue:''
							})(
								<RangeDate 
									onSelect={
										(d)=> {
											this.props.form.setFieldsValue({
												rangeTime:d,
												startTime:d.startString,
												endTime:d.endString,
											});
										}
									}
									initialValueData={ rangeTime }
								/>
							)
						}
					</FormItem>
					<span className="f-hide">
				        {
							getFieldDecorator('startTime',{
								initialValue:''
							})(
								<Input />
							)
						}
						{
							getFieldDecorator('endTime',{
								initialValue:''
							})(
								<Input />
							)
						}
					</span>

					{
						defaultData ?
							<span className="f-hide">
								{
									getFieldDecorator('id')
									(
										<Input />
									)
								}
							</span>
						: null
					}

		      	</Form>
		</Modal>);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		if (defaultData) { //如果是编辑，设置默认值
			let {
				startTime,
				endTime,
			} = defaultData;

			this.setState({
				rangeTime: {
					startTime,
					endTime
				}
			});

			this.props.form.setFieldsValue(defaultData);
			this.props.form.setFieldsValue({
				rangeTime: 'edit'
			});
		}

	}
	_ok(e) {
		e.preventDefault();

		let {
			result
		} = this.props;

		console.log('result:', result);

		this.props.form.validateFields((err, values) => {
			if (!err) {
				let {
					name,
					startTime,
					endTime,
					id
				} = values;

				if (id) {
					result = result.filter((d) => {
						return d.id != id;
					});
				}

				let param = [{
					'name': name,
					'startTime': startTime,
					'endTime': endTime,
					id
				}, ...result]
				console.log('param:', param);
				this._save(param);
			}
		});
	}
	_save(param) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpostArray('/api/system/holiday/saveHolidays', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk();
			})
			.catch((err) => {
				console.log(err);
				this.setState({
					saveing: false
				});
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;