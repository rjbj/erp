import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Popconfirm
} from 'antd';
import {
  numberFormate
} from '../../common/g.js';

export default class Main extends Component {
  render() {
    let {
      onEdit,
      onDelete,
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    const columns = [{
      title: '名称',
      width: '200px',
      dataIndex: 'name',
    }, {
      title: '时间段',
      width: '200px',
      className: 'f-align-center',
      dataIndex: 'modifyTime',
      render: (value, row, index) => {
        return (<div>
          { row.startTime }
          <p>-</p>
          { row.endTime }
        </div>);
      }
    }, {
      title: '优惠金额',
      width: '100px',
      dataIndex: 'amount',
      render: (value, row, index) => {
        return (<div>
          ¥{ numberFormate(value) }
        </div>);
      }
    }, {
      title: '适用课程',
      dataIndex: 'applicableCourses',
      render: (value, row, index) => {
        return (<div>
          { row.applicableCourses }
        </div>);
      }
    }, {
      title: '优惠类型',
      width: '100px',
      dataIndex: 'type',
      render: (value, row, index) => {
        return (<div>
          { row.preferentialType }
        </div>);
      }
    }, {
      title: '状态',
      width: '100px',
      className: 'f-align-center',
      dataIndex: 'isDisable',
      render: (value, row, index) => {
        let {
          isDisable
        } = row;
        // isDisable = !parseInt(isDisable);
        return (<div>
          {
            isDisable == '0' ? "启用" : "禁用" 
          }
          {/*<Switch 
            defaultChecked={isDisable} 
            checkedChildren="启用"
            unCheckedChildren="禁用" 
          />*/}
        </div>);
      }
    }, {
      title: '操作',
      dataIndex: 'des',
      width: '100px',
      className: "f-align-center",
      render: (value, row, index) => {
        return (<div>
          <Popconfirm 
            title="确定要删除吗?" 
            onConfirm={
              ()=> onDelete(row)
            }
            okText="确定" 
            cancelText="取消"
          >
            <Icon 
              className="f-mr3 f-pointer" 
              type="delete" 
            />
          </Popconfirm>
          <Icon 
            className="f-pointer" 
            type="edit" 
            onClick={ ()=> onEdit(row) }
          />
        </div>);
      }
    }];

    return (
      <Table
          columns={ columns }
          dataSource={ data }
          pagination={ pagination }
          loading={loading}
          onChange={onChange}
          bordered
      />
    );
  }
}