import React, {
	Component
} from 'react';
import {
	Input,
	Form,
	Select,
	Radio,
	Switch,
	Modal,
	message
} from 'antd';
import {
	fpost
} from '../../common/io.js';
import SelectCourses from '../../common/selectCourses.js';
import {
	discountType
} from '../../common/staticData.js';
import RangeDate from '../../common/rangeDate.js';
import {
	default as NumberInput
} from '../../common/numberInput.js';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false,
			isCommon: '1',
			courseIds: [],
			rangeTime: null,
			amount: {}
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onOk,
			onCancel,
			defaultData
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		let {
			isCommon,
			courseIds,
			rangeTime,
			saveing,
			amount
		} = this.state;
		let modalTitle = defaultData ? '编辑' : '新增';
		return (<Modal title={`${modalTitle}优惠`}
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
			>
				<Form>
					<FormItem
						label="名称"
						{...formItemLayout}
					>
						{
							getFieldDecorator('name', {
								rules: [{ 
									required: true, 
									message: '请输入名称' 
								}],
							})(
								<Input placeholder="请输入名称" />
							)
						}
					</FormItem>

					<FormItem
						label="选择时间段"
						{...formItemLayout}
					>
						{
							getFieldDecorator('rangeTime',{
								rules: [{ 
									required: true, 
									message: '请选择时间段' 
								}],
								initialValue:''
							})(
								<RangeDate 
									onSelect={
										(d)=> {
											this.props.form.setFieldsValue({
												rangeTime:d,
												startTime:d.startString,
												endTime:d.endString,
											});
										}
									}
									initialValueData={ rangeTime }
									showTime={true}
								/>
							)
						}
					</FormItem>
					<span className="f-hide">
				        {
							getFieldDecorator('startTime',{
								initialValue:''
							})(
								<Input />
							)
						}
						{
							getFieldDecorator('endTime',{
								initialValue:''
							})(
								<Input />
							)
						}
					</span>

					<FormItem
						label="适用课程"
						{...formItemLayout}
					>
						{
							getFieldDecorator('isCommon', {
								rules: [{ 
									required: true, 
									message: '请选择适用课程' 
								}],
								initialValue:'1'
							})(
								<RadioGroup
									onChange= {
										(e)=> {
											let v =e.target.value;
											this.setState({
												isCommon:v
											});
										}
									}
								>
							        <Radio value='1'>全部课程</Radio>
							        <Radio value='0'>指定课程</Radio>
							    </RadioGroup>
							)
						}
					</FormItem>
					{
						isCommon == '0' ?
							<FormItem
								label="指定课程"
								{...formItemLayout}
							>
								{
									getFieldDecorator('courseIds', {
										rules: [{ 
											required: true, 
											message: '请选择课程' 
										}],
										initialValue:courseIds
									})(
										<SelectCourses 
											mode="multiple"
											onSelect= {
												(v)=> {
													this.props.form.setFieldsValue({
														courseIds:v
													});
												}
											}
											initialValueData={
												defaultData && defaultData.courseIdStr ?
												(defaultData.courseIdStr).split(',') : []
											}
										/>
									)
								}
							</FormItem>
						: null
					}

					<FormItem
						label="金额"
						{...formItemLayout}
					>
						{
							getFieldDecorator('amount', {
								rules: [{ 
									required: true, 
									message: '请输入金额' 
								}],
								initialValue:amount
							})(
								<NumberInput
									style={{
										width:'100%'
									}}
								/>
							)
						}
					</FormItem>

					<FormItem
						label="优惠类型"
						{...formItemLayout}
					>
						{
							getFieldDecorator('type', { 
								rules: [{  
									required: true,
									message: '请选择优惠类型',
								}],
							})
							(
								<Select
									size="large"
	            					style={{minWidth:'160px'}}
	            					placeholder="请选择优惠类型"
								>
									{
										discountType.map((d,i)=> {
											return(<Option 
													key={i} 
													value={d.value}
												>{d.label}</Option>);
										})
									}
								</Select>
							)
						}
					</FormItem>

					<FormItem
						label="启用"
						{...formItemLayout}
					>
						{
							getFieldDecorator('isDisable', {
								rules: [{
									required: true,
								}],
								valuePropName: 'checked',
								initialValue:true
							})(
								<Switch  
									checkedChildren="启用" 
									unCheckedChildren="禁用" 
								/>
							)
						}
					</FormItem>

					{
						defaultData ?
							<span className="f-hide">
								{
									getFieldDecorator('id')
									(
										<Input />
									)
								}
							</span>
						: null
					}

		      	</Form>
		</Modal>);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		if (defaultData) { //如果是编辑，设置默认值
			let {
				isDisable,
				isCommon,
				startTime,
				endTime,
				courseIdStr,
				amount
			} = defaultData;
			defaultData.isDisable = isDisable == '0' ? true : false;

			this.setState({
				isCommon: isCommon,
				courseIds: courseIdStr.split(','),
				amount: {
					number: amount
				},
				rangeTime: {
					startTime,
					endTime
				}
			});

			this.props.form.setFieldsValue(defaultData);
			this.props.form.setFieldsValue({
				rangeTime: 'edit',
				amount: {
					number: amount
				}
			});
		}

	}
	_ok(e) {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			let isDisable = values.isDisable == true ? '0' : '1';
			let amount = values.amount.number;
			values = {
				...values,
				isDisable,
				amount
			}

			if (!err) {
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpost('/api/system/preferential/savePreferential', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk();
			})
			.catch((err) => {
				console.log(err);
				this.setState({
					saveing: false
				});
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;