import React, {
	Component
} from 'react';
import {
	Input,
	Form,
	Modal,
	message
} from 'antd';
import {
	fpost
} from '../../common/io.js';
import SelectSchool from '../../common/selectSchool.js';

const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onCancel,
			defaultData
		} = this.props;

		let {
			saveing
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};
		let modalTitle = defaultData ? '编辑' : '新增';
		return (<Modal title={`${modalTitle}教室`}
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
			>
				<Form>
					<FormItem
						label="教室名称"
						{...formItemLayout}
					>
						{
							getFieldDecorator('name', {
								rules: [{ 
									required: true, 
									message: '请输入教室名称' 
								}],
							})(
								<Input placeholder="请输入教室名称" />
							)
						}
					</FormItem>

					<FormItem
						label="选择校区"
						{...formItemLayout}
					>
						{
							getFieldDecorator('schoolAreaId', { 
								rules: [{
									required: true, 
									message: '请选择校区',
								}],
								initialValue: ""
							})
							(
								<SelectSchool 
									onSelect={
										(v) => {
											this.props.form.setFieldsValue({
												schoolAreaId:v
											});
										}
									}
									width='100%' 
									url="/api/system/schoolarea/listSchoolArea"
									initialValueData={ 
										defaultData && defaultData['schoolAreaId']  ?
										defaultData['schoolAreaId'] : null
									}
								/>
							)
						}
					</FormItem>
					{
						defaultData ?
							<span className="f-hide">
								{
									getFieldDecorator('id')
									(
										<Input />
									)
								}
							</span>
						: null
					}
		      	</Form>
		</Modal>);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		console.log(defaultData)

		if (defaultData) { //如果是编辑，设置默认值
			let {
				id,
				name,
				schoolAreaId
			} = defaultData;

			this.props.form.setFieldsValue({
				id,
				name,
				schoolAreaId
			});
		}

	}
	_ok(e) {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpost('/api/system/schoolarea/saveClassRoom', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk();
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;