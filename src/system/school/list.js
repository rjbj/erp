import React, {
	Component
} from 'react';
import {
	Row,
	Col,
	Card,
	Button,
	Popconfirm
} from 'antd';

export default class Main extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		let {
			onEdit,
			onDelete,
			data
		} = this.props;

		return (<Row gutter={16}>
			{
				data.map((d,i)=> {
					return(<Col key={i} span={8} className="f-mb4">
						<Card title={d.name} className="f-box-shadow1">
							<Row className="f-mb3">
								<Col span="6" className="f-fz2 f-pale">类型</Col>
								<Col span="18">{d.schoolAreaTypeStr}</Col>
							</Row>
							<Row className="f-mb3">
								<Col span="6" className="f-fz2 f-pale">电话</Col>
								<Col span="18">{d.contractPhone}</Col>
							</Row>
							<Row className="f-mb3">
								<Col span="6" className="f-fz2 f-pale">地址</Col>
								<Col span="18" className="f-line1" title={d.areaAddress}>
									{d.areaAddress}
								</Col>
							</Row>
							<footer className="f-align-right">
								<Popconfirm 
									title="确定要删除吗?" 
									onConfirm={
										()=> onDelete(d)
									}
									okText="确定" 
									cancelText="取消"
								>
									<Button 
										className="f-radius3" 
										icon="delete"
									>删除</Button>
								</Popconfirm>
								<Button 
									type="primary" 
									className="f-radius3 f-ml2" 
									icon="edit"
									onClick={ ()=> onEdit(d) }
								>编辑
								</Button>
							</footer>
						</Card>
					</Col>);
				})
			}
		</Row>);
	}
}