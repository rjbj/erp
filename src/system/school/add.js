import React, {
	Component
} from 'react';
import {
	Input,
	Form,
	Select,
	Modal,
	message
} from 'antd';
import {
	fpost
} from '../../common/io.js';
import {
	schoolType
} from '../../common/staticData.js';
import SelectCity from '../../common/selectCity.js';

const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onCancel,
			defaultData
		} = this.props;

		let {
			saveing
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};
		let modalTitle = defaultData ? '编辑' : '新增';

		return (<Modal title={`${modalTitle}校区`}
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
			>
				<Form>
					<FormItem
						label="校区名称"
						{...formItemLayout}
					>
						{
							getFieldDecorator('name', {
								rules: [{ 
									required: true, 
									message: '请输入校区名称' 
								}],
							})(
								<Input placeholder="请输入校区名称" />
							)
						}
					</FormItem>

					<FormItem
						label="选择类型"
						{...formItemLayout}
					>
						{
							getFieldDecorator('type', { 
								rules: [{
									required: true, 
									message: '请选择类型',
								}]
							})
							(
								<Select
									size="large"
                					placeholder="请选择类型"
								>
									{
										schoolType.map((d,i)=> {
											return(<Option 
													key={i} 
													value={d.value}
												>{d.label}</Option>);
										})
									}
								</Select>
							)
						}
					</FormItem>

					<FormItem
						label="地区"
						{...formItemLayout}
					>
						{
							getFieldDecorator('areaCode', {
								rules: [{ 
									required: true, 
									message: '请输入地区' 
								}],
							})(
								<SelectCity 
									onSelect={
										(v) => {
											this.props.form.setFieldsValue({
												areaCode: v
											});
										}
									}
									initialValueData={ 
										defaultData && defaultData['provinceId']  ?
										[defaultData['provinceId'], defaultData['cityId'], defaultData['areaId']] : null
									}
								/>
							)
						}
					</FormItem>

					<FormItem
						label="详细地址"
						{...formItemLayout}
					>
						{
							getFieldDecorator('address', {
								rules: [{ 
									required: true, 
									message: '请输入详细地址' 
								}],
							})(
								<Input.TextArea placeholder="请输入详细地址" />
							)
						}
					</FormItem>

					<FormItem
						label="电话号码"
						{...formItemLayout}
					>
						{
							getFieldDecorator('contractPhone', {
								rules: [{ 
									required: true, 
									message: '请输入电话号码',
								},{
									pattern: /(^1[0-9]\d{4,9}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
									message:'电话号码格式有误'
								}],

							})(
								<Input placeholder="请输入电话号码" />
							)
						}
					</FormItem>
					{
						defaultData ?
							<span className="f-hide">
								{
									getFieldDecorator('id')
									(
										<Input />
									)
								}
							</span>
						: null
					}
		      	</Form>
		</Modal>);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		console.log(defaultData)

		if (defaultData) { //如果是编辑，设置默认值
			this.props.form.setFieldsValue({
				name: defaultData['name'],
				type: defaultData['type'],
				areaCode: [defaultData['provinceId'], defaultData['cityId'], defaultData['areaId']],
				address: defaultData['address'],
				contractPhone: defaultData['contractPhone'],
				id: defaultData['id'],
			});
		}

	}

	_ok(e) {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpost('/api/system/schoolarea/saveSchoolArea', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk();
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;