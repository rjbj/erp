import React, {
	Component
} from 'react';
import {
	Modal,
	Input,
	message,
	Form
} from 'antd';

import {
	fpost
} from '../../common/io.js';

const FormItem = Form.Item;
const {
	TextArea
} = Input;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			/*大类相关*/
			saveing: false, //是否正在保存大类名称
		}
	}
	render() {
		let {
			visibleType,
			onCancel,
			defaultData
		} = this.props;

		let {
			saveing
		} = this.state;
		let modalTitle = defaultData ? '编辑' : '新增';
		return (<Modal title={`${modalTitle}大类`}
				visible={ visibleType }
				onOk={ this._validator.bind(this) }
				confirmLoading={ saveing }
				onCancel={ onCancel }
			>
				<MyForm 
					ref={ form=> this.form=form }
					defaultData={ defaultData }
				/>
			</Modal>);
	}
	_validator(e) {
		e.preventDefault();
		this.form.validateFields((err, values) => {
			if (!err) {
				this._save(values);
			}
		});
	}
	_save(values) {
		let {
			onOk
		} = this.props;

		this.setState({
			saveing: true
		});

		fpost('/api/system/course/saveSubject', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onOk();
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

class MainForm extends React.Component {
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			defaultData
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (
			<Form>
				<FormItem
					label="大类名称"
					{...formItemLayout}
				>
					{
						getFieldDecorator('name', { 
							rules: [{
								required: true, 
								message: '请输入课程大类名称' 
							}] 
						})
						(
							<TextArea placeholder="请输入课程大类名称" />
						)
					}
				</FormItem>
				{
					defaultData ?
						<span className="f-hide">
							{
								getFieldDecorator('id')
								(
									<Input />
								)
							}
						</span>
					: null
				}
	      	</Form>
		);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		if (defaultData) { //如果是编辑，设置默认值
			let {
				name,
				id
			} = this.props.defaultData;

			this.props.form.setFieldsValue({
				name,
				id
			});
		}

	}
}

const MyForm = Form.create()(MainForm);