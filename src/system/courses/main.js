import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Row,
	Col,
	Icon,
	message
} from 'antd';
/*获取课程列表*/
import {
	default as CourseList
} from './courses';
/*新增课程大类*/
import {
	default as AddType
} from './addType';
/*新增课程*/
import {
	default as AddCourse
} from './addCourse';
import {
	fpost
} from '../../common/io.js';
import {
	Loading
} from '../../common/g.js';
import './main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleType: false, //是否显示添加大类弹窗
			typeDefaultValue: null, //编辑大类的默认值
			visibleAddCourseModal: false, //是否显示新增课程弹窗
			courseDefaultValue: null, //编辑课程的默认值
			loading: true, //是否正在获取课程列表
			result: null,
		}
	}
	render() {
		let {
			result,
			visibleType,
			typeDefaultValue,
			visibleAddCourseModal,
			courseDefaultValue
		} = this.state;

		return (<div>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>系统设置</Breadcrumb.Item>
			    <Breadcrumb.Item>基础设置</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">课程设置</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-bg-white f-box-shadow1 f-radius2 f-pd4">
				<Row className="f-mb3">
				    <Col span={12} className="f-pt2">
				    	<h3 className="f-title-blue">课程列表</h3>
				    </Col>
				    <Col span={12} className="f-align-right">
				    	<a  
				    		onClick={ this._visibleAddType.bind(this) }
				    		className="f-btn-green f-mr3"
			    		>
				    		<Icon 
				    			className="f-fz7 f-vertical-middle f-bold" 
				    			type="plus" 
			    			/>
			    			添加大类
				    	</a>
				    	<a 
				    		className="f-btn-blue"
				    		onClick={ this._visibleAddCourse.bind(this) }
			    		>
				    		<Icon 
				    			className="f-fz7 f-vertical-middle f-bold" 
				    			type="plus" 
			    			/>
				    			添加课程
				    	</a>
				    	{
				    		visibleType ?
						    	<AddType 
						    		visibleType={ visibleType }
						    		onCancel={ this._addTypeCancel.bind(this) }
						    		onOk= { this._addTypeOk.bind(this) }
						    		defaultData={ typeDefaultValue }
						    	/>
						    : null
				    	}
				    	{
				    		visibleAddCourseModal ?
						    	<AddCourse
						    		visibleType={ visibleAddCourseModal }
						    		onCancel={ this._addCourseCancel.bind(this) }
						    		onOk= { this._addCourseOk.bind(this) }
						    		defaultData= { courseDefaultValue } 
						    	/>
						    : null
				    	}
				    </Col>
			    </Row>
			    {
			    	result ?
					    <CourseList 
					    	data={ result } 
					    	editType= { this._editType.bind(this) } 
					    	editCourse= { this._editCourse.bind(this) } 
					    	removeCourse= { this._removeCourse.bind(this) }
					    	removeType= { this._removeType.bind(this) }
				    	/>
				    : <Loading />
			    }
			</div>

		</div>);
	}
	componentDidMount() {
		/*获取课程列表*/
		this._getList();
	}
	_getList() {
		fpost('/api/system/course/listSubjectCourse')
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: true
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				result.forEach((d) => {
					d.key = d.id
				});

				this.setState({
					result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_visibleAddType() { //新增大类
		this.setState({
			visibleType: true
		});
	}
	_addTypeCancel() { //取消新增大类
		this.setState({
			visibleType: false,
			typeDefaultValue: null
		});
	}
	_addTypeOk() { //大类新增成功
		this._addTypeCancel();
		this._getList();
	}
	_editType(typeDefaultValue) { //编辑课程大类
		console.log(typeDefaultValue);
		this.setState({
			visibleType: true,
			typeDefaultValue
		});
	}
	_visibleAddCourse() { //显示新增课程弹窗
		this.setState({
			visibleAddCourseModal: true
		});
	}
	_addCourseCancel() { //取消新增课程
		this.setState({
			visibleAddCourseModal: false,
			courseDefaultValue:null
		});
	}
	_addCourseOk() {
		this._addCourseCancel();
		this._getList();
	}
	_editCourse(courseDefaultValue) { //编辑课程
		console.log(courseDefaultValue);
		this.setState({
			visibleAddCourseModal: true,
			courseDefaultValue
		});
	}
	_removeCourse(param) {
		fpost('/api/system/course/deleteCourse', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this._getList();
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_removeType(param) {
		fpost('/api/system/course/deleteSubject', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this._getList();
			})
			.catch((err) => {
				console.log(err);
			});
	}
}