import React, {
	Component
} from 'react';
import {
	Input,
	Form,
	Modal,
	Switch,
	message
} from 'antd';

import {
	fpost
} from '../../common/io.js';
import TreeSelectComponent from '../../common/treeSelect.js';

const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false,
			menuIds: [],
			treeEdited: false, //看看树形数据有没有操作过，如果没有则传allMenuIds
			allMenuIds: '',
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onCancel,
			defaultData
		} = this.props;

		let {
			saveing,
			menuIds
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};
		let modalTitle = defaultData ? '编辑' : '新增';
		return (<Modal title={`${modalTitle}权限组`}
				visible={ visible }
				onOk={ this._ok.bind(this) }
				confirmLoading={ saveing }
				onCancel={ onCancel }
			>
				<Form>
					<FormItem
						label="权限组名称"
						{...formItemLayout}
					>
						{
							getFieldDecorator('name', {
								rules: [{ 
									required: true, 
									message: '请请输入权限组名称选择' 
								}]
							})(
								<Input placeholder="请输入权限组名称" />
							)
						}
					</FormItem>

					<FormItem
						label="说明"
						{...formItemLayout}
					>
						{
							getFieldDecorator('desc', {
								rules: [{ 
									required: true, 
									message: '请输入说明' 
								}]
							})(
								<Input placeholder="请输入说明" />
							)
						}
					</FormItem>
					<FormItem
						label="包含权限"
						{...formItemLayout}
					>
						{
							getFieldDecorator('authIds', {
								rules: [{ 
									required: true, 
									message: '请选择包含权限' 
								}],
								initialValue:menuIds
							})(
								<TreeSelectComponent 
									onSelect={
										(d)=> {
											this.props.form.setFieldsValue({
												authIds:d
											});
											this.setState({
												treeEdited:true
											});
										}
									}
									initialValueData={ menuIds }
								/>
							)
						}
					</FormItem>
					<FormItem
						label="启用"
						{...formItemLayout}
					>
						{
							getFieldDecorator('isDisable', {
								rules: [{
									required: true,
								}],
								valuePropName: 'checked',
								initialValue:true
							})(
								<Switch  
									checkedChildren="启用" 
									unCheckedChildren="禁用" 
								/>
							)
						}
					</FormItem>
					{
						defaultData && defaultData.id ?
							<span className="f-hide">
								{
									getFieldDecorator('id')
									(
										<Input />
									)
								}
							</span>
						: null
					}
		      	</Form>
		</Modal>);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		console.log(defaultData)

		if (defaultData) { //如果是编辑，设置默认值
			let {
				isDisable,
				name,
				desc,
				id,
				menuIds,
				allMenuIds,
			} = defaultData;
			defaultData.isDisable = isDisable == '0' ? true : false;

			this.props.form.setFieldsValue({
				name,
				desc,
				id,
				isDisable: defaultData.isDisable
			});

			this.setState({
				menuIds: menuIds.split(','),
				allMenuIds,
			})
		}

	}
	_ok(e) {
		let {
			treeEdited,
			allMenuIds
		} = this.state;
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				let isDisable = values.isDisable == true ? '0' : '1';
				values = {
					...values,
					isDisable: isDisable
				}
				if (!treeEdited) { //如果没有编辑过就传后台默认给的带有父id的数据字段，不然后台保存有误
					values.authMenuStr = allMenuIds;
				}
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpost('/api/system/auth/saveRole', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false
				}, () => {
					onSaveOk();
				});
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;