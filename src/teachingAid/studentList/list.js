import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">学员列表</span>
          <a 
            className="f-btn-green f-right"
            target="_blank"
            onClick={exportFile}
          >
            <Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />导出学员名单
          </a>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 996}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  dataIndex: 'name',
  width: '160px',
  className: 'f-align-center',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      name,
      stuNo
    } = row;
    return <div 
      className="f-line1 f-over-hide"
      style={{
        width:'160px'
      }}
      title={`${name}(${stuNo})`}
    >
      { name }({stuNo})
    </div>
  }
}, {
  title: '头像',
  dataIndex: 'imageUrl',
  className: 'f-align-center',
  width: '76px',
  render: (value, row, index) => {
    let {
      headImgUrl
    } = row;
    return (<div className="f-list-avatar">
        <img src= {value}/> 
      </div>);
  }
}, {
  title: '性别',
  dataIndex: 'genderStr',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '生日',
  dataIndex: 'birthday',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '绑定手机号码',
  dataIndex: 'otherPhone',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '母亲号码',
  dataIndex: 'motherMobile',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '父亲号码',
  dataIndex: 'fatherMobile',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '100px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      studentId,
      name,
      phone
    } = row;
    return (<div>
  		<Dropdown 
        overlay={
          <Menu>
            <Menu.Item key="1">
              <Link to={`/teachingAid/studentList/detail?studentId=${studentId}`}>学员信息</Link>
            </Menu.Item>
            {/*<Menu.Item key="2">
              转入e校通
            </Menu.Item>*/}
          </Menu>
        }>
        <Button type="primary" className="f-radius4">
          查看<Icon type="down" />
        </Button>
      </Dropdown>
  	</div>);
  }
}];