import React, {
	Component
} from 'react';
import {
	Button,
	Icon,
	Row,
	Col,
	Card,
	Popconfirm
} from 'antd';
export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		let {
			onEdit,
			onDelete,
			data
		} = this.props;

		return (<Row gutter={16}>
			<Col key="1" span={8} className="f-mb4">
				<Card title="name" className="f-box-shadow1">
					<Row className="f-mb3">
						<Col span="6" className="f-fz2 f-pale">系列</Col>
						<Col span="18">OW</Col>
					</Row>
					<footer className="f-align-right">
						<Popconfirm 
							title="确定要删除吗?" 
							onConfirm={
								()=> onDelete('d')
							}
							okText="确定" 
							cancelText="取消"
						>
							<Button 
								className="f-radius3" 
								icon="delete"
							>删除</Button>
						</Popconfirm>
						<Button 
							type="primary" 
							className="f-radius3 f-ml2" 
							icon="edit"
							onClick={ ()=> onEdit('d') }
						>编辑
						</Button>
					</footer>
				</Card>
			</Col>
		</Row>);
	}
}