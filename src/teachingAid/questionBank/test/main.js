import React, {
	Component
} from 'react';
import {
	Form,
	Input,
	Icon,
	Button
} from 'antd';
const FormItem = Form.Item;

let uuid = 0;
class DynamicFieldSet extends React.Component {
	remove = (k) => {
		const {
			form
		} = this.props;
		// can use data-binding to get
		const keys = form.getFieldValue('keys');
		// We need at least one passenger
		if (keys.length === 1) {
			return;
		}

		// can use data-binding to set
		form.setFieldsValue({
			keys: keys.filter(key => key !== k),
		});
	}

	add = () => {
		uuid++;
		const {
			form
		} = this.props;
		// can use data-binding to get
		const keys = form.getFieldValue('keys');
		const nextKeys = keys.concat(uuid);
		// can use data-binding to set
		// important! notify form to detect changes
		form.setFieldsValue({
			keys: nextKeys,
		});
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	}

	render() {
		const {
			getFieldDecorator,
			getFieldValue
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				xs: {
					span: 24
				},
				sm: {
					span: 4
				},
			},
			wrapperCol: {
				xs: {
					span: 24
				},
				sm: {
					span: 20
				},
			},
		};
		const formItemLayoutWithOutLabel = {
			wrapperCol: {
				xs: {
					span: 24,
					offset: 0
				},
				sm: {
					span: 20,
					offset: 4
				},
			},
		};
		getFieldDecorator('keys', {
			initialValue: []
		});
		const keys = getFieldValue('keys');
		const formItems = keys.map((k, index) => {
			console.log(k);
			console.log(index);
			return (
				<FormItem
		          {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
		          label={index === 0 ? '请输入:' : ''}
		          required={false}
		          key={k}
		        >
		          {getFieldDecorator(`names-${k}`, {
		            validateTrigger: ['onChange', 'onBlur'],
		            rules: [{
		              required: true,
		              whitespace: true,
		              message: "请输入.",
		            }],
		          })(
		            <Input placeholder="请输入" style={{ width: '60%', marginRight: 8 }} />
		          )}
		          {keys.length > 1 ? (
		            <Icon
		              className="dynamic-delete-button"
		              type="minus-circle-o"
		              disabled={keys.length === 1}
		              onClick={() => this.remove(k)}
		            />
		          ) : null}
		        </FormItem>
			);
		});
		return (
			<Form onSubmit={this.handleSubmit}>
        		{formItems}
		        <FormItem {...formItemLayoutWithOutLabel}>
		          <Button type="dashed" onClick={this.add} style={{ width: '60%' }}>
		            <Icon type="plus" /> 新增
		          </Button>
		        </FormItem>
		        <FormItem {...formItemLayoutWithOutLabel}>
		          <Button type="primary" htmlType="submit">Submit</Button>
		        </FormItem>
	      </Form>
		);
	}
}

const WrappedDynamicFieldSet = Form.create()(DynamicFieldSet);
export default WrappedDynamicFieldSet;