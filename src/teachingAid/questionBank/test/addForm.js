import React, {
	Component
} from 'react';
import {
	Form,
	Input,
	Button,
	Modal
} from 'antd';

const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false
		}
	}
	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		let {
			visible,
			onCancel,
			defaultData
		} = this.props;

		let {
			saveing
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		let modalTitle = 0 ? '编辑' : '新增';

		return (<Modal title={`${modalTitle}校区`}
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
			>
			<Form>
				<FormItem
					label="单元名称"
					{...formItemLayout}
				>
					{
						getFieldDecorator('name', { 
							rules: [{
								required: true, 
								message: '请输入教材名称' 
							}] 
						})
						(
							<Input placeholder="请输入教材名称" />
						)
					}
				</FormItem>
				<FormItem
					label="选择系列"
					{...formItemLayout}
				>
					{
						getFieldDecorator('name', { 
							rules: [{
								required: true, 
								message: '请选择系列' 
							}] 
						})
						(
							<Input placeholder="请选择系列" />
						)
					}
				</FormItem>
				<FormItem
					label="选择教材"
					{...formItemLayout}
				>
					{
						getFieldDecorator('name', { 
							rules: [{
								required: true, 
								message: '请选择系列' 
							}] 
						})
						(
							<Input placeholder="请选择系列" />
						)
					}
				</FormItem>
	      	</Form>
		</Modal>);
	}
	_ok(e) {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this._save(values);
			}
		});
	}
	_save(values) {
		let {
			onSaveOk
		} = this.props;

		this.setState({
			saveing: true
		});

		console.log(values);

		onSaveOk();

		// fpost('/api/system/course/saveSubject', values)
		// 	.then(res => res.json())
		// 	.then((res) => {
		// 		if (!res.success) {
		// 			this.setState({
		// 				saveing: false
		// 			});
		// 			message.error(res.message || '系统错误');
		// 			throw new Error(res.message || '系统错误');
		// 		};
		// 		return (res);
		// 	})
		// 	.then((res) => {
		// 		onSaveOk();
		// 	})
		// 	.catch((err) => {
		// 		this.setState({
		// 			saveing: false
		// 		});
		// 		console.log(err);
		// 	});
	}
}

export default Form.create()(Main);