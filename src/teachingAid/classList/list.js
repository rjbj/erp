import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Popconfirm
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">班级列表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 1426}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '班级图标',
  dataIndex: 'imageUrl',
  className: 'f-align-center',
  width: '76px',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      imageUrl
    } = row;
    return (<div className="f-list-avatar">
        <img src= {value}/> 
      </div>);
  }
}, {
  title: '班级名称',
  dataIndex: 'className',
  width: '150px',
  className: 'f-align-center',
  render: (value, row, index) => {
    let {
      className,
    } = row;
    return <div 
      className="f-line1 f-over-hide"
      style={{
        width:'150px'
      }}
      title={`${className}`}
    >
      { className }
    </div>
  }
}, {
  title: '学生数',
  dataIndex: 'studentLimitNums',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '班主任',
  dataIndex: 'teacherName',
  className: 'f-align-center',
  width: '150px'
}, {
  title: '教材名称',
  dataIndex: 'lessonName',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '开始时间',
  dataIndex: 'classStartTime',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '结束时间',
  dataIndex: 'classEndTime',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '校区',
  dataIndex: 'schoolAreaName',
  className: 'f-align-center',
  width: '150px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '200px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      classId,
      name,
      phone
    } = row;
    return (<div>
      {/*<Popconfirm 
          title="确定要删除吗?" 
          onConfirm={
              ()=> {
                console.log('delete');
              }
          }
          okText="确定" 
          cancelText="取消"
      >
          <Icon 
              className="f-mr3 f-pointer" 
              type="delete" 
          />
      </Popconfirm>
      <Icon 
          className="f-pointer f-mr5" 
          type="edit" 
          onClick={ 
            ()=> {
              window.location.href="/teachingAid/editClass";
            }
          }
      />*/}
  		<Dropdown 
        overlay={
          <Menu>
            <Menu.Item key="1">
              <Link to={`/teachingAid/classList/members?classId=${classId}`}>查看班级成员</Link>
            </Menu.Item>
            {/*<Menu.Item key="2">
              转入e校通
            </Menu.Item>*/}
          </Menu>
        }>
        <Button type="primary" className="f-radius4">
          查看<Icon type="down" />
        </Button>
      </Dropdown>
  	</div>);
  }
}];