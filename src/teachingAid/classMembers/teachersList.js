import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Popconfirm
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';
import {
  getUrlParam
} from '../../common/g.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      deleteMember
    } = this.props;

    if (!data || !data.students) {
      return null
    }

    const columns = [{
      title: '姓名',
      dataIndex: 'name',
      render: (value, row, index) => {
        let {
          name,
          stuNo
        } = row;
        return <div 
          title={`${name}`}
        >
          { name }
        </div>
      }
    }, {
      title: '头像',
      dataIndex: 'imageUrl',
      className: 'f-align-center',
      width: '76px',
      render: (value, row, index) => {
        let {
          imageUrl
        } = row;
        return (<div className="f-list-avatar">
            <img src= {value}/> 
          </div>);
      }
    }, {
      title: '手机号',
      dataIndex: 'mobile',
      className: 'f-align-center',
    }, {
      title: '操作',
      dataIndex: 'des',
      width: '200px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          relationId
        } = row;
        return (<div>
          <Popconfirm 
              title="确定要移出班级吗?" 
              onConfirm={
                  ()=> {
                     deleteMember({
                        relationId,
                        type:1
                      })
                  }
              }
              okText="确定" 
              cancelText="取消"
          >
            <Button>移出班级</Button>
          </Popconfirm>
        </div>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">老师列表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data.teachers }
            pagination={ false }
            loading={loading}
            bordered
				  />
			</div>
    );
  }

}