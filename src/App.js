import React from 'react';
import {
	BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';
import {
	message
} from 'antd';
import store from 'store2';
import moment from 'moment';
import 'moment/locale/zh-cn';

import $ from 'jquery';
import {
	fpost
} from './common/io.js';
import {
	getUrlParam
} from './common/g.js';

import {
	default as Home
} from './home/main.js'; /*首页*/
import {
	default as FrontBusiness
} from './front/main.js'; /*前台业务*/
import {
	default as FinanceStatistical
} from './finance/main.js'; /*财务统计*/
import {
	default as PersonManage
} from './person/main.js'; /*人事管理*/
import {
	default as StorageManage
} from './storage/main.js'; /*仓库管理*/
import {
	default as SystemManage
} from './system/main.js'; /*系统设置*/
import {
	default as Teach
} from './teach/main.js'; /*教务教学*/
import {
	default as Market
} from './market/main.js'; /*市场运营*/
import {
	default as AfterSale
} from './after/main.js'; /*售后服务*/
import {
	default as TeachingAid
} from './teachingAid/main.js'; /*教辅平台*/
import {
	ForgotCode
} from './forgotCode/main.js'; /*忘记密码第一步*/
import {
	IdentityVerify
} from './forgotCode/verification.js'; /*忘记密码第二步*/
import {
	ResetPassComplete
} from './forgotCode/resetPassComplete.js'; /*忘记密码最后一步*/
import {
	SetPassword
} from './forgotCode/setPassword.js'; /*重置密码*/
import {
	Login
} from './login/main.js'; /*登录页面*/
import {
	Index
} from './index/main.js'; /*首页*/
import {
	default as PrintPage
} from './print/main.js'; /*首页*/

moment.locale('zh-cn');
/*storage持久化本地数据创建默认存储对象FANCY*/

window.store = store; // https://github.com/nbubna/store
let pathname = window.location.pathname;

console.log('store', store.getAll());
console.log('session', store.session());
// store(false);
// store.session(false);
/*每次加载清空头部菜单的选中项，重新设置*/
store.session.set('tMenu', '');

//const url=getUrlParam('target')||'/index';
const url = window.location.href.split(window.location.host)[1];
class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		let sessionId = store.get('sessionId');

		if (pathname == '/forgotCode') {
			return <ForgotCode />;
		} else if (pathname == '/IdentityVerify') {
			return <IdentityVerify />;
		} else if (pathname == '/resetPassComplete') {
			return <ResetPassComplete />;
		} else if (pathname == '/resetPassword') {
			return <SetPassword />;
		}

		if (!sessionId) {
			return <Login />;
		}
		return (<div>
	    	<Router>
				<Switch>
					<Route path="/" exact component={ Index } />
					<Route path="/front" component={ FrontBusiness } />
					<Route path="/finance" component={ FinanceStatistical } />
					<Route path="/person" component={ PersonManage } />
					<Route path="/storage" component={ StorageManage } />
					<Route path="/system" component={ SystemManage } />
					<Route path="/teach" component={ Teach } />
					<Route path="/market" component={ Market } />
					<Route path="/after" component={ AfterSale } />
					<Route path="/teachingAid" component={ TeachingAid } />
					<Route path="/forgotCode" component={ ForgotCode } />
					<Route path='/IdentityVerify' component={ IdentityVerify } />
					<Route path='/resetPassComplete' component={ ResetPassComplete } />
					<Route path='/resetPassword' component={ SetPassword } />
					<Route path='/login' component={ Login } />
					<Route path='/index' component={Index } />
					<Route path='/print' component={PrintPage } />
					<Route path="*" component={ NoMatch} />
				</Switch>
			</Router>
		</div>)
	}
	componentDidMount() {
		/*检测用户是否登陆*/
		this._getLoginInfo();
		/*获取全局配置信息*/
		this._getGlobalConfig();
	}
	_getLoginInfo() {
		let state = '';
		if (window.location.href.indexOf('/login') != -1 || window.location.href.indexOf('/forgotCode') != -1 || window.location.href.indexOf('/IdentityVerify') != -1 || window.location.href.indexOf('/resetPassComplete') != -1) {
			state = '1'
		} else {
			state = '';
		}
		if (!state) {
			fpost('/api/system/user/getLoginUserInfo')
				.then((res) => {
					return res.json();
				})
				.then((res) => {
					if (res.success == true) {
						//处于登陆状态
					} else if (res.result == 'NO_LOGIN') {
						message.error('请先登录', 2);
						setTimeout(
							window.location.href = '/login?target=' + encodeURIComponent(url), 2000
						);
					} else {
						message.error(res.message)
					}
				});
		}
	}
	_getGlobalConfig() {
		fpost('/api/global/config/getGlobalConfig')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				// console.log(res.result);
				store.session(res.result);
			});
	}

}

const NoMatch = () => (
	<div>
    没有找到您要的页面
  </div>
)

export default App