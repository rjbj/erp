import React, {
    Component
} from 'react';
import {
    Select
} from 'antd';
import {
    fpost
} from './io.js';
const Option = Select.Option;

export default class SelectRoleGroups extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fetching: false,
            result: []
        }
    }
    onSelect(value) {
        console.log('onSelect', value);
    }
    render() {
        const {
            fetching,
            result
        } = this.state;
        let {
            initialValueData
        } = this.props;


        let width = this.props.width ? this.props.width : '200px';
        let mode = this.props.mode || '';

        if (!result.length) {
            return null;
        }

        return (
            <Select
                size="large"
                allowClear
                mode={mode}
                placeholder="请选择权限组"
                style={{ minWidth: '60px',width:width }}
                onChange={ this._handleChange.bind(this) }
                defaultValue= { initialValueData }
            >
                <Option value=''>请选择</Option>
                {
                    result.map((d,i)=> {
                        return (<Option key={i} value={d.id}>{d.name}</Option>);
                    })
                }
            </Select>
        );
    }
    componentDidMount() {
        this._getData();
    }
    _handleChange(v) {
        this.props.onSelect(v);
    }
    _getData() {
        fpost('/api/system/auth/listRoles')
            .then((res) => res.json())
            .then((res) => {
                if (!res.success || !res.result || !res.result.length) {
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {
                // console.log(result);
                if (result) {
                    this.setState({
                        result
                    });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }
}