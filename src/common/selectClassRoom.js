import React, {
	Component
} from 'react';
import {
	Table,
	Button,
	Col,
	Row,
	Modal,
	Form,
	Select,
	Input,
	message,
	Tooltip
} from 'antd';
import SelectSchool from './selectSchool.js';
import {
	fpost
} from './io.js';
import {
	undefinedToEmpty
} from './g.js';

const FormItem = Form.Item;

const columns = [{
	title: '班级名称',
	dataIndex: 'name',
	render: (value, row, index) => {
		let {
			id
		} = row;
		return <div>
			<p>{value}</p>
			<a href={`/teach/viewDetails?id=${id}`} target="_blank">（查看排课信息）</a>
	  	</div>
	}
}, {
	title: '已上／已排课次',
	dataIndex: 'yipai',
	className: "f-align-center",
	render: (value, row, index) => {
		let {
			signUpNum,
			courseTimes
		} = row;
		return (<div>
	  		{signUpNum}/{courseTimes}
	  	</div>);
	}
}, {
	title: '报名人数／额定人数',
	dataIndex: 'type',
	className: "f-align-center",
	render: (value, row, index) => {
		let {
			registrationNum,
			rated
		} = row;
		return (<div>
	  		{registrationNum}/{rated}
	  	</div>);
	}
}, {
	title: '开班时间',
	dataIndex: 'classStartTime',
	render: (value, row, index) => {
		return (<div>
	  		{value}
	  	</div>);
	}
}, {
	title: '上课时间',
	dataIndex: 'classStartTimeList',
	render: (value, row, index) => {
		let data = value.map((d, i) => {
			return <p key={i}>{d}</p>
		})
		return (<Tooltip placement="topLeft" title={data}>
	        <ul>
				{
					value.map((d,i)=> {
						if(i>=3) { 
							return null 
						}
						return <li key={i}>{d}</li>
					})
				}
		  	</ul>
	    </Tooltip>);
	}
}, {
	title: '老师',
	dataIndex: 'teacherName'
}];

export default class SelectClassRoom extends Component {
	constructor(props) {
		super(props);
		let {
			schoolAreaId = '',
				filterRegistrationFull = '',
				courseId = ''
		} = this.props;

		this.state = {
			selectedRowKeys: [],
			loading: false,
			result: null,
			pagination: {
				size: "small",
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {
				filterRegistrationFull: filterRegistrationFull,
				schoolAreaId: schoolAreaId,
				courseId: courseId
			}
		}
	}
	render() {
		let {
			schoolAreaId,
			handleOk,
			handleCancel,
		} = this.props;

		const {
			selectedRowKeys,
			result,
			pagination,
			loading
		} = this.state;

		let selectData = null;
		if (selectedRowKeys.length) {
			selectData = result.filter((d) => {
				return d.key == selectedRowKeys.join(',')
			});
			selectData = selectData[0];
		}

		return (<Modal
			title="选择班级"
			width='80%'
			visible={ this.props.visible }
			onOk={ ()=> handleOk(selectData) }
			onCancel={ handleCancel }
			>
				<MyFormCreate 
					ref={ (form)=> this.form=form }
					onSearch= { this._getSearchParam.bind(this) }
					schoolAreaId={schoolAreaId}
				/>
				<div className="f-mb4" />
				<Table
				    columns={ columns }
				    dataSource={ result }
				    rowSelection = {{
				        selectedRowKeys,
				        type:'radio',
				        onChange: this._onSelectChange.bind(this),
			        }}
			        pagination={ pagination }
			        loading={loading}
			        onChange={this._getList.bind(this)}
				/>

        </Modal>);
	}
	componentDidMount() {
		this._getList();
	}
	_getSearchParam() {
		let {
			searchParma
		} = this.state;

		const form = this.form;

		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			/*如果值为undefined则替换为空*/
			values = undefinedToEmpty(values);
			// console.log(values)

			this.setState({
				searchParma: {
					...searchParma,
					...values
				},
				currentPage: 1
			}, () => {
				this._getList();
			});
		});
	}
	_onSelectChange(v) {
		// console.log(v);
		this.setState({
			selectedRowKeys: v
		});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		let {
			filterRegistrationFull
		} = this.props;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		// console.log('pager', pager);

		fpost('/api/educational/class/list', searchParma)
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;
				data.forEach((d) => {
					d.key = d.id
				});

				let total = Number(result.total);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						currentPage: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				}, () => {
					// console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
}

class MyForm extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			schoolAreaId
		} = this.props;

		return (<div>
			<Row>
				<Col span={ 20 }>
			        <Form layout="inline" >
						<FormItem>
							{getFieldDecorator('schoolAreaId',{
								initialValue:schoolAreaId
							})(
								<SelectSchool 
									onSelect={ 
										(v)=> {
											this.props.form.setFieldsValue({
												schoolAreaId:v
											});
										}
									} 
									initialValueData={ schoolAreaId }
									disabled={true}
								/>
							)}
						</FormItem>
						<FormItem >
							{getFieldDecorator('name')(
								<Input placeholder="请输入班级名称" />
							)}
						</FormItem>
						<FormItem >
							<Button
								type="primary"
								size="large"
								onClick={ this.props.onSearch }
							>
								搜索
							</Button>
						</FormItem>
			        </Form>
				</Col>
				<Col span={ 4 } className="f-align-right f-lh6">
					<a href="/teach/createClass" target="_blank" >新建班级</a>
				</Col>
			</Row>
		</div>);
	}
}
let MyFormCreate = Form.create()(MyForm);