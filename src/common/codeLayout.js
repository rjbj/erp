import React, {
	Component
} from 'react';
import {
	Row,
	Col,
	Layout
} from 'antd';
const {
	Header,
} = Layout;
export class CodeLayout extends Component {
	render() {
		return (
			<Header>
				<div className="f-header">
	                <Row>
	                    <Col span={4}>
	                        <h1 className="logo f-left f-white f-align-center f-align-center" style={{width:'200px'}}>
	                            <i className="iconfont icon-xiaoqu f-fz9 f-mr2"></i>
	                            e校通
	                        </h1>
	                     </Col>
	                  </Row>
	             </div>
             </Header>
		);
	}
}