import React, {
	Component
} from 'react';
import {
	Upload,
	Icon,
	message,
	Spin
} from 'antd';
import {
	host
} from './io.js';

export default class AvatarUpload extends Component {
	constructor(props) {
		super(props);

		this.state = {
			imageUrl: this.props.imageUrl,
			loading: false
		};
	}

	_handleChange(info) {
		console.log(this.state.loading);
		this.setState({
			loading: true
		});

		if (info.file.status === 'done' && info.file.response && info.file.response.success) {
			let {
				result
			} = info.file.response;
			console.log(result);
			let {
				id,
				url
			} = result;

			this.props.onSuccess({
				id,
				url
			});
			this.setState({
				imageUrl: url,
				loading: false
			});
		}
	}

	_beforeUpload(file) {
		const isJPG = true;
		if (!isJPG) {
			message.error('You can only upload JPG file!');
		}
		const isLt2M = file.size / 1024 / 1024 < 2;
		if (!isLt2M) {
			message.error('Image must smaller than 2MB!');
		}
		return isJPG && isLt2M;
	}

	render() {
		const {
			imageUrl,
			loading
		} = this.state;
		return (
			<Upload 
				className = "avatar-uploader avatar f-radius2 f-over-hide f-relative"
				name = "avatar"
				showUploadList = { false }
				action = {`${host}/api/common/resource/uploadImg`}
				beforeUpload = { this._beforeUpload.bind(this) }
				onChange = { this._handleChange.bind(this) }
			> 
				{
					imageUrl ?
					<img src={ imageUrl } alt="头像" style={{width:'100%'}} /> : <Icon type="plus" className="avatar-uploader-trigger" />
				} 
				{
					loading 
					? 
					<span style={{position:'absolute',left:'50%',top:'50%',marginTop:'-16px',marginLeft:'-16px'}}><Spin size="large" /></span> 
					: null
				}
			</Upload>
		);
	}
}