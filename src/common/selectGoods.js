import React, {
	Component
} from 'react';
import {
	Table,
	Button,
	Modal,
	Form,
	Select,
	Input
} from 'antd';
import {
	fpost
} from './io.js';

const FormItem = Form.Item;
const Option = Select.Option;

export default class SelectGoods extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterData: {}, //筛选字段
			selectedRowKeys: this.props.goodsDataKeys || [], //已经选择的数据id数组
			data: [], //表格数据
			isLoading: false, //是否正在请求列表数据
			pagination: { //分页配置信息
				showSizeChanger: true,
				showQuickJumper: true,
				size: "small",
				total: 1
			},
			currentPage: 1,
			pageSize: 99999999,
			total: 1,
			schoolAreaId: this.props.id || this.props.schoolAreaId || '', //校区id
			warehouseId: this.props.warehouseId || '' //仓库id
		}
	}
	render() {
		let {
			handleOk,
			handleCancel,
			purchasePrice, //是否显示采购价
		} = this.props;

		const {
			selectedRowKeys,
			data
		} = this.state;

		let selectData = [];
		if (selectedRowKeys.length) {
			selectedRowKeys.forEach((key) => {
				let curData;
				curData = data.filter((d) => {
					return d.key == key
				});
				if (curData[0]) {
					curData[0].salesPrice = curData[0].salePrice;
					selectData.push(curData[0]);
				}
			});
		}

		const columns = [{
			title: '名称',
			dataIndex: 'materialName'
		}, {
			title: '类型',
			dataIndex: 'category',
			className: "f-align-center"
		}, {
			title: purchasePrice ? '采购单价' : '单价',
			dataIndex: purchasePrice ? 'purchasePrice' : 'salePrice',
			className: "f-align-center"
		}, {
			title: '单位',
			dataIndex: 'unit'
		}, {
			title: '库存',
			dataIndex: 'inventory'
		}];
		// console.log(data);
		return (<Modal
			title="选择物品"
			width='80%'
			visible={ this.props.visible }
			onOk={ ()=> handleOk(selectData) }
			onCancel={ handleCancel }
			>
				<MyFormCreate 
					ref={ (form)=> this.form=form }
					onSearch= { this._getSearchParam.bind(this) }
				/>
				<div className="f-mb4" />
				<Table
				    columns={ columns }
				    dataSource={ data }
				    pagination={ this.state.pagination }
				    rowSelection = {{
				        selectedRowKeys,
				        onChange: this._onSelectChange.bind(this),
			        }}
				/>
				{/*<Table
				    columns={ columns }
				    dataSource={ data }
				    pagination={ this.state.pagination }
				    rowSelection = {{
				        selectedRowKeys,
				        onChange: this._onSelectChange.bind(this),
			        }}
			        onChange={
			        	(pager)=> {
			        		this._getList(pager);
			        	}
			        }
				/>*/}

        </Modal>);
	}
	componentDidMount() {
		this._getList();
	}
	_getList(pager = {}) { //获取列表数据
		this.setState({
			isLoading: true
		});
		let {
			pagination,
			currentPage,
			pageSize,
			total,
			filterData,
			schoolAreaId,
			warehouseId
		} = this.state;

		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;
		console.log(pager);

		fpost('/api/materialstock/pageMaterialStockVO', {
				currentPage,
				pageSize,
				...filterData,
				schoolAreaId,
				warehouseId,
				isDisable: 0
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					return;
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;

				data.forEach((d) => {
					d.key = d.materialId;
					d.originalPrice = d.salePrice;
				});
				let total = Number(result.total);
				this.setState({
					data,
					pagination: {
						...pagination,
						total: total,
						currentPage: currentPage,
						// pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
				});
			})
			.catch((err) => {
				this.setState({
					data: []
				});
				console.log(err);
			});;
	}
	_getSearchParam() {
		const form = this.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			this.setState({
				filterData: values
			}, () => {
				// console.log(this.state.filterData);
				this._getList();
			});
		});
	}
	_onSelectChange(v) {
		this.setState({
			selectedRowKeys: v
		});
	}
}

class MyForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			//类别
			typeOption: null
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			typeOption
		} = this.state;

		return (<div>
	        <Form layout="inline" >
				<FormItem 
					label="物品名称"
				>
					{getFieldDecorator('materialName',{
						initialValue:''
					})(
						<Input 
						    style={{ width: 200 }}
							placeholder="请输入物品名称" 
						/>
					)}
				</FormItem>
				<FormItem
					label="库存数量"
				>
					{getFieldDecorator('inventory',{
						initialValue:'-999'
					})(
						<Select
						    showSearch
						    style={{ width: 200 }}
						    placeholder="选择库存数量"
						>
						    <Option value="-999">不限</Option>
						    <Option value="0">等于0</Option>
						    <Option value="-1">小于0</Option>
						    <Option value="1">大于0</Option>
						</Select>
					)}
				</FormItem>
				{
					typeOption ?
						<FormItem
							label="类别"
						>
							{getFieldDecorator('category',{
								initialValue:'-999'
							})(
								<Select
								    showSearch
								    style={{ width: 200 }}
								    placeholder="选择类别"
							  	>
								    <Option value="-999">不限</Option>
							  		{
							  			typeOption.map( (d,i)=> <Option key={ d } value={ d }>{ d }</Option> )
							  		}
								</Select>
							)}
						</FormItem>
					: null
				}
				<FormItem >
					<Button
						type="primary"
						size="large"
						className="f-mr2"
						onClick={ this.props.onSearch }
					>
						搜索
					</Button>
					<Button
						size="large"
						onClick={ ()=> {
							this.props.form.resetFields();
						} }
					>
						重置
					</Button>
				</FormItem>
	        </Form>
		</div>);
	}
	componentDidMount() {
		this._getTypeOption();
	}
	_getTypeOption() {
		fpost('/api/material/listMaterialCategory')
			.then((res) => res.json())
			.then((res) => {
				if (!res.success || !res.result || !res.result.length) {
					return;
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					typeOption: result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}
let MyFormCreate = Form.create()(MyForm);