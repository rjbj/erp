import React, {
  Component
} from 'react';

import {
  DatePicker,
  Row,
  Col
} from 'antd';
import moment from 'moment';

export default class RangeDate extends Component {
  constructor(props) {
    super(props);
    let startString = (this.props.initialValueData && this.props.initialValueData.startTime) || null;
    let startValue = startString ? moment(startString) : null;
    let endString = (this.props.initialValueData && this.props.initialValueData.endTime) || null;
    let endValue = endString ? moment(endString) : null;

    this.state = {
      startValue: startValue,
      startString: startString,
      endValue: endValue,
      endString: endString,
      endOpen: false,
    }
  }

  disabledStartDate = (startValue) => {
    const endValue = this.state.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = (endValue) => {
    const startValue = this.state.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }

  onChange = (field, value) => {
    let {
      onSelect
    } = this.props;

    this.setState({
      [field]: value,
    }, () => {
      onSelect(this.state);
    });
  }

  onStartChange = (value, dateString) => {
    this.onChange('startValue', value);
    this.setState({
      startString: dateString
    });
  }

  onEndChange = (value, dateString) => {
    this.onChange('endValue', value);
    this.setState({
      endString: dateString
    });
  }

  handleStartOpenChange = (open) => {
    if (!open) {
      this.setState({
        endOpen: true
      });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({
      endOpen: open
    });
  }

  render() {
    const {
      startValue,
      endValue,
      endOpen
    } = this.state;
    let {
      width,
      size = "large",
      showTime = false,
      format = 'YYYY-MM-DD',
    } = this.props;

    if (showTime) {
      format = 'YYYY-MM-DD HH:mm:ss'
    }

    return (
      <Row gutter={8}>
        <Col span={12}>
          <DatePicker
            size={size}
            style={{
              width:'100%'
            }}
            showTime
            disabledDate={this.disabledStartDate}
            format={format}
            value={startValue}
            placeholder="开始"
            onChange={this.onStartChange}
            onOpenChange={this.handleStartOpenChange}
          />
        </Col>
        <Col span={12}>
          <DatePicker
            size={size}
            style={{
              width:'100%'
            }}
            showTime
            disabledDate={this.disabledEndDate}
            format={format}
            value={endValue}
            placeholder="结束"
            onChange={this.onEndChange}
            open={endOpen}
            onOpenChange={this.handleEndOpenChange}
          />
        </Col>
      </Row>
    );
  }
}