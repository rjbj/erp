import React, {
	Component
} from 'react';
import {
	Spin,
	Icon,
	message
} from 'antd';
import $ from 'jquery';
import {
	fpost
} from './io.js';

export const Loading = () => {
	return(<div style={{
		textAlign:'center',
		paddingTop:'50px'
	}}>
		<Spin /> <br /><br />
		<span className="colorGray">数据加载中...</span>
	</div>);
}

export function _getDays(start, end) { //计算两个日期之间的天数
	let arr = start.split("-");
	let starttime = new Date(arr[0], arr[1], arr[2]);
	let starttimes = starttime.getTime();
	let arrs = end.split("-");
	let endtime = new Date(arrs[0], arrs[1], arrs[2]);
	let endtimes = endtime.getTime();
	let intervalTime = endtimes - starttimes; //两个日期相差的毫秒数 一天86400000毫秒 
	let Inter_Days = ((intervalTime).toFixed(2) / 86400000) + 1; //加1，是让同一天的两个日期返回一天 
	return Inter_Days;
}

export function _getHistogram1(data, urldata, chart, $this, $name, color, chart1, $this1, chart2, $this2, type, type1, type2, $pageShow, $noData, fn) { //参考柱状图中的某地区蒸发量和降水量
	chart.clear();
	$pageShow.show();
	$noData.hide();
	chart.showLoading({
		text: "图表数据正在努力加载..."
	});
	if(chart1) {
		chart1.clear();
		chart1.showLoading({
			text: "图表数据正在努力加载..."
		});
	}
	if(chart2) {
		chart2.clear();
		chart2.showLoading({
			text: "图表数据正在努力加载..."
		});
	}
	let options1 = {
		title: {
			text: '',
			subtext: '',
			x: 'center'
		},
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		legend: {
			orient: 'vertical',
			left: 'left',
			data: []
		},
		toolbox: {
			show: true,
			feature: {
				dataView: {
					show: true,
					readOnly: false
				},
				saveAsImage: {
					show: true
				}
			}
		},
		series: [{
			name: '',
			type: 'pie',
			radius: '55%',
			center: ['50%', '60%'],
			data: [],
			itemStyle: {
				normal: {
					label: {
						show: true,
						formatter: function(val) { //让series 中的文字进行换行  
							if(val.name.length > 8) {
								return val.name.substring(0, 8) + '...';
							} else {
								return val.name
							}
						}
					},
					labelLine: {
						show: true
					}
				},
				emphasis: {
					shadowBlur: 10,
					shadowOffsetX: 0,
					shadowColor: 'rgba(0, 0, 0, 0.5)'
				}
			}
			//			itemStyle: {
			//				emphasis: {
			//					shadowBlur: 10,
			//					shadowOffsetX: 0,
			//					shadowColor: 'rgba(0, 0, 0, 0.5)'
			//				}
			//			}
		}]
	};
	let options = {
		title: {},
		tooltip: {
			trigger: 'axis'
		},
		legend: {
			data: []
		},
		toolbox: {
			show: true,
			feature: {
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['line', 'bar']
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		grid: {},
		calculable: true,
		xAxis: [{
			type: 'category',
			data: [],
			axisLabel: {
				interval: 0,
				formatter: function(value) {
					var ret = ""; //拼接加\n返回的类目项  
					var maxLength = 1; //每项显示文字个数  
					var valLength = value.length; //X轴类目项的文字个数  
					var rowN = Math.ceil(valLength / maxLength); //类目项需要换行的行数  
					if(rowN > 1) //如果类目项的文字大于3,  
					{
						for(var i = 0; i < rowN; i++) {
							var temp = ""; //每次截取的字符串  
							var start = i * maxLength; //开始截取的位置  
							var end = start + maxLength; //结束截取的位置  
							temp = value.substring(start, end) + "\n";
							ret += temp; //凭借最终的字符串  
						}
						return ret;
					} else {
						return value;
					}
				}
			}
		}],
		yAxis: [{
			type: 'value'
		}],
		series: []
	};
	options.title = $name;
	fpost(urldata, data)
		.then((res) => {
			return res.json();
		})
		.then((res) => {
			if(res.success == true) {
				if(type == 1) { //柱状图
					if(res.result.barChart) {
						$this.css({
							'height': '500px'
						});
						options.legend.data = res.result.barChart.legendData;
						options.xAxis[0].data = res.result.barChart.xAxisData;
						options.series = res.result.barChart.barChartSeries;
						options.series[0].color = color.color1;
						options.series[1].color = color.color2;
						chart.hideLoading();
						chart.setOption(options);
					} else {
						chart.hideLoading();
						chart.setOption({
							title: {
								text: '暂无数据......',
								textStyle: {
									fontSize: 16,
									color: 'black',
									align: 'center',
									fontWeight: "normal"
								},
							}
						});
						$this.css({
							'height': '40px'
						})
					}
				}
				if(type1 == 2) { //饼图
					if(res.result.pieChart1) {
						$this1.css({
							'height': '400px'
						});
						options1.legend.data = res.result.pieChart1.legendData;
						options1.series[0].data = res.result.pieChart1.series[0].data;
						options1.series[0].name = res.result.pieChart1.series[0].name;
						options1.title.text = res.result.pieChart1.title;
						chart1.hideLoading();
						chart1.setOption(options1);
					} else {
						chart1.hideLoading();
						chart1.setOption({
							title: {
								text: '暂无数据......',
								textStyle: {
									fontSize: 16,
									color: 'black',
									align: 'center',
									fontWeight: "normal"
								},
							}
						});
						$this1.css({
							'height': '40px'
						})
					}
				}
				if(type2 == 3) { //饼图
					if(res.result.pieChart2) {
						$this2.css({
							'height': '400px'
						});
						options1.legend.data = res.result.pieChart2.legendData;
						options1.series[0].data = res.result.pieChart2.series[0].data;
						options1.series[0].name = res.result.pieChart2.series[0].name;
						options1.title.text = res.result.pieChart2.title;
						chart2.hideLoading();
						chart2.setOption(options1);
					} else {
						chart2.hideLoading();
						chart2.setOption({
							title: {
								text: '暂无数据......',
								textStyle: {
									fontSize: 16,
									color: 'black',
									align: 'center',
									fontWeight: "normal"
								},
							}
						});
						$this2.css({
							'height': '40px'
						})
					}
				}
			} else {
				message.error(res.message)
			}
		});
}

export function _getHistogram2(data, urldata, chart, $this, $name, chart1, $this1, $name1, type, type1, $pageShow, $noData, fn) { //参考坐标轴刻度与标签对齐
	chart.clear();
	$pageShow.show();
	$noData.hide();
	chart.showLoading({
		text: "图表数据正在努力加载..."
	});
	chart1.clear();
	chart1.showLoading({
		text: "图表数据正在努力加载..."
	});
	let options1 = {
		title: {
			text: '',
			subtext: '',
			x: 'center'
		},
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		legend: {
			orient: 'vertical',
			left: 'left',
			data: []
		},
		toolbox: {
			show: true,
			feature: {
				dataView: {
					show: true,
					readOnly: false
				},
				saveAsImage: {
					show: true
				}
			}
		},
		series: [{
			name: 'ss',
			type: 'pie',
			radius: '55%',
			center: ['50%', '60%'],
			data: [],
			itemStyle: {
				normal: {
					label: {
						show: true,
						formatter: function(val) { //让series 中的文字进行换行  
							if(val.name.length > 8) {
								return val.name.substring(0, 8) + '...';
							} else {
								return val.name
							}
						}
					},
					labelLine: {
						show: true
					}
				},
				emphasis: {
					shadowBlur: 10,
					shadowOffsetX: 0,
					shadowColor: 'rgba(0, 0, 0, 0.5)'
				}
			}
		}]
	};
	let options = {
		title: {
			text: '',
			subtext: ''
		},
		toolbox: {
			show: true,
			feature: {
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['line', 'bar']
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		color: ['#FF6060'],
		tooltip: {
			trigger: 'axis',
			axisPointer: { // 坐标轴指示器，坐标轴触发有效
				type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
			}
		},
		grid: {
			left: '3%',
			right: '4%',
			bottom: '3%',
			containLabel: true
		},
		xAxis: [{
			type: 'category',
			data: [],
			axisTick: {
				alignWithLabel: true
			},
			axisLabel: {
				interval: 0,
				formatter: function(value) {
					var ret = ""; //拼接加\n返回的类目项  
					var maxLength = 1; //每项显示文字个数  
					var valLength = value.length; //X轴类目项的文字个数  
					var rowN = Math.ceil(valLength / maxLength); //类目项需要换行的行数  
					if(rowN > 1) //如果类目项的文字大于3,  
					{
						for(var i = 0; i < rowN; i++) {
							var temp = ""; //每次截取的字符串  
							var start = i * maxLength; //开始截取的位置  
							var end = start + maxLength; //结束截取的位置  
							temp = value.substring(start, end) + "\n";
							ret += temp; //凭借最终的字符串  
						}
						return ret;
					} else {
						return value;
					}
				}
			}
		}],
		yAxis: [{
			type: 'value'
		}],
		series: []
	};
	options.title = $name;
	fpost(urldata, data)
		.then((res) => {
			return res.json();
		})
		.then((res) => {
			if(res.success == true) {
				if(type == 2) { //柱状图
					if(res.result.barChart) {
						$this.css({
							'height': '500px'
						});
						options.xAxis[0].data = res.result.barChart.xAxisData;
						options.series = res.result.barChart.barChartSeries;
						chart.hideLoading();
						chart.setOption(options);
					} else {
						chart.hideLoading();
						chart.setOption({
							title: {
								text: '暂无数据......',
								textStyle: {
									fontSize: 16,
									color: 'black',
									fontWeight: "normal"
								},
							}
						});
						$this.css({
							'height': '40px'
						})
					}
				}
				if(type1 == 1) { //饼图
					if(res.result.pieChart) {
						$this1.css({
							'height': '400px'
						});
						options1.legend.data = res.result.pieChart.legendData;
						options1.series[0].data = res.result.pieChart.series[0].data;
						options1.series[0].name = res.result.pieChart.series[0].name;
						options1.title.text = res.result.pieChart.title;
						chart1.hideLoading();
						chart1.setOption(options1);
					} else {
						chart1.hideLoading();
						chart1.setOption({
							title: {
								text: '暂无数据......',
								textStyle: {
									fontSize: 16,
									color: 'black',
									fontWeight: "normal"
								},
							}
						});
						$this1.css({
							'height': '40px'
						})
					}
				}
			} else {
				message.error(res.message)
			}
		});
}
// 这是截取字符串的方法
export function truncate(str, len) {
	if(str.html().length > len) {
		var new_str = str.html().substr(0, len + 1);
		while(new_str.length) {
			var ch = new_str.substr(-1); //字符串是否为空的验证
			new_str = new_str.substr(0, -1);
			if(ch == ' ') {
				break;
			}
		}
		if(new_str == '') {
			new_str = str.html().substr(0, len);
		}
	}
	str.html(new_str + '...');
}

export const NoData = () => {
	return(<div style={{
    textAlign:'center',
    paddingTop:'50px'
  }}>
    <Icon
      style={{
        color:'#999',
        fontSize:'40px'
      }}
      type="exclamation-circle" 
    /> <br /><br />
    <span className="colorGray">暂无数据...</span>
  </div>);
}

/*获取url中的参数*/
export function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if(r != null) return unescape(r[2]);
	return null;
}

/*支持中文*/
export function getUnicodeParam(key) {
	// 获取参数
	var url = window.location.search;
	// 正则筛选地址栏
	var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	// 匹配目标参数
	var result = url.substr(1).match(reg);
	//返回参数值
	return result ? decodeURIComponent(result[2]) : null;
}

export function BrowserType() {
	var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
	var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器  
	var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判断是否IE浏览器  
	var isEdge = userAgent.indexOf("Windows NT 6.1; Trident/7.0;") > -1 && !isIE; //判断是否IE的Edge浏览器  
	var isFF = userAgent.indexOf("Firefox") > -1; //判断是否Firefox浏览器  
	var isSafari = userAgent.indexOf("Safari") > -1 && userAgent.indexOf("Chrome") == -1; //判断是否Safari浏览器  
	var isChrome = userAgent.indexOf("Chrome") > -1 && userAgent.indexOf("Safari") > -1; //判断Chrome浏览器  

	if(!isIE) {
		var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
		reIE.test(userAgent);
		var fIEVersion = parseFloat(RegExp["$1"]);
		return {
			name: 'ie',
			version: fIEVersion
		}
	};

	return null;
}

//列表分页配置
export const Pagination = (total, searchPara, getDataList, callback) => {
	return {
		total: parseInt(total),
		pageSize: parseInt(searchPara.pageSize),
		showSizeChanger: true,
		showQuickJumper: true,
		current: searchPara.currentPage,
		onChange: (page, pageSize) => {
			searchPara.currentPage = page;
			getDataList(searchPara);
			if(callback != undefined) callback();
		},
		onShowSizeChange: (current, size) => {
			searchPara.pageSize = size;
			searchPara.currentPage = 1;
			getDataList(searchPara);
		}
	}
}

export let undefinedToEmpty = (values) => { //将undefined 或者 null 转换为空,并去除首位空格

	for(let key in values) {
		if(values[key] == undefined || values[key] == null) {
			values[key] = '';
		} else if(typeof values[key] === 'string') {
			values[key] = values[key].toString().replace(/^\s+|\s+$/g, '');
		} else if(typeof values[key] == 'object') {
			values[key] = undefinedToEmpty(values[key]);
		}
	}
	return values;
}

export let numberFormate = (v) => { //金额千分位加保留两位小数的展示
	v = v ? v : 0;
	v = Number(v).toFixed(2);
	v = v ? (v.toString()).replace(/\B(?=(\d{3})+(?!\d))/g, ',') : 0;

	return v;
}

export let changeTwoDecimal = function(x) {
	var f_x = parseFloat(x);
	if(isNaN(f_x)) {
		f_x = 0;
	}
	f_x = Math.round(f_x * 100) / 100;
	return f_x;
}