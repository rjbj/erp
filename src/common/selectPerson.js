import React, {
    Component
} from 'react';
import {
    Select,
    Spin
} from 'antd';
import debounce from 'lodash.debounce';
import {
    fpost
} from './io.js';
const Option = Select.Option;

export default class UserRemoteSelect extends Component {
    constructor(props) {
        super(props);
        this.lastFetchId = 0;
        this.fetchUser = debounce(this.fetchUser, 800);
    }
    state = {
        data: [],
        value: '',
        fetching: false,
        pid: this.props.initialValueData || '' //设置默认值所用
    }
    fetchUser = (name = '') => {
        let pid = this.state.pid || '';
        let {
            data,
            url
        } = this.props;
        url = url ? url : '/api/hr/staff/listStaffs';
        let param = {
            name: name
        }
        if (data) {
            param = {
                ...param,
                ...data
            }
        }
        console.log(param);

        this.setState({
            fetching: true
        });
        fpost(url, param)
            .then(res => res.json())
            .then((res) => {
                if (res.code == 'OK' && res.result) {
                    let data = res.result;
                    data.forEach((d) => {
                        d.label = d.name;
                    });

                    /*设置默认值，通过父组件传入的pid*/
                    if (pid) {
                        let value = '',
                            obj = null;
                        obj = data.find((objdata) => objdata.id == pid);
                        value = (obj && obj.name) ? `${obj.name}-${obj.department}-${obj.mobile}` : value;
                        this.setState({
                            value,
                            pid: ''
                        });
                    }

                    this.setState({
                        data
                    });
                }
            });
    }
    handleChange = (value) => {
        // console.log('handleChange', value);
        let {
            data
        } = this.state;

        let obj = data.find((d) => d.id == value);
        value = (obj && obj.name) ? `${obj.name}-${obj.department}-${obj.mobile}` : value;
        let pid = (obj && obj.id) ? obj.id : '';

        this.setState({
            value,
            data: [],
            fetching: false,
        });

        /*将数据传递给父组件*/
        this.props.onSelect(pid);
    }
    onSelect = (pid) => {
        // console.log('onSelect', pid);
    }
    render() {
        let {
            fetching,
            data,
            value,
        } = this.state;

        let {
            width
        } = this.props;

        // console.log('value:',value);

        return (
            <Select
                mode="combobox"
                size="large"
                allowClear
                value={ value }
                placeholder="请选择人员"
                notFoundContent={fetching ? <Spin size="small" /> : null}
                filterOption={false}
                onSearch={ this.fetchUser }
                onChange={ this.handleChange }
                onSelect={ this.onSelect }
                style={{ width: width ? width : '100%',minWidth:'60px' }}
            >
                {
                    data.map(d => <Option value={ d.id } key={d.id}>
                        { `${d.name}-${d.department}-${d.mobile}` }
                    </Option>)
                }
            </Select>
        );
    }
    componentDidMount() {
        this.fetchUser();
    }
}