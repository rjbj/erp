import 'babel-polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import Routers from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.less';

if (!String.prototype.trim) {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

ReactDOM.render(<Routers />, document.getElementById('root'));
registerServiceWorker();