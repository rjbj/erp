import React, { Component } from 'react';
import { Form, Breadcrumb, Table, Input, Select, Button, Checkbox, DatePicker, message } from 'antd';
import { fpost } from '../common/io.js';
import { Loading, Pagination, undefinedToEmpty } from '../common/g.js';

const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const CheckboxGroup = Checkbox.Group;
const FormItem = Form.Item;
const Option = Select.Option;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			inOutDataList: null,
			stockList: [],
			categoryList: [],
			warehouseId: '',
			firstWarehouseId: '',
			total: 1,
			searchPara: {
				currentPage: 1,
				pageSize: 10
			}
		}
	}
	getInOutList(para) {
		fpost('/api/materialstock/pageMaterialStockModifyVO', para).then(res=>res.json()).then(res=>{
			if(res.success == true) {
				let arr = [];
				res.result.records.map((e, i)=>{
					e.key = i;
					arr.push(e);
				});
				this.setState({
					inOutDataList: arr,
					searchPara: para,
					total: res.result.total
				})
			}else {
				message.error(res.message);
			}
		})
	}
	updateWarehouseId(v) {
		this.setState({
			warehouseId: v
		})
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount() {
		let self = this;
		//查询仓库列表
		fpost('/api/warehouse/listWarehouseSimpleVO').then(res=>res.json()).then(res=>{
			if(res.success == true){
				this.setState({
					stockList: res.result,
					warehouseId: res.result.length? res.result[0].warehouseId : '',
					firstWarehouseId:  res.result.length? res.result[0].warehouseId : ''
				}, function(){
					if(self.state.warehouseId == '') {
						self.setState({
							inOutDataList: []
						})
					}else {
						self.getInOutList({ currentPage: 1, pageSize: 10, operTypes: [], warehouseId: self.state.warehouseId })
					}
				})
			}else{
				message.error(res.message);
			}
		});

		//查询分类列表
		fpost('/api/material/listMaterialCategory').then(res=>res.json()).then(res=>{
			if(res.success == true){
				this.setState({
					categoryList: res.result
				})
			}else{
				message.error(res.message);
			}
		});
	}
	render(){
		const columns = [
			{
			  title: '物品名称',
			  dataIndex: 'materialName',
			  key: 'materialName',
			  className:'f-align-center'
			},
			{
			  title: '类别',
			  dataIndex: 'category',
			  key: 'category',
			  className:'f-align-center'
			},
			{
			  title: '调整仓库',
			  dataIndex: 'warehouseName',
			  key: 'warehouseName',
			  className:'f-align-center'
			},
			{
			  title: '调整校区',
			  dataIndex: 'schoolAreaName',
			  key: 'schoolAreaName',
			  className:'f-align-center'
			},
			{
			  title: '操作类型',
			  dataIndex: 'type',
			  key: 'type',
			  className:'f-align-center',
			  render: text => (
			  	<span>{text=='1'? '进货' : text=='2'? '退货' : text=='3'? '调拨出' : text=='4'? '调拨入' : text=='5'? '报损' : text=='6'? '内部员工领用' : text=='7'? '内部员工退领' : text=='8'? '销售' : text=='9'? '销售退回' : text=='10'? '库存调整' : ''}</span>
			  )
			},
			{
			  title: '数量',
			  dataIndex: 'quantity',
			  key: 'quantity',
			  className:'f-align-center'
			},
			{
			  title: '单位',
			  dataIndex: 'unit',
			  key: 'unit',
			  className:'f-align-center'
			},
			{
			  title: '单价',
			  dataIndex: 'unitPrice',
			  key: 'unitPrice',
			  className:'f-align-center'
			},
			{
			  title: '操作日期',
			  dataIndex: 'createTime',
			  key: 'createTime',
			  className:'f-align-center'
			},
			{
			  title: '操作人',
			  dataIndex: 'operatorName',
			  key: 'operatorName',
			  className:'f-align-center'
			},
			{
			  title: '备注',
			  dataIndex: 'remark',
			  key: 'remark',
			  className:'f-align-center'
			}];
		return (<div className='courseArranging classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>进出库查询</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<SearchFormCreate pageSize={this.state.searchPara.pageSize} stockList={ this.state.stockList } setPara={ this.setPara.bind(this) } getInOutList={ this.getInOutList.bind(this) } updateWarehouseId={ this.updateWarehouseId.bind(this) } categoryList={ this.state.categoryList } warehouseId={ this.state.warehouseId } firstWarehouseId={ this.state.firstWarehouseId }/>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">进出库列表</h3>
						</div>
						{
							this.state.inOutDataList != null?
							<Table columns={columns} dataSource={this.state.inOutDataList} bordered={true} pagination={ Pagination(this.state.total, this.state.searchPara, this.getInOutList.bind(this)) } scroll={{ x: 1300 }}/>
							: <Loading />
						}
					</div>
				</div>)
	}
}

const plainOptions = [
						{label: '进货', value: 1},
 						{label: '退货', value: 2},
  						{label: '调拨出', value: 3},
  						{label: '调拨入', value: 4},
  						{label: '报损', value: 5},
  						{label: '领用', value: 6},
  						{label: '退领', value: 7},
  						{label: '库存调整', value: 10},
  						{label: '销售', value: 8},
  						{label: '销售退回', value: 9}
  					];
const defaultCheckedList = [];

class SearchForm extends Component {
	constructor(props){
		super(props);
		this.state = {
		    checkedList: defaultCheckedList,
	        indeterminate: false,
	        checkAll: false,
		    startValue: null,
		    endValue: null,
		    endOpen: false,
		  };
	}
	changeUnit = (checkedList) => {
		this.setState({
		  checkedList,
		  indeterminate: !!checkedList.length && (checkedList.length < plainOptions.length),
		  checkAll: checkedList.length === plainOptions.length,
		});
	}
	onCheckAllChange = (e) => {
		this.setState({
		  checkedList: e.target.checked ? [1,2,3,4,5,6,7,10,8,9] : [],
		  indeterminate: false,
		  checkAll: e.target.checked,
		});
	}
	disabledStartDate = (startValue) => {
	    const endValue = this.state.endValue;
	    if (!startValue || !endValue) {
	      return false;
	    }
	    return startValue.valueOf() > endValue.valueOf();
	  }

	  disabledEndDate = (endValue) => {
	    const startValue = this.state.startValue;
	    if (!endValue || !startValue) {
	      return false;
	    }
	    return endValue.valueOf() <= startValue.valueOf();
	  }
	  onChange = (field, value) => {
	    this.setState({
	      [field]: value,
	    });
	  }
	  onStartChange = (value) => {
	    this.onChange('startValue', value);
	  }

	  onEndChange = (value) => {
	    this.onChange('endValue', value);
	  }

	  handleStartOpenChange = (open) => {
	    if (!open) {
	      this.setState({ endOpen: true });
	    }
	  }

	  handleEndOpenChange = (open) => {
	    this.setState({ endOpen: open });
	  }
	  updateWarehouseId(v) {
		this.props.updateWarehouseId(v);
	}
	searchCondition() {
		let obj = this.props.form.getFieldsValue(), pageSize = this.props.pageSize;
		obj.currentPage = 1;
		obj.pageSize = pageSize;
		obj.startTime = obj.startTime? obj.startTime.format(dateFormat) : '';
		obj.endTime = obj.endTime? obj.endTime.format(dateFormat) : '';
		obj.warehouseId = obj.warehouseId? obj.warehouseId : '';
		obj.operTypes = this.state.checkedList;
		obj.category = obj.category? obj.category : '';

		obj = undefinedToEmpty(obj);
		this.props.getInOutList(obj);
		this.props.setPara(obj);
	}
	resetForm(){
		let form = this.props.form, pageSize = this.props.pageSize;
		form.resetFields();
		this.setState({
			checkedList: [],
			checkAll: false,
			indeterminate: false
		}, function(){
			this.props.getInOutList({ currentPage: 1, pageSize: pageSize, operTypes: [], warehouseId: this.props.firstWarehouseId });
		});
	}
	render() {
		const { getFieldDecorator } = this.props.form;
		const { startValue, endValue, endOpen } = this.state;

		return (<Form layout={'inline'}>
					<FormItem style={{display:'block', marginBottom:'20px'}} label='操作类型' wrapperCol={{style:{display:'inline-block', verticalAlign:'middle', width:'90%'}}}>
						{getFieldDecorator('operTypes', {
				            rules: [{
				              required: false
				            }]
				          })(
			            	<div>
    			            	<div className='f-inline-block f-vertical-middle'>
    		            	      <Checkbox indeterminate={this.state.indeterminate} onChange={this.onCheckAllChange} checked={this.state.checkAll}>全选</Checkbox>
    			            	</div>
    				            <CheckboxGroup className='f-inline-block f-vertical-middle' options={plainOptions} value={this.state.checkedList} onChange={this.changeUnit} />
			            	</div>
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} label='仓库' wrapperCol={{style:{width:'50%', display:'inline-block', verticalAlign:'middle'}}}>
						{getFieldDecorator('warehouseId', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: this.props.stockList.length? this.props.stockList[0].warehouseId : undefined
				          })(
				            <Select
				                showSearch
				                style={{ width: 200 }}
				                placeholder="请选择"
								onChange={ this.updateWarehouseId.bind(this) }
				                optionFilterProp="children"
				                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				              >
							    {
							    	this.props.stockList.length? this.props.stockList.map((e, i)=>{
										return (<Option key={i} value={ e.warehouseId }>{ e.warehouseName }</Option>)
							    	}) : ''
							    }
				              </Select>
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }}
						label="选择时段"
						wrapperCol={{style:{width:'200px'}}}
					>
						{
							getFieldDecorator('startTime', { 
								
							})(<DatePicker
						          disabledDate={this.disabledStartDate}
						          showTime
						          format="YYYY-MM-DD HH:mm:ss"
						          setFieldsValue={startValue}
						          placeholder="开始时间"
						          onChange={this.onStartChange}
						          onOpenChange={this.handleStartOpenChange}
						        />)
						}
					</FormItem>
					<div className='f-inline-block' style={{lineHeight:'30px'}}>--</div>
					<FormItem style={{ marginBottom:'20px' }} className='f-ml4'
						wrapperCol={{style:{width:'200px'}}}
					>
						{
							getFieldDecorator('endTime', { 
								
							})(<DatePicker
						          disabledDate={this.disabledEndDate}
						          showTime
						          format="YYYY-MM-DD HH:mm:ss"
						          setFieldsValue={endValue}
						          placeholder="结束时间"
						          onChange={this.onEndChange}
						          open={endOpen}
						          onOpenChange={this.handleEndOpenChange}
						        />)
						}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} label='物品名称' wrapperCol={{style:{width:'200px'}}}>
						{getFieldDecorator('materialName', {
				            rules: [{
				              required: false,
				              message: '请输入物品名称',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入物品名称" />
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} label='类别' wrapperCol={{style:{width:'50%', display:'inline-block', verticalAlign:'middle'}}}>
						{getFieldDecorator('category', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: undefined
				          })(
				            <Select
				                showSearch
				                style={{ width: 200 }}
				                placeholder="请选择"
				                optionFilterProp="children"
				                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				              >
							    {
							    	this.props.categoryList.length? this.props.categoryList.map((e, i)=>{ 
										return (<Option key={i} value={ e }>{ e }</Option>)
							    	 }) : ''
							    }
				              </Select>
				          )}
					</FormItem>
					<FormItem>
						<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.searchCondition.bind(this) }>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
	        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
	        				<i className='iconfont icon-reset f-mr2'></i>
	        				重置
	        			</Button>
					</FormItem>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);