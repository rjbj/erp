import React, {
  Component
} from 'react';
import {
  Table,
  Popconfirm,
  Button
} from 'antd';
import {
  Link
} from 'react-router-dom';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    const columns = [{
      title: '领取人姓名',
      dataIndex: 'receriverName',
      width: '100px'
    }, {
      title: '领取人电话',
      dataIndex: 'receriverPhone',
      className: 'f-align-center',
      width: '150px'
    }, {
      title: '仓库名称',
      dataIndex: 'warehouseName',
      width: '200px'
    }, {
      title: '物品名称',
      dataIndex: 'materialName',
      width: '200px'
    }, {
      title: '数量',
      className: 'f-align-center',
      dataIndex: 'qty',
      width: '50px'
    }, {
      title: '售卖类型',
      className: 'f-align-center',
      dataIndex: 'operTypeStr',
      width: '100px'
    }, {
      title: '是否学员',
      className: 'f-align-center',
      dataIndex: 'isStudentStr',
      width: '100px'
    }, {
      title: '是否领取',
      className: 'f-align-center',
      dataIndex: 'isTakenStr',
      width: '100px'
    }, {
      title: '领取时间',
      dataIndex: 'takenTime',
      className: 'f-align-center',
      width: '200px'
    }, {
      title: '操作员',
      dataIndex: 'operatorName',
      className: 'f-align-center',
      width: '100px'
    }, {
      title: '操作',
      dataIndex: 'des',
      fixed: 'right',
      width: '100px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          id,
          isTaken
        } = row;
        return (<div>
            {
              isTaken && isTaken == 1 ?
                <span className="f-pale">已领取</span>
              :
              <Popconfirm 
                  title="确定要领取吗?" 
                  onConfirm={
                    ()=> this.props.get(id)
                  }
                  okText="确定" 
                  cancelText="取消"
                >
                  <Button
                    className="ant-btn f-lh4"
                    type="primary"
                  >
                    领取
                  </Button>
              </Popconfirm>
            }
        </div>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">物品领取列表</span>
        </h3>
        <Table
            columns={ columns }
            bordered
            dataSource={ data }
            scroll={{ x: 1300}}
            pagination={ pagination }
            loading={loading}
            onChange={onChange}
          />
      </div>
    );
  }

}