import React, { Component } from 'react';
import { Breadcrumb, Form, Select, Input, DatePicker, Button, Modal, Table, Row, Col, message, InputNumber } from 'antd';
import { default as Tablinks } from './links.js';
import { fpost, fpostArray } from '../../common/io.js';
import { Loading, Pagination, undefinedToEmpty, numberFormate } from '../../common/g.js';
import { default as SelectGoods } from '../../common/selectGoods';
import moment from 'moment';

const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const FormItem = Form.Item;
const Option = Select.Option;

export default class Main extends Component {
	constructor(props){
		super(props);
		this.state = {
			visibleType: false,
			inOutListData: null,
			stockList: [],
			goodsBoxVisible: false,
			chooseGoodsList: [],
			isView: false,
			warehouseId: '',
			addColumns: [],
			total: 1,
			totalMoney: 0,
			warehouseName: '',
			findResult: {},
			isRequest: false,
			searchPara: {
				currentPage: 1,
				pageSize: 10,
				operType: 1
			}
		}
		this.showGoodsDetail.bind(this);
		this.removeChooseGoods.bind(this);
		this.updateChooseGoods.bind(this);
	}
	updateChooseGoods(v, id, type) {
		let chooseGoodsData = this.state.chooseGoodsList, totalMoney = 0;
		chooseGoodsData.map((o, i)=>{
			if(o.materialId == id){
				if(type == 'num'){
					o.quantity = v;
					//o.unitPrice = o.unitPrice || o.purchasePrice;
				}else if(type == 'price'){
					//o.unitPrice = v != undefined ? v : o.purchasePrice;
					o.unitPrice = v;
				}
				o.materialTotalAmount = parseInt(o.quantity) * o.unitPrice || 0;
				o.beforeModifyQuantityTemp = o.inventory;
			}
			o.purchasePrice = typeof o.purchasePrice == 'string'? Number(o.purchasePrice.replace(/,/g, '')) : o.purchasePrice;
			o.materialTotalAmount = typeof o.materialTotalAmount == 'string'? Number(o.materialTotalAmount.replace(/,/g, '')) : o.materialTotalAmount;
			totalMoney += o.materialTotalAmount;
			o.purchasePrice = numberFormate(o.purchasePrice);
			o.materialTotalAmount = numberFormate(o.materialTotalAmount);
			return o;
		});
		this.setState({
			chooseGoodsList: chooseGoodsData,
			totalMoney
		});
	}
	addGoods(){
		if(!this.searchForm.props.form.getFieldsValue().warehouseId) {
			message.warning('请先选择仓库!');
			return;
		}
		this.setState({
			visibleType: true,
			isView: false,
			addColumns: [
							{
							  title: '物品名称',
							  dataIndex: 'materialName',
							  key: 'materialName',
							  className:'f-align-center'
							},
							{
							  title: '规格',
							  dataIndex: 'specify',
							  key: 'specify',
							  className:'f-align-center'
							},
							{
							  title: '单位',
							  dataIndex: 'unit',
							  key: 'unit',
							  className:'f-align-center'
							},
							{
							  title: '数量',
							  key: 'quantity',
							  className:'f-align-center',
							  render: (text, record) => (
							  	<FormItem style={{ marginBottom: 0}}>
								  	<InputNumber precision={0} className='f-center-number' style={{width:'150px'}} min={1} max={9999} defaultValue={1} placeholder="请输入物品数量" onChange={ (e)=>{ this.updateChooseGoods(e, record.materialId, 'num') } }/>
								</FormItem>
							  )
							},
							{
							  title: '进货单价',
							  key: 'purchasePrice',
							  className:'f-align-center',
							  render: (text, record) => (
								<FormItem style={{ marginBottom: 0}}>
									<InputNumber precision={2} formatter={val=>numberFormate(val)} className='f-center-number' min={0} max={99999.99} style={{width:'150px'}} placeholder="请输入进货单价" defaultValue={ record.purchasePrice } onChange={ (e)=>{ this.updateChooseGoods(e, record.materialId, 'price') } }/>
								</FormItem>
							  )
							},
							{
							  title: '金额',
							  dataIndex: 'materialTotalAmount',
							  key: 'materialTotalAmount',
							  className:'f-align-center',
							  render: (text, record) => (
									<span>{ text != undefined? text : record.purchasePrice}</span>
							  )
							},
							{
							  title: '操作',
							  key: 'action',
							  className:'f-align-center',
							  render: (text, record) => (
								<Button onClick={ ()=>{ this.removeChooseGoods(record.key) } }>删除</Button>
							  )
							}
						]
		})
	}
	saveData(){
		const { pageSize } = this.state.searchPara, warehouseId = this.state.warehouseId;
		if(!this.state.isView){
			let obj = this.addForm.props.form.getFieldsValue(), isError = false;
				obj.occurringDate = obj.occurringDate.format('YYYY-MM-DD HH:mm:ss');
				obj.materials = this.state.chooseGoodsList.map((o, i)=>{
					//o.unitPrice = o.purchasePrice;
					if(o.quantity==undefined || o.unitPrice == undefined) {
						message.warning('请填写完表格或删除不必要的记录!');
						isError = true;
					}
					o.beforeModifyQuantityTemp = o.inventory;
					return o;
				});

				if(isError) return;

				obj.operType = 1;
				obj.currentPage = 1;
				obj.pageSize = pageSize;
				obj.warehouseId = this.state.warehouseId;

			if(obj.materials.length == 0) {
				message.warning('请先选择物品!');
				return;
			}	

			this.addForm.props.form.validateFields(	
					(err, fieldsValue) => {
			        	if(err){
							message.error('请填写完整!');
							isError = true;
			           }		
				}
			)

			if(isError) return;

			this.setState({
				isRequest: true
			})

			obj = undefinedToEmpty(obj);

			fpostArray('/api/materialstock/updateMaterialStockModify', obj).then(res=>res.json()).then(res=>{
				if(res.success == true) {
					this.setState({
						visibleType: false,
						chooseGoodsList: [],
						totalMoney: 0,
						warehouseId: ''
					}, function(){
						this.setState({
							warehouseId: warehouseId
						})
					})
					message.success(res.message);
					this.addForm.props.form.resetFields();
					this.getInOutList({currentPage: 1, pageSize, operType: 1, warehouseId: this.state.warehouseId});
				}else {
					message.error(res.message);
				}

				this.setState({
					isRequest: false
				})
			})
		}
	}
	cancelAddGoods(){
		this.setState({
			visibleType: false,
			chooseGoodsList: [],
			isView: false,
			totalMoney: 0
		})
		this.addForm.props.form.resetFields();
	}
	getInOutList(para){

		//查询进货列表
		fpost('/api/materialstock/pageMaterialStockModifyInUpdatePage', para).then(res=>res.json()).then(res=>{
			if(res.success == true){
				let data = [];
				res.result.records.forEach((e, i)=>{
					e.key = i;
					data.push(e);
				});
				this.setState({
					inOutListData: data,
					total: res.result.total,
					searchPara: para
				})
			}else{
				message.error(res.message);
			}
		});
	}
	cancelGoodsBox() {
		this.setState({
			goodsBoxVisible: false
		});
	}
	showGoodsList() {
		this.setState({
			goodsBoxVisible: true
		});
	}
	afterSelectGoods(data) {
		let totalMoney = this.state.totalMoney, arr = [];	
			totalMoney = typeof totalMoney == 'string'? Number(this.state.totalMoney.replace(/,/g, '')) : totalMoney;	
		arr = data.filter((e, i)=>{
				let isRepeat = this.state.chooseGoodsList.some((o)=>{ if(e.materialId == o.materialId) return true; });
				e.materialTotalAmount = numberFormate(e.purchasePrice);
				e.beforeModifyQuantityTemp = e.inventory;
				e.quantity = 1;
				e.unitPrice = e.purchasePrice;
				if(!isRepeat) totalMoney+=e.quantity*e.purchasePrice;
				return !isRepeat;
			});
		this.setState({
			chooseGoodsList: [...this.state.chooseGoodsList, ...arr],
			totalMoney,
			goodsBoxVisible: false
		})
	}
	showGoodsDetail( id ) {
		fpost('/api/materialstock/listMaterialInStockModifyDetail', { materialStockModifyId: id }).then(res=>res.json()).then(res=>{
			if(res.success == true){
				let arr = [];
				res.result.materialList.forEach((e, i)=>{
					e.key = e.materialId;
					arr.push(e);
				})
				this.setState({
					chooseGoodsList: arr,
					visibleType: true,
					findResult: res.result,
					isView: true,
					addColumns: [
							{
							  title: '物品名称',
							  dataIndex: 'materialName',
							  key: 'materialName',
							  className:'f-align-center'
							},
							{
							  title: '规格',
							  dataIndex: 'specify',
							  key: 'specify',
							  className:'f-align-center'
							},
							{
							  title: '单位',
							  dataIndex: 'unit',
							  key: 'unit',
							  className:'f-align-center'
							},
							{
							  title: '数量',
							  key: 'quantity',
							  className:'f-align-center',
							  render: (text, record) => (
							  	<span>{ record.quantity }</span>
							  )
							},
							{
							  title: '进货单价',
							  dataIndex: 'purchasePrice',
							  key: 'purchasePrice',
							  className:'f-align-center',
							  render: (text, record) => (
								<span>{ numberFormate(text) }</span>
							  )
							},
							{
							  title: '金额',
							  dataIndex: 'materialTotalAmount',
							  key: 'materialTotalAmount',
							  className:'f-align-center',
							  render: (text, record) => (
								<span>{ numberFormate(text) }</span>
							  )
							}
						]
				})
			}
		})
	}
	removeChooseGoods(id) {
		let chooseGoodsData = this.state.chooseGoodsList.filter((e)=>{ return e.materialId != id }), totalMoney = 0;
			chooseGoodsData.forEach((o, i)=>{
				o.materialTotalAmount = typeof o.materialTotalAmount == 'string'? Number(o.materialTotalAmount.replace(/,/g, '')) : o.materialTotalAmount;
				totalMoney += o.materialTotalAmount;
				o.materialTotalAmount = numberFormate(o.materialTotalAmount);
			});
		this.setState({
			chooseGoodsList: chooseGoodsData,
			totalMoney
		});
	}
	updateWarehouseId(v) {
		this.setState({
			warehouseId: ''
		}, function(){
			this.setState({
				warehouseId: v
			})
		})
	}
	updateWarehouseValue(v) {
		this.setState({
			warehouseName: v
		})
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount(){
		let self = this, { currentPage, pageSize } = this.state.searchPara;
		//查询仓库列表
		fpost('/api/warehouse/listWarehouseSimpleVO').then(res=>res.json()).then(res=>{
			if(res.success == true){
				this.setState({
					stockList: res.result,
					warehouseId: res.result.length? res.result[0].warehouseId : '',
					warehouseName: res.result.length? res.result[0].warehouseName : ''
				}, function(){
					if(self.state.warehouseId == '') {
						self.setState({
							inOutListData: []
						});
					}else {
						self.getInOutList({ currentPage , pageSize, operType: 1, warehouseId: self.state.warehouseId });
					}
				})
			}else{
				message.error(res.message);
			}
		});
	}
	render(){

		const columns = [
			{
			  title: '日期',
			  dataIndex: 'occurringDate',
			  key: 'occurringDate',
			  className:'f-align-center',
			  render: text => <span>{text.substr(0, 10)}</span>,
			}, {
			  title: '合计金额',
			  dataIndex: 'totalPrice',
			  key: 'totalPrice',
			  className:'f-align-center',
			  render: text => (
				<span>{numberFormate(text)}</span>
			  )
			}, {
			  title: '供应商',
			  dataIndex: 'suplierName',
			  key: 'suplierName',
			  className:'f-align-center',
			}, {
			  title: '操作人',
			  key: 'operatorName',
			  dataIndex: 'operatorName',
			  className:'f-align-center',
			}, {
			  title: '备注',
			  dataIndex: 'remark',
			  key: 'remark',
			  className:'f-align-center',
			}, {
			  title: '操作时间',
			  dataIndex: 'createTime',
			  key: 'createTime',
			  className:'f-align-center',
			}, {
			  title: '操作',
			  key: 'view',
			  className:'f-align-center',
			  render: (text, record) => (
				<Button className='f-radius4' type='primary' onClick={ ()=>{ this.showGoodsDetail(record.materialStockModifyId) } }><i className='iconfont icon-chakan f-pointer f-mr1' style={{ fontSize:'14px' }}></i>查看</Button>
			  )
			}];

		return (<div className='courseArranging classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>进出库管理</span></Breadcrumb.Item>
					</Breadcrumb>
					<Tablinks />
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt1 f-radius1'>
						<SearchFormCreate searchPara={ this.state.searchPara } updateWarehouseValue={ this.updateWarehouseValue.bind(this) } setPara={this.setPara.bind(this)} updateWarehouseId={ this.updateWarehouseId.bind(this) } wrappedComponentRef={ (inst) => this.searchForm = inst } warehouseId={ this.state.warehouseId } stockList={this.state.stockList} updatePurchaseList={ this.getInOutList.bind(this) }/>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt1 f-radius1 f-mt5'>
						<div className='f-bg-white f-radius1'>
							<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
								<h3 className="f-title-blue">进货列表</h3>
								<a className="f-btn-green" onClick={ this.addGoods.bind(this) }>
									<i className='iconfont icon-tianjiajiahaowubiankuang'></i>新增
								</a>
								<Modal title={'进货：'+(this.state.isView? this.state.findResult.warehouseName : this.state.warehouseName)}
									wrapClassName="vertical-center-modal"
									visible={ this.state.visibleType }
									width={ 800 }
									onOk={ this.saveData.bind(this) }
									onCancel={ this.cancelAddGoods.bind(this) }
									bodyStyle={{padding:'16px 0 0'}}
									footer={this.state.isView? [] : [
						            <Button key="back" onClick={ this.cancelAddGoods.bind(this) }>取消</Button>,
						            <Button key="submit" type="primary" onClick={ this.saveData.bind(this) } loading={this.state.isRequest}>
						              确定
						            </Button>,
						          ]}
								>
									<ChooseFormCreate totalMoney={this.state.totalMoney} findResult={ this.state.findResult } addColumns={ this.state.addColumns } showGoodsList={ this.showGoodsList.bind(this) } chooseGoodsList={ this.state.chooseGoodsList } isView={ this.state.isView } wrappedComponentRef={ (inst) => this.addForm = inst }/>
								</Modal>
								{
									this.state.warehouseId && this.state.goodsBoxVisible? <SelectGoods purchasePrice={true} warehouseId={this.state.warehouseId} visible={this.state.goodsBoxVisible} handleCancel={ this.cancelGoodsBox.bind(this) } handleOk={ this.afterSelectGoods.bind(this) }/> : ''
								}
							</div>
							{
								this.state.inOutListData != null? 
								<Table columns={columns} dataSource={this.state.inOutListData} pagination={ Pagination(this.state.total, this.state.searchPara, this.getInOutList.bind(this)) } bordered className='f-align-center'/>
								: <Loading />
							}
							
						</div>
					</div>
				</div>)
	}
}

class SearchForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
		    startValue: null,
		    endValue: null,
		    endOpen: false,
		  };
	}
	disabledStartDate = (startValue) => {
	    const endValue = this.state.endValue;
	    if (!startValue || !endValue) {
	      return false;
	    }
	    return startValue.valueOf() > endValue.valueOf();
	  }

	  disabledEndDate = (endValue) => {
	    const startValue = this.state.startValue;
	    if (!endValue || !startValue) {
	      return false;
	    }
	    return endValue.valueOf() <= startValue.valueOf();
	  }

	  onChange = (field, value) => {
	    this.setState({
	      [field]: value,
	    });
	  }

	  onStartChange = (value) => {
	    this.onChange('startValue', value);
	  }

	  onEndChange = (value) => {
	    this.onChange('endValue', value);
	  }

	  handleStartOpenChange = (open) => {
	    if (!open) {
	      this.setState({ endOpen: true });
	    }
	  }

	  handleEndOpenChange = (open) => {
	    this.setState({ endOpen: open });
	  }
	searchCondition() {
		let obj = this.props.form.getFieldsValue(), { pageSize } = this.props.searchPara;
		obj.currentPage = 1;
		obj.pageSize = pageSize;
		obj.operType = 1;
		obj.startTime = obj.startTime? obj.startTime.format(dateFormat) : '';
		obj.endTime = obj.endTime? obj.endTime.format(dateFormat) : '';
		obj.warehouseId = obj.warehouseId? obj.warehouseId : '';

		obj = undefinedToEmpty(obj);

		this.props.updatePurchaseList(obj);
		this.props.setPara(obj);
	}
	resetForm(){
		const { pageSize } = this.props.searchPara;
		this.props.form.resetFields();
		this.props.updatePurchaseList({ currentPage: 1, pageSize, operType: 1, warehouseId: this.props.stockList[0].warehouseId });
		this.props.setPara({
			searchPara: { currentPage: 1, pageSize, operType: 1, warehouseId: this.props.stockList[0].warehouseId }	
		});
	}
	updateWarehouseId(v) {
		this.props.updateWarehouseId(v);
	}
	updateWarehouseValue(value, option) {
		this.props.updateWarehouseValue(option.props.children)
	}
	render(){
		const { getFieldDecorator } = this.props.form;
		const { startValue, endValue, endOpen } = this.state;

		return (<Form layout='inline' className='f-over-hide'>
					<FormItem style={{ marginBottom:'20px' }} label='仓库' wrapperCol={{style:{width:'50%'}}}>
						{getFieldDecorator('warehouseId', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: this.props.stockList.length? this.props.stockList[0].warehouseId : undefined
				          })(
				            <Select
				                showSearch
				                style={{ width: 200 }}
				                placeholder="请选择"
				                onChange={ this.updateWarehouseId.bind(this) }
				                onSelect={ this.updateWarehouseValue.bind(this) }
				                optionFilterProp="children"
				                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				              >
							    {
							    	this.props.stockList.length? this.props.stockList.map((e, i)=>{
										return (<Option key={i} value={ e.warehouseId }>{ e.warehouseName }</Option>)
							    	}) : ''
							    }
				              </Select>
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} label='操作人' wrapperCol={{style:{width:'200px'}}}>
						{getFieldDecorator('operaterName', {
				            rules: [{
				              required: false,
				              message: '请输入操作人',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入操作人" />
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }}
						label="选择时段"
						wrapperCol={{style:{width:'200px'}}}
					>
						{
							getFieldDecorator('startTime', { 
								
							})(<DatePicker
						          disabledDate={this.disabledStartDate}
						          showTime
						          format="YYYY-MM-DD HH:mm:ss"
						          setFieldsValue={startValue}
						          placeholder="开始时间"
						          onChange={this.onStartChange}
						          onOpenChange={this.handleStartOpenChange}
						        />)
						}
					</FormItem>
					<div className='f-inline-block' style={{lineHeight:'30px'}}>--</div>
					<FormItem style={{ marginBottom:'20px' }} className='f-ml4'
						wrapperCol={{style:{width:'200px'}}}
					>
						{
							getFieldDecorator('endTime', { 
								
							})(<DatePicker
						          disabledDate={this.disabledEndDate}
						          showTime
						          format="YYYY-MM-DD HH:mm:ss"
						          setFieldsValue={endValue}
						          placeholder="结束时间"
						          onChange={this.onEndChange}
						          open={endOpen}
						          onOpenChange={this.handleEndOpenChange}
						        />)
						}
					</FormItem>
					<FormItem className='f-right'>
						<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.searchCondition.bind(this) }>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
	        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
	        				<i className='iconfont icon-reset f-mr2'></i>
	        				重置
	        			</Button>
					</FormItem>
				</Form>)
	}
}
class ChooseForm extends Component {
	constructor(props) {
		super(props);
		this.onChange.bind(this);
	}
	onChange(m, d){
		
	}
	render(){
		const { getFieldDecorator } = this.props.form;
		const { operatorName='', suplierName='', remark='', occurringDate } = this.props.findResult;
		return (<Form>
					<FormItem className='f-inline-block f-mr5' style={{marginLeft:'50px'}} label='进货日期' labelCol={{style:{verticalAlign: 'middle'}}} wrapperCol={{style:{width:'200px', display:'inline-block', verticalAlign: 'middle'}}}>
						{
							!this.props.isView? getFieldDecorator('occurringDate', {
				            rules: [{
				              required: true,
				            }],
				            initialValue: moment(new Date() , 'YYYY-MM-DD HH:mm:ss')
				          })(
				            <DatePicker showTime={true}/>
				          ) : <span className='f-vertical-middle'>{occurringDate}</span>
						}
					</FormItem>
					<FormItem className='f-inline-block' labelCol={{style:{verticalAlign: 'middle'}}} label='供应商'  wrapperCol={{style:{width:'200px', display:'inline-block', verticalAlign: 'middle'}}}>
						{
							!this.props.isView? getFieldDecorator('suplierName', {
					            rules: [{
					              required: true,
					              message: '请输入供应商',
					              whitespace: true
					            },
					            {
					            	max: 50,
					            	min: 1,
					            	transform: (val) =>{
					            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
					            	},
					            	message: '长度不能超过50（25个汉字）'
					            }],
					            initialValue: ''
					          })(
					            <Input placeholder="请输入供应商" />
					          ) : <span className='f-vertical-middle'>{suplierName}</span>
						}
					</FormItem>
					{
						this.props.isView? '' : <FormItem className='f-inline-block'>
													<Button style={{border:'none'}} className='f-blue' onClick={ this.props.showGoodsList }>选择物品</Button>
												</FormItem>
					}
					<Table style={{height:'500px', overflowY:'scroll'}} columns={this.props.addColumns} dataSource={ this.props.chooseGoodsList } bordered={this.props.isView? true : false} pagination={false}/>
					<div style={{background:'#F3F3F8', paddingLeft:'50px', lineHeight:'50px'}}><span>合计：</span><span>{numberFormate(this.props.isView? this.props.findResult.totalPrice : this.props.totalMoney)}</span></div>
					<Row className='f-mt5'>
						<Col span={12} style={{paddingLeft:'50px'}}>
							<FormItem className='f-inline-block' label='备注' labelCol={{ style:{ verticalAlign: 'middle' } }} wrapperCol={{style:{width:'200px', display:'inline-block', verticalAlign: 'middle'}}}>
									{
										!this.props.isView? getFieldDecorator('remark', {
								            rules: [{
								              required: false,
								              message: '请输入备注',
								            },
								            {
								            	max: 200,
								            	min: 1,
								            	transform: (val) =>{
								            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
								            	},
								            	message: '长度不能超过200（100个汉字）'
								            }],
								            initialValue: ''
								          })(
								            <Input placeholder='请输入备注' />
								          ) : <span className='f-vertical-middle'>{remark}</span>
									}
								</FormItem>
						</Col>
						<Col span={12} className='f-align-right f-pr4'>
							{
								this.props.isView? <span>操作人：{ operatorName }</span> : ''
							}
						</Col>
					</Row>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);
const ChooseFormCreate = Form.create()(ChooseForm);