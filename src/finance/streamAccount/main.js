import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Button,
	Table,
	DatePicker,
	Form,
	Row,
	Col,
	Select,
	Input,
	message,
	Pagination
} from 'antd';
import '../main.less';
import {
	Loading,
	numberFormate
} from '../../common/g.js';
import $ from 'jquery';
import {
	fpost
} from '../../common/io.js';

const Option = Select.Option;
const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			pageResult: null, //分页返回的列表总信息
			currentPage: 1, //当前页码
			pageSize: 10, //每页显示条数
			total: null, //总数
			loading: 1, //是否是正在加载中
			allSchool: [],
			masterInfo: [], //销售员
		}
	}
	_getSchool() { //获取全部校区信息
		const self = this;
		fpost('/api/system/schoolarea/listSchoolArea')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							allSchool: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getMaster = (v) => { //获取销售员下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				name: v ? v : '',
				type: 1
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterList: res.result
					})
					let masterInfo = []; //助教、老师等下拉搜索信息
					for (let i = 0; i < res.result.length; i++) {
						let list = {};
						list.id = res.result[i].id;
						list.name = res.result[i].name + '-' + res.result[i].department + '-' + res.result[i].mobile;
						masterInfo.push(list)
					}
					self.setState({
						masterInfo: masterInfo
					})
				} else {
					message.error(res.message)
				}
			});
	}
	componentDidMount() {
		this._getMaster(); //获取销售员
		this._getData(); //获取列表数据
		this._getSchool(); //获取校区下拉框
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this._getData();
		})
	}
	_search() { //搜索
		this.setState({
			currentPage: 1
		}, function() {
			let classname = {};
			classname['ss'] = $.trim($('.inputBG').val());
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	_getData(fieldsValue) {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['startTime'] = fieldsValue['startTime'] ? fieldsValue['startTime'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['endTime'] = fieldsValue['endTime'] ? fieldsValue['endTime'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['phone'] = fieldsValue['phone'] ? fieldsValue['phone'] : '';

				fieldsValue['salesId'] = fieldsValue['salesId'] ? fieldsValue['salesId'] : '';
				fieldsValue['schoolAreaId'] = fieldsValue['schoolAreaId'] ? fieldsValue['schoolAreaId'] : '';
				fieldsValue['studentNo'] = fieldsValue['studentNo'] ? $.trim(fieldsValue['studentNo']) : '';
				fieldsValue['studentName'] = fieldsValue['studentName'] ? $.trim(fieldsValue['studentName']) : '';
				fieldsValue['phone'] = fieldsValue['phone'] ? $.trim(fieldsValue['phone']) : '';

				fieldsValue.pageSize = this.state.pageSize;
				fieldsValue.currentPage = this.state.currentPage;
				fieldsValue.financeType = fieldsValue.financeType ? fieldsValue.financeType : '';
				fieldsValue.tradeType = fieldsValue.tradeType ? fieldsValue.tradeType : '';
				fieldsValue.businessCode = fieldsValue.businessCode ? fieldsValue.businessCode : '';
				let reg = /^1(3|4|5|7|8|9)\d{9}$/;
				if ($.trim(fieldsValue['phone'])) {
					if (!reg.test($.trim(fieldsValue['phone']))) {
						message.warning('手机号格式不正确，请重新输入', 1);
						return false;
					}
				} else {

				}
				this.getList(fieldsValue);
			}
		)
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			this._getData();
		})
	}
	getList(fieldsValue) { //获取列表
		let self = this;
		fpost('/api/finance/financeFundFlow/pageFinanceFundFlow ', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
							item.amount = numberFormate(item.amount)
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
						self._event();
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getPhone(v) {
		let reg = /^1(3|4|5|7|8|9)\d{9}$/;
		if (v.target.value && !reg.test($.trim(v.target.value))) {
			message.warning('手机号格式不正确，请重新输入', 1);
			return false;
		}
	}
	_event() {
		for (var i = 0; i < $('.amountNum').length; i++) {
			$('.amountNum').eq(i).html(parseFloat($('.amountNum').eq(i).html()).toFixed(2))
		}
		for (var i = 0; i < $('.studentNo').length; i++) {
			if ($('.studentNo').eq(i).html() == '()') {
				$('.studentNo').eq(i).html('');
			}
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const columns = [{
			title: '学员姓名（学号）',
			className: 'f-align-center',
			key: '0',
			fixed: 'left',
			width: 150,
			render: (text, record) => {
				return (
					<span>{record.studentName}
						<i className='studentNo'>({record.studentNo})</i>
					</span>
				)
			}
		}, {
			title: '时间',
			dataIndex: 'createTime',
			key: '1',
			width: 200,
			className: 'f-align-center',
		}, {
			title: '校区',
			dataIndex: 'schoolAreaName',
			className: 'f-align-center',
			key: '2',
			width: 150,
		}, {
			title: '销售员',
			dataIndex: 'salesName',
			className: 'f-align-center',
			key: '3',
			width: 150,
		}, {
			title: '支付方式',
			dataIndex: 'tradeTypeStr',
			className: 'f-align-center',
			key: '8',
			width: 150,
		}, {
			title: '收支类别',
			dataIndex: 'financeTypeStr',
			key: '9',
			className: 'f-align-center',
			width: 150,
		}, {
			title: '金额',
			key: '10',
			className: 'f-align-center',
			width: 150,
			render: (text, record) => {
				return (
					<span>{record.amount}</span>
				)
			}
		}, {
			title: '业务类型',
			dataIndex: 'businessCodeStr',
			key: '11',
			className: 'f-align-center',
			width: 150,
		}, {
			title: '业务信息描述',
			className: 'f-align-center',
			key: '12',
			width: 200,
			render: (text, record) => {
				return (
					<div title={record.remark} style={{width:'250px'}} className='f-line1'>
						{record.remark}
			   		</div>
				)
			}
		}, {
			title: '操作员',
			className: 'f-align-center f-line1',
			key: '13',
			width: 150,
			fixed: 'right',
			render: (text, record) => {
				return (
					<div title={record.operatorName}>
			   			{record.operatorName}
			   		</div>
				)
			}
		}];
		return (<div className='courseArranging Topselect classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>财务统计</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">流水账</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div>
				<div className='f-bg-white Topselect f-radius1 f-box-shadow1 ' style={{padding:'20px 20px 15px 20px'}}>
					<Form>
						 <Row>
					        		<Col span={9}>
					        		<Col span={4} className='f-fz4 f-black f-h4-lh4'>时间段:&emsp;</Col>
					        		<Col span={20} >
						    			 <Col span={11}>
						    			 <FormItem {...formItemLayout}>
								          {getFieldDecorator('startTime')(
								            	<DatePicker
								            		showTime
									          disabledDate={this.disabledStartDate.bind(this)}
									          format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.startValue}
									          placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)}
									          onOpenChange={this.handleStartOpenChange.bind(this)}/>
								          )}
								     </FormItem>
						    			 </Col>
						    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
						    			  <Col span={11}>
						    			  <FormItem {...formItemLayout}>
								          {getFieldDecorator('endTime')(
								            <DatePicker
								             showTime
									          disabledDate={this.disabledEndDate.bind(this)}
									          format="YYYY-MM-DD HH:mm:ss"
											  setFieldsValue={this.state.endValue}
									          placeholder="结束时间"
									          onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen}
									          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								          )}
								     </FormItem>
							         	</Col>
							        	</Col>
					        		</Col>
					        		<Col span={5} >
					             <Col span={12} className='f-align-right f-fz4 f-black f-h4-lh4'>收支类别:&emsp;</Col>
					              <Col span={12} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('financeType', {
					                  	valuePropName: 'value',
					                  })(
					                    <Select placeholder="请选择">
					                        	<Option value="0">收入</Option>
						              		<Option value="1">支出</Option>
					                      </Select>  
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					          	<Col span={5} >
					             <Col span={12} className='f-align-right f-fz4 f-black f-h4-lh4'>支付方式:&emsp;</Col>
					              <Col span={12} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('tradeType', {
					                  })(
					                    <Select placeholder="请选择" >
					                        	<Option value="1">现金</Option>
					                        	<Option value="2">微信</Option>
					                        	<Option value="3">支付宝</Option>
					                        	<Option value="4">刷卡</Option>
						              		<Option value='5'>转账</Option>
						              		<Option value="6">支票</Option>
					                      </Select>  
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					          	<Col span={5} >
					             <Col span={12} className='f-align-right f-fz4 f-black f-h4-lh4'>业务类型:&emsp;</Col>
					              <Col span={12} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('businessCode', {
					                  	valuePropName: 'value',
					                  })(
					                    <Select placeholder="请选择">
					                        	<Option value="REGISTRATION">报名</Option>
						              		<Option value="COURSE_TRANSFER">转班</Option>
						              		<Option value="COURSE_REFUND">退课</Option>
						              		<Option value="COURSE_PARYLY_REFUND">退差价</Option>
						              		<Option value="COURSE_ARREAR">补欠费</Option>
						              		<Option value="MATERIAL_REFUND">物品退货</Option>
						              		<Option value="MATERIAL_SALES">物品售卖</Option>
						              		<Option value="BALANCE_RECHARGE">余额充值</Option>
						              		<Option value="BALANCE_WITHDRAWALS">余额提现</Option>
					                      </Select>  
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        
					        <Row>
					        		<Col span={9}>
					        			<Col span={4} className='f-fz4 f-black f-h4-lh4'>校区:&emsp;</Col>
					        			<Col span={20}>
					        				<FormItem {...formItemLayout}>
						                  {getFieldDecorator('schoolAreaId', {
						                  })(
						                   <Select
					                        showSearch
					                        allowClear
					                        placeholder="请选择"
					                        optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
					                      >
					                        	{ this.state.allSchool.map((elem, index) => {
								                  return(
								                  	<Option key={elem.id} value={elem.id}>{elem.name}</Option>
								                  )
								                }) 
											 }
					                      </Select> 
						                  )}
					                    </FormItem>
					        			</Col>
					        		</Col>
					        		<Col span={5}>
						             <Col span={12} className='f-align-right f-fz4 f-black f-h4-lh4'>销售员:&emsp;</Col>
						              <Col span={12}>
						              <FormItem {...formItemLayout}>
						                  {getFieldDecorator('salesId', {
						                  	valuePropName: 'value',
						                  })(
						                    <Select
						                  	  showSearch
						                  	  allowClear
									          placeholder='请选择销售员'
									          style={{ width: '100%' }}
									          optionFilterProp="children"
	                        					filterOption={
	                        						(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
	                        					}
									        >
												{this.state.masterInfo?
								                        	this.state.masterInfo.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name}</Option>)
								                        	})
								                        	:null
							                      }
									        </Select> 
						                  )}
						                </FormItem>
							            </Col>
					          	</Col>
					          	<Col span={5}>
					             <Col span={12} className='f-align-right f-fz4 f-black f-h4-lh4'>学员姓名:&emsp;</Col>
					              <Col span={12} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('studentName', {
					                  })(
					                   <Input type='text' placeholder='请输入学员姓名' className='pageInpHeight'/>
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					          	<Col span={5}>
					             <Col span={12} className='f-align-right f-fz4 f-black f-h4-lh4'>手机号:&emsp;</Col>
					              <Col span={12} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('phone', {
					                  	valuePropName: 'value',
					                  })(
					                    <Input type='text' placeholder='请输入手机号' onBlur={this._getPhone.bind(this)} className='pageInpHeight'/>
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        
					        <Row>
						        	<Col span={9}>
					        			<Col span={4} className='f-align-right f-fz4 f-black f-h4-lh4'>学号:&emsp;</Col>
					              <Col span={20} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('studentNo', {
					                  	valuePropName: 'value',
					                  })(
					                    <Input type='text' placeholder='请输入学号' className='pageInpHeight'/>
					                  )}
					                </FormItem>
						            </Col>
					        		</Col>
					        		
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this._search.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					</Form>
				</div>
			
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 '>
					<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue">流水账列表
						{/*<i className='f-blue'>(温馨提醒:列表上的上课老师指的是选择了试听课,测试和公开课这3种报课来源时填写的上课老师，而不是班级的上课老师)</i>*/}
						</h3>
					</div>
					{this.state.pageResult?
						<div>
							<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 1600}} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
				</div>
		</div>);
	}
}
const StreamAccount = Form.create()(Main);
export {
	StreamAccount
}