import React, {
	Component
} from 'react';
import { Breadcrumb, DatePicker, Form, Row, Col, Select, message ,Table} from 'antd';
import '../main.less';
import $ from 'jquery';
import {_getHistogram2 } from '../../common/g.js';
import { fpost } from '../../common/io.js';
import echarts from 'echarts';
const Option = Select.Option;
const FormItem = Form.Item;
const tagDetail = []; //校区id

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
			allCourse:[],
			checked: true,
			checked1: true,
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	_getCourse() { //获取全部课程信息
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						res.result.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							allCourse: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getHistogram(fieldsValue) { //收入和支出
		let chart = echarts.init(document.getElementById('histogram'));
		let $this = $('#histogram');
		let $name = {
			text: '',
			subtext: '学费（元）'
		}
		let chart1 = echarts.init(document.getElementById('income'));
		let $this1 = $('#income');
		let $name1 = {
			text: '',
			subtext: '',
			x: 'center'
		}
		let type=2;//柱状图
		let type1=1;//饼图
		let $pageShow=$('.pageShow');  //图表和明细整体所在的容器
		let $noData=$('.noData');//点击生成报表之前默认展示暂无数据......
		let urldata = '/api/finance/financeFundFlow/statisticConsumed';
		_getHistogram2(fieldsValue, urldata, chart, $this, $name,chart1, $this1, $name1, type,type1,$pageShow,$noData,function(res) {});
	}
	componentDidMount() {
		this._getCourse();  //全部课程
		setTimeout(() => window.scrollTo(0,0), 150);
		$('.pageShow').hide();
	}
	makeReport() { //点击生成报表事件
		for(var i = 0; i < $('.childCheck').length; i++) {
			if($('.childCheck').eq(i).prop("checked")) {
				tagDetail.push($('.childCheck').eq(i).attr('data-id'));
			}
		}
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!err){
					fieldsValue['startTime'] = fieldsValue['startTime'] ? fieldsValue['startTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue['endTime'] = fieldsValue['endTime'] ? fieldsValue['endTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue.type = '42';
					if(tagDetail.length>0){
						fieldsValue.courseIds=tagDetail;
					}
					this._getHistogram(fieldsValue); //柱状图
				}else{
					message.error('请先选好必填条件才可以生成报表哦');
				}
			}
		)
		tagDetail.splice(0, tagDetail.length)
	}
	allSelect(v) { //全选
		if(v.target.checked) {
			$('.childCheck').prop("checked", true)
		} else {
			$('.childCheck').prop("checked", false)
		}
		 this.setState({
	      checked: v.target.checked,
	      checked1: v.target.checked,
	    })
	}
	render() {
		const dataSource = [{
		  key: '1',
		  name: '胡彦斌',
		  age: 32,
		  address: '西湖区湖底公园1号',
		}, {
		  key: '2',
		  name: '胡彦祖',
		  age: 42,
		  address: '西湖区湖底公园1号'
		}];
		
		const columns = [{
		  title: '学员姓名（学号）',
		  dataIndex: 'address',
		  key: '0',
		  width: 100,
		  className: 'f-align-center'
		},{
		  title: '课程',
		  dataIndex: 'name',
		  key: '1',
		  className: 'f-align-center',
		  width:150
		}, {
		  title: '班级',
		  dataIndex: 'age',
		  className: 'f-align-center',
		  key: '2',
		  width:150
		}, {
		  title: '课消金额',
		  className: 'f-align-center',
		  dataIndex: 'address',
		  key: '3',
		  width:150
		},{
		  title: '课消类型',
		  dataIndex: 'address',
		  className: 'f-align-center',
		  key: '4',
		  width:150
		},{
		  title: '校区',
		  dataIndex: 'address',
		  className: 'f-align-center',
		  key: '5',
		  width:150
		}];
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return(<div className='courseArranging Topselect classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>财务统计</Breadcrumb.Item>
				    <Breadcrumb.Item>课消统计</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">按课程</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<Form className='f-box-shadow1 f-radius1 f-bg-white' style={{padding:'16px 16px 0 16px'}}>
					<Row className='f-mb3'>
						<Col span={4}>
							<span className='f-fz4 f-black f-mr4'>课程:&emsp;</span>
							<input type='checkbox' className='f-mr2' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
       						}} onClick={this.allSelect.bind(this)}/>
       						<i className='f-fz3' style={{color: '#4A4A4A'}}>全选</i>
						</Col>
						<Col span={20} className='f-left'>
							{ this.state.allCourse.map((elem, index) => {
			                  return(
			                  	 <div key={index} style={{marginRight:'40px'}} className='f-left f-mb3'>
									<input type='checkbox' className='f-mr2 childCheck' data-id={elem.id} style={{
	           							border: '1px solid #D9D9D9',
										borderRadius: '2px',
	       							}} />
	       							<i className='f-fz3' style={{color: '#4A4A4A'}}>{elem.name}</i>
								</div>
			                  )
			                }) 
						 }
						</Col>
					</Row>
					<Row>
						<Col span={10}>
							<Col span={5} className='f-fz4 f-black f-h4-lh4 f-label'>时间段:&emsp;</Col>
				        		<Col span={19} >
					    			 <Col span={11}>
					    			 	<FormItem {...formItemLayout}>
								      {getFieldDecorator('startTime',{
								      	rules: [{
							              required: true,
							              message: '请先选择开始时间',
							            }],
								      })(
							            	<DatePicker
							            	  showTime
								          disabledDate={this.disabledStartDate.bind(this)}
								          format="YYYY-MM-DD HH:mm:ss"
								          setFieldsValue={this.state.startValue}
								          placeholder="开始时间"
								          onChange={this.onStartChange.bind(this)}
								          onOpenChange={this.handleStartOpenChange.bind(this)}/>
							            	)}
								     </FormItem>
					    			 </Col>
					    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
					    			  <Col span={11}>
					    			  	<FormItem {...formItemLayout}>
								          {getFieldDecorator('endTime',{
								          	rules: [{
								              required: true,
								              message: '请先选择结束时间',
								            }],
								          })(
								            <DatePicker
								              showTime
									          disabledDate={this.disabledEndDate.bind(this)}
									          format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.endValue}
									          placeholder="结束时间"
									          onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen}
									          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								            )}
									     </FormItem>
						         	</Col>
							    </Col>
				       </Col>
				       <Col span={9} className='f-align-right f-pr4'>
				       		<Col span={8} className='f-fz4 f-black f-h4-lh4 f-label'>课消类型:&emsp;</Col>
							<Col span={14}>
								<FormItem>
				                  {getFieldDecorator('consumedType', {
				                  	initialValue:'1'
				                  })(
				                    <Select
			                        placeholder="请选择"
			                        optionFilterProp="children"
			                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
			                      >
			                       <Option key='1' value='1'>原价课消</Option>
			                       <Option key='2' value='2'>优惠课消</Option>
			                      </Select> 
				                  )}
				                </FormItem>
				              </Col>
				        	</Col>
				        	<Col span={5} className='f-align-right f-pr4'>
							<a className="f-btn-blue" onClick={this.makeReport.bind(this)}>生成报表</a>
				        	</Col>
					</Row>
				</Form>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-pd4 f-mt5 pageShow'>
					<div id="histogram" style={{width:' 1000px',height:'500px'}} className='f-mb2'></div>
					<div id="income" style={{width:' 1000px',height:'400px'}}></div> 
					{/*<div className='f-mt5 f-mb4'>
						<div className='f-flex f-mb3' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">明细列表</h3>
							<a className="f-btn-blue _export">
							<i className='iconfont icon-chuangjian f-mr1'></i>
							导出报表</a>
						</div>
						<Table columns={columns} dataSource={dataSource} bordered pagination={false}/>
					</div>*/}
				</div>
				<div className='noData f-mt4 f-align-center'>暂无数据......</div>
		</div>);
	}
}
const CourseConsume = Form.create()(Main);
export {
	CourseConsume
}