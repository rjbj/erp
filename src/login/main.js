import React, {
	Component
} from 'react';
import {
	Row,
	Col,
	Button,
	message,
	Input,
	Form,
	Modal
} from 'antd';
import {
	fpostLogin
} from '../common/io.js';
import {
	getUnicodeParam
} from '../common/g.js';
import store from 'store2';
import './main.less';
const confirm = Modal.confirm;

const FormItem = Form.Item;
let url = getUnicodeParam('target');
let pathname = window.location.pathname;
if (!url) {
	if (pathname == '/login') {
		url = '/index';
	} else {
		url = window.location.href;
	}
} else if (url.indexOf('login') != -1) {
	url = '/index';
};

export class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: 1,
			name: null,
			password: null,
		};
	}
	_next() { //登录按钮操作
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if (!err) {
					fieldsValue.password = fieldsValue.password ? fieldsValue.password : '';
					fieldsValue.username = fieldsValue.username ? fieldsValue.username : '';
					this._getData(fieldsValue);
				} else {
					message.error('请先把表单信息填写完整才可以登录哦')
				}
			}
		)
	}
	Confirm() {
		confirm({
			title: '需要重置密码吗?',
			okText: '确定',
			okType: 'danger',
			cancelText: '取消',
			onOk() {
				window.location.href = '/resetPassword?target=' + url;
			},
			onCancel() {
				window.location.href = url;
			},
		});
	}
	_getData(fieldsValue) {
		fpostLogin('/login', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {

					store.set('sessionId', res.result.sessionId);
					store.set('userInfo', res.result.userInfo);
					message.success('登录成功');
					if (res.result.resetPwd == '1') {
						this.Confirm();
					} else {
						window.location.href = url;
					}
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		return (
			<Form style={{minHeight:'100vh',background: 'white'}} className='Topselect loginPage'>
					<div className="f-header" style={{height: '87px',lineHeight: '87px'}}>
		                <Row>
		                    <Col span={4}>
		                        <h1 className="logo f-left f-black f-align-center f-align-center" style={{width:'200px'}}>
		                            <i className="iconfont icon-xiaoqu f-fz9 f-mr2"></i>
		                            e校通
		                        </h1>
		                     </Col>
		                  </Row>
		             </div>
		             <div style={{width:'100%',padding:'80px 80px',backgroundImage: 'linear-gradient(-180deg, #22AFFF 0%, #2187FF 100%)'}} className='f-align-center'>
		             	<div className='f-flex' style={{justifyContent:'space-around'}}>
							<div>
		             			<img src ='http://img.fancyedu.com/sys/ic/operation/1511418105700_school.png' style={{width:'520px'}}/>
							</div>
							<div className='f-bg-white f-align-center f-radius1 f-box-shadow1' style={{padding:'38px 24px'}}>
								<div style={{fontSize: '22px',marginBottom:'40px'}} className='f-black f-mb5'>
									欢迎登录
								</div>
								<div style={{width:'340px',height:'40px',background: '#F1F1F3',paddingLeft:'45px'}} className='f-radius2 f-relative f-mb5'>
									<img src='http://img.fancyedu.com/sys/ic/operation/1511418599712_person.png' className='f-absolute' style={{left:'15px',top:'8px'}}/>
									<div style={{width:'295px',left:'45px'}} className='f-absolute'>
				            				<FormItem>
						                  {getFieldDecorator('username', {
						                  	rules: [{
								              required: true,
								              message: '请输入用户名@机构代码',
								            }],
						                  })(
						                  	<Input placeholder='请输入用户名@机构代码' className='pageInpHeight' onPressEnter={this._next.bind(this)}/>
						                  )}
						                </FormItem>
				            			</div>
								</div>
								<div style={{width:'340px',height:'40px',background: '#F1F1F3',paddingLeft:'45px'}} className='f-radius2 f-relative f-mb5'>
									<img src='http://img.fancyedu.com/sys/ic/operation/1511419109546_pass.png' className='f-absolute' style={{left:'15px',top:'8px'}}/>
									<div style={{width:'295px',left:'45px'}} className='f-absolute'>
				            				<FormItem>
						                  {getFieldDecorator('password', {
						                  	rules: [{
								              required: true,
								              message: '请输入密码',
								            }],
						                  })(
						                  	<Input placeholder='请输入密码' type='password' className='pageInpHeight' onPressEnter={this._next.bind(this)}/>
						                  )}
						                </FormItem>
				            			</div>
								</div>
	            		 			<Button type="primary" onClick={this._next.bind(this)}
	            		 				style={{width:'340px',height:'44px',lineHeight:'44px'}} className='f-mb3'
	            		 			>登录e校通</Button>
	            		 			<div className='f-fz3 f-align-right' 
	            		 				style={{color: '#2187FF'}}
	            		 			>
									<a href='/forgotCode'>忘记密码</a>
								</div>
							</div>
		             	</div>
		             </div>
	           	
	           		<div>
	           			<img src='http://img.fancyedu.com/sys/ic/operation/1511419351564_footBG.png' style={{width:'100%'}}/>
	           		</div>
			</Form>
		);
	}
}
const Login = Form.create()(Main);
export {
	Login
}