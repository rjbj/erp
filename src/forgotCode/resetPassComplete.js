import React, {
	Component
} from 'react';
import { Row, Steps, Button, Form } from 'antd';
import { CodeLayout } from '../common/codeLayout.js';
import './main.less';

const Step = Steps.Step;

export class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: 3,  //当前选中步骤条第四步
		};
	}
	render() {
		const steps = [{
			title: '填写用户名',
			content: '填写用户名',
		}, {
			title: '身份验证',
			content: '身份验证',
		},  {
			title: '完成',
			content: '完成',
		}];
		const {
			current
		} = this.state;
		return(
			<Form style={{minHeight:'100vh'}} className='Topselect'>
				<CodeLayout/>
	            <div style={{margin:'0 auto',marginTop:'10%',width:'70%'}}>
	            		<div style={{marginBottom:'70px'}}>
	            			<Steps current={current}>
				          {steps.map(item => <Step key={item.title} title={item.title} />)}
				        </Steps>
	            		</div>
	            		<Row className='f-mb5 f-align-center'>
	            			<img src ='http://img.fancyedu.com/sys/ic/operation/1511416482011_sure.png'/>
	            		</Row>
	            		<Row className='f-align-center f-fz3 f-mb5' style={{color: '#243650'}}>
	            			密码修改成功
	            		</Row>
	            		<Row className='f-align-center'>
	            		 <Button type="primary" onClick={()=>{window.location.href='/login'}}>立即登录</Button>
	            		</Row>
	            </div>
			</Form>
		);
	}
}
const ResetPassComplete = Form.create()(Main);
export {
	ResetPassComplete
}